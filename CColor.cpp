#include "sdk/Structs.h"

CColor red(255, 0, 0, 255);
CColor green(0, 255, 0, 255);
CColor blue(0, 0, 255, 255);
CColor white(255, 255, 255, 255);
CColor black(0, 0, 0, 255);
CColor yellow(255, 255, 0, 255);
CColor grey(192, 192, 192, 255);

CColor green_menu(0, 255, 0, 190);
CColor white_menu(255, 255, 255, 190);
CColor black_menu(0, 0, 0, 190);

CColor::CColor(int r, int g, int b, int a) :
	m_iR(r), m_iG(g), m_iB(b), m_iA(a)
{
	m_dwColor = 0;
	Set();
}

CColor &CColor::operator =(CColor& color)
{
	m_iR = color.m_iR;
	m_iG = color.m_iG;
	m_iB = color.m_iB;
	m_iA = color.m_iA;

	Set();

	return *this;
}

void CColor::Set()
{
	m_dwColor = 0;
	m_dwColor |= ((BYTE)m_iA << 24);
	m_dwColor |= ((BYTE)m_iR << 16);
	m_dwColor |= ((BYTE)m_iG << 8);
	m_dwColor |= ((BYTE)m_iB << 0);
}

DWORD CColor::Get()
{
	return m_dwColor;
}