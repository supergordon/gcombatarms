#include "SDK\\Structs.h"

DWORD modulebase = 0;
BOOL APIENTRY DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	if(fdwReason == DLL_PROCESS_ATTACH)
	{
		char path[MAX_PATH] = "";
		GetModuleFileName(hinstDLL, path, MAX_PATH);
		if(strlen(path) != 0) {
			exit(1);
		}

		/*GetModuleFileName(GetModuleHandle(0), path, MAX_PATH);
		if(strstr(path, "Level Up")) {
			exit(1);
		}*/
		//g_pTools->LogText("Base: %08X", hinstDLL);
		modulebase = (DWORD)hinstDLL;
		ShellExecute(0, _open, website, 0, 0, SW_SHOW);
		CreateThread(0, 0, InitializeGameHook, 0, 0, 0);
		srand(GetTickCount());
	}

	return true;
}

/*
004F90C6    8B86 C8000000   MOV EAX,DWORD PTR DS:[ESI+C8]
004F90CC    8B8E CC000000   MOV ECX,DWORD PTR DS:[ESI+CC]
004F90D2    8B96 D0000000   MOV EDX,DWORD PTR DS:[ESI+D0]
004F90D8    F3:0F1086 48010>MOVSS XMM0,DWORD PTR DS:[ESI+148]
004F90E0    53              PUSH EBX
004F90E1    57              PUSH EDI
*/