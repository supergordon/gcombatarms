#ifndef _MENU_
#define _MENU_

class CMenu
{
	char szTitel[64];
	struct elements_t
	{
		char szName[64];
		int iMin;
		int iMax;
		int iStep;
		int *pValue;
		char szDescription[512];
	};

	struct sub_t {
		char szName[64];
		elements_t item[30];
		int items;
	};

	sub_t subitem[10];
	int iSelected;
	int x, y;
	int w;
	bool bOnHighlighting;
	bool bShowValue;

public:
	CMenu(char* szTitelName, int x, int y, int w, bool bInHighlighting, bool bShowValue);
	~CMenu();

	void AddEntry(char* szName, int* pValue, int iMin, int iMax, int iStep, char* desc = NULL);

	void AddEntry(sub_t* item, char* szName) {
		int count = item->items + 1;
		memcpy(&subitem[count], item, sizeof(sub_t));
		strcpy(subitem[count].szName, szName);
		subitem[count].items = count;
	}

	void AddSubentry(int index, char* szName, int* pValue, int iMin, int iMax, int iStep) {
		
	}

	bool KeyHandling(int key);
	void Draw();
	elements_t list[100];
	bool bActive;
	bool bMenuActive;
	int iElements;
	bool boolAimkey;
	WNDPROC window;
}; 
extern CMenu* pMenu;
extern CMenu* pFriends;

#endif