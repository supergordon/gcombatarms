#include "SDK\\Structs.h"

#include <ctime>


EndScene_t pEndScene = NULL;
Reset_t pReset = NULL;
DrawIndexedPrimitive_t pDrawIndexedPrimitive = NULL;
DrawIndexedPrimitive_t2 pDrawIndexedPrimitive2 = NULL;
SetStreamSource_t pSetStreamSource = NULL;

//CD3DFont	*pFont = NULL;

LPDIRECT3DDEVICE9 pD3Ddev = 0;
LPDIRECT3DTEXTURE9 texRed=0, texYellow=0, texBlue=0, texGreen=0, texWhite=0, texBlack=0;
D3DVIEWPORT9 viewPort;
D3DXMATRIX projection, view, world, identity;
IDirect3DVertexBuffer9* la = 0;

UINT STRIDE;
UINT KE;

CCVARS cvars;
LTVector vTelePlayer;

CConsoleCommand cmdNospread(&cvars.Nospread);
CConsoleCommand cmdSatChams(&cvars.SatChams);
CConsoleCommand cmdBoxes(&cvars.Boxes);
CConsoleCommand cmdTracers(&cvars.Tracers);
CConsoleCommand cmdSuperjump(&cvars.SJump);
CConsoleCommand cmdShowFPS(&cvars.ShowFps);
CConsoleCommand cmdGravity(&cvars.Gravity);
CConsoleCommand cmdSelfKill(&cvars.SelfKill);
CConsoleCommand cmdWireframe(&cvars.Wrireframe);
CConsoleCommand cmdSpeedhack(&cvars.Speedhack);
CConsoleCommand cmdPickup(&cvars.PickupCmd);

enum { BOXES=10, TRACERS=11, SJUMP=12, SHOWFPS=13, RESPAWN=14, ACT=15, NOSWAY=16,GRAVITY=17,SELFKILL=18,AMMO=19,WIREFRAME=20, BREATH=21 };


void SetPatches(int a, int b)
{
	//if(a == 3) //speedhack
	//{
	//	if(b)
	//	{
	//		//g_pEngine->m_pLTBase->RunConsoleCommand("\"ShowFPS\" \"1.000000\"");
	//		char Buffer[50];//FRunVel
	//		sprintf(Buffer,/*FRunVel %f*/XorStr<0x8C,11,0x06D2D1BD>("\xCA\xDF\xFB\xE1\xC6\xF4\xFE\xB3\xB1\xF3"+0x06D2D1BD).s, (285.0f + (float)cvars.Speedhack * 75));
	//		g_pEngine->m_pLTBase->RunConsoleCommand(Buffer);
	//		sprintf(Buffer,/*BRunVel %f*/XorStr<0x89,11,0x8FE881FE>("\xCB\xD8\xFE\xE2\xDB\xEB\xE3\xB0\xB4\xF4"+0x8FE881FE).s, (285.0f + (float)cvars.Speedhack * 75));
	//		g_pEngine->m_pLTBase->RunConsoleCommand(Buffer);
	//		sprintf(Buffer,/*SRunVel %f*/XorStr<0x7C,11,0x7A017356>("\x2F\x2F\x0B\x11\xD6\xE4\xEE\xA3\xA1\xE3"+0x7A017356).s, (285.0f + (float)cvars.Speedhack * 75));
	//		g_pEngine->m_pLTBase->RunConsoleCommand(Buffer);

	//	}
	//	if(b == 0)
	//	{
	//		//g_pEngine->m_pLTBase->RunConsoleCommand("\"ShowFPS\" \"0.000000\"");
	//	}
	//}
}

void Menu()
{

	if(!pMenu->window) {
		if((GetAsyncKeyState(VK_INSERT) & 1) || (GetAsyncKeyState(VK_F12) & 1))
			pMenu->bMenuActive = !pMenu->bMenuActive;
	}


	if(pMenu->bMenuActive)
	{
		pMenu->Draw();

		if(!pMenu->window) {
			pMenu->KeyHandling(0);
		}

		pMenu->iElements = 0;


		pMenu->AddEntry(/*-DirectX*/XorStr<0x3E,9,0xE39C600C>("\x13\x7B\x29\x33\x27\x20\x30\x1D"+0xE39C600C).s, &cvars.DirectX, 0, 1, 1, /*D3D settings*/XorStr<0xe4,13,0x7a4717af>("\xa0\xd6\xa2\xc7\x9b\x8c\x9e\x9f\x85\x83\x89\x9c"+0x7a4717af).s);
		if(cvars.DirectX) {
			//pMenu->AddEntry("  Test", &cvars.Test, 0, 0x3C, 1);
			pMenu->AddEntry(/*  Chams*/XorStr<0x19,8,0x014FE06F>("\x39\x3A\x58\x74\x7C\x73\x6C"+0x014FE06F).s, &cvars.Chams, 0, 12, 1, /*Lets you color people and see them through wall*/XorStr<0x23,48,0xb954ba87>("\x6f\x41\x51\x55\x7\x51\x46\x5f\xb\x4f\x42\x42\x40\x42\x11\x42\x56\x5b\x45\x5a\x52\x18\x58\x54\x5f\x1c\x4e\x5b\x5a\x60\x35\x2a\x26\x29\x65\x32\x2f\x3a\x26\x3f\x2c\x24\x6d\x39\x2e\x3c\x3d"+0xb954ba87).s);
			pMenu->AddEntry(/*  Wallhack*/XorStr<0x94,11,0x9C2F3348>("\xB4\xB5\xC1\xF6\xF4\xF5\xF2\xFA\xFF\xF6"+0x9C2F3348).s, &cvars.Wallhack, 0, 1, 1, /*Lets you see people through wall*/XorStr<0x2a,33,0xc18cc25b>("\x66\x4e\x58\x5e\xe\x56\x5f\x44\x12\x40\x51\x50\x16\x47\x5d\x56\x4a\x57\x59\x1d\x4a\x57\x32\x2e\x37\x24\x2c\x65\x31\x26\x24\x25"+0xc18cc25b).s);
			pMenu->AddEntry(/*  Asus*/XorStr<0xFD,7,0xF1501E36>("\xDD\xDE\xBE\x73\x74\x71"+0xF1501E36).s, &cvars.Asus, 0, 1, 1, /*Complete level gets wallhacked*/XorStr<0xcd,31,0xfd2fca66>("\x8e\xa1\xa2\xa0\xbd\xb7\xa7\xb1\xf5\xba\xb2\xae\xbc\xb6\xfb\xbb\xb8\xaa\xac\xc0\x96\x83\x8f\x88\x8d\x87\x84\x83\x8c\x8e"+0xfd2fca66).s);
			pMenu->AddEntry(/*  Wireframe*/XorStr<0x15,12,0x9A347DF3>("\x35\x36\x40\x71\x6B\x7F\x7D\x6E\x7C\x73\x7A"+0x9A347DF3).s, &cvars.Wireframe, 0, 1, 1, /*Wireframe mode*/XorStr<0xc4,15,0x28c329f5>("\x93\xac\xb4\xa2\xae\xbb\xab\xa6\xa9\xed\xa3\xa0\xb4\xb4"+0x28c329f5).s);
			pMenu->AddEntry(/*  Nofog*/XorStr<0x74,8,0x34DFBFC0>("\x54\x55\x38\x18\x1E\x16\x1D"+0x34DFBFC0).s, &cvars.Nofog, 0, 1, 1, /*Removes the fog*/XorStr<0xae,16,0x131446e1>("\xfc\xca\xdd\xde\xc4\xd6\xc7\x95\xc2\xdf\xdd\x99\xdc\xd4\xdb"+0x131446e1).s);
			pMenu->AddEntry(/*  Nosky*/XorStr<0x5A,8,0xE0C5B110>("\x7A\x7B\x12\x32\x2D\x34\x19"+0xE0C5B110).s, &cvars.Nosky, 0, 1, 1, /*Removes the sky*/XorStr<0x88,16,0x21babb22>("\xda\xec\xe7\xe4\xfa\xe8\xfd\xaf\xe4\xf9\xf7\xb3\xe7\xfe\xef"+0x21babb22).s);
			//pMenu->AddEntry(/*  Nohands*/XorStr<0x89,10,0x7F18A67A>("\xA9\xAA\xC5\xE3\xE5\xEF\xE1\xF4\xE2"+0x7F18A67A).s, &cvars.Nohands, 0, 1, 1, /*Removes your hands*/XorStr<0x3b,19,0x6c9e39d5>("\x69\x59\x50\x51\x49\x25\x32\x62\x3a\x2b\x30\x34\x67\x20\x28\x24\x2f\x3f"+0x6c9e39d5).s);
			pMenu->AddEntry(/*  Crosshair*/XorStr<0x17,12,0x8CC15FB4>("\x37\x38\x5A\x68\x74\x6F\x6E\x76\x7E\x49\x53"+0x8CC15FB4).s, &cvars.Crosshair, 0, 5, 1, /*Display a crosshair on the middle of the screen*/XorStr<0xe3,48,0xe27aafb1>("\xa7\x8d\x96\x96\x8b\x89\x90\xca\x8a\xcc\x8e\x9c\x80\x83\x82\x9a\x92\x9d\x87\xd6\x98\x96\xd9\x8e\x93\x99\xdd\x93\x96\x64\x65\x6e\x66\x24\x6a\x60\x27\x7c\x61\x6f\x2b\x7f\x6e\x7c\x6a\x75\x7f"+0xe27aafb1).s);
		}

		pMenu->AddEntry(/*-Aimbot*/XorStr<0x47,8,0xE2DF6983>("\x6A\x09\x20\x27\x29\x23\x39"+0xE2DF6983).s, &cvars.Aimbot, 0, 1, 1, /*Aimbot settings*/XorStr<0x9f,16,0x36d19ed4>("\xde\xc9\xcc\xc0\xcc\xd0\x85\xd5\xc2\xdc\xdd\xc3\xc5\xcb\xde"+0x36d19ed4).s);
		if(cvars.Aimbot) {
			pMenu->AddEntry(/*  On*/XorStr<0xE5,5,0xDDD98BE5>("\xC5\xC6\xA8\x86"+0xDDD98BE5).s, &cvars.Aimtoggle, 0, 1, 1, /*Turn aimbot on or off*/XorStr<0x41,22,0x71737443>("\x15\x37\x31\x2a\x65\x27\x2e\x25\x2b\x25\x3f\x6c\x22\x20\x6f\x3f\x23\x72\x3c\x32\x33"+0x71737443).s);
			pMenu->AddEntry(/*  Key*/XorStr<0xF4,6,0x7BFC9257>("\xD4\xD5\xBD\x92\x81"+0x7BFC9257).s, &cvars.Aimkey, 0, 255, 1, /*Tells the aimbot on what key you have to press to aim. Key = see virtual key list*/XorStr<0x1e,82,0x1d50ea86>("\x4a\x7a\x4c\x4d\x51\x3\x50\x4d\x43\x7\x49\x40\x47\x49\x43\x59\xe\x40\x5e\x11\x45\x5b\x55\x41\x16\x5c\x5d\x40\x1a\x42\x53\x48\x1e\x57\x21\x37\x27\x63\x30\x2a\x66\x37\x3a\x2c\x39\x38\x6c\x39\x21\x6f\x31\x38\x3f\x7d\x74\x1e\x33\x2e\x78\x64\x7a\x28\x39\x38\x7e\x29\x9\x13\x16\x16\x5\x9\x46\xc\xd\x10\x4a\x7\x5\x1e\x1a"+0x1d50ea86).s);
			//pMenu->AddEntry(/*  Fov*/XorStr<0x65,6,0x0F4D25B1>("\x45\x46\x21\x07\x1F"+0x0F4D25B1).s, &cvars.Aimfov, 0, 360, 1);
			pMenu->AddEntry(/*  Smooth*/XorStr<0x8c,9,0xbcbd558b>("\xac\xad\xdd\xe2\xff\xfe\xe6\xfb"+0xbcbd558b).s, &cvars.Aimsmooth, 0, 100, 1, /*Slowly aims to the enemy*/XorStr<0x95,25,0xf6c394fa>("\xc6\xfa\xf8\xef\xf5\xe3\xbb\xfd\xf4\xf3\xec\x80\xd5\xcd\x83\xd0\xcd\xc3\x87\xcd\xc7\xcf\xc6\xd5"+0xf6c394fa).s);
			pMenu->AddEntry(/*  Mode*/XorStr<0x1,7,0xcb64ca65>("\x21\x22\x4e\x6b\x61\x63"+0xcb64ca65).s, &cvars.Aimmode, 0, 2, 1, /*0. Aim by Distance, 1. Aim by lowest health*/XorStr<0x2d,44,0xf82bc5fb>("\x1d\x0\xf\x71\x58\x5f\x13\x56\x4c\x16\x73\x51\x4a\x4e\x5a\x52\x5e\x5b\x13\x60\x70\x6c\x63\x5\x2c\x2b\x67\x2a\x30\x6a\x27\x23\x3a\x2b\x3c\x24\x71\x3a\x36\x35\x39\x22\x3f"+0xf82bc5fb).s);
			pMenu->AddEntry(/*  Lock*/XorStr<0x47,7,0xdda8de77>("\x67\x68\x5\x25\x28\x27"+0xdda8de77).s, &cvars.Aimlock, 0, 1, 1, /*Locks on the player you are currently aiming at*/XorStr<0x64,48,0xf994cacb>("\x28\xa\x5\xc\x1b\x49\x5\x5\x4c\x19\x6\xa\x50\x1\x1e\x12\xd\x10\x4\x57\x1\x16\xf\x5b\x1d\xf\x1b\x5f\xe3\xf4\xf0\xf1\xe1\xeb\xf2\xeb\xf1\xa9\xeb\xe2\xe1\xe4\xe0\xe8\xb0\xf0\xe6"+0xf994cacb).s);
			pMenu->AddEntry(/*  Aimpoint*/XorStr<0xB3,11,0x0D99B559>("\x93\x94\xF4\xDF\xDA\xC8\xD6\xD3\xD5\xC8"+0x0D99B559).s, &cvars.Aimpoint, 0, 3, 1, /*Choose where the aimbot should aim on like head, chest, ...*/XorStr<0x93,60,0x2a60fb61>("\xd0\xfc\xfa\xf9\xe4\xfd\xb9\xed\xf3\xf9\xef\xfb\xbf\xd4\xc9\xc7\x83\xc5\xcc\xcb\xc5\xc7\xdd\x8a\xd8\xc4\xc2\xdb\xc3\xd4\x91\xd3\xda\xd9\x95\xd9\xd9\x98\xd5\xd3\xd0\xd9\x9d\xd6\xda\xa1\xa5\xee\xe3\xa7\xad\xa3\xb4\xbc\xe5\xea\xe5\xe2\xe3"+0x2a60fb61).s);
			pMenu->AddEntry(/*  Aim Thru*/XorStr<0x24,11,0x79D5B4B8>("\x04\x05\x67\x4E\x45\x09\x7E\x43\x5E\x58"+0x79D5B4B8).s, &cvars.Aimthru, 0, 1, 1, /*Aimbot aims through walls*/XorStr<0xb8,26,0xe81a8482>("\xf9\xd0\xd7\xd9\xd3\xc9\x9e\xde\xa9\xac\xb1\xe3\xb0\xad\xb4\xa8\xbd\xae\xa2\xeb\xbb\xac\xa2\xa3\xa3"+0xe81a8482).s);
			pMenu->AddEntry(/*  Autoshoot*/XorStr<0x97,12,0xF6F39451>("\xB7\xB8\xD8\xEF\xEF\xF3\xEE\xF6\xF0\xCF\xD5"+0xF6F39451).s, &cvars.Autoshoot, 0, 1, 1, /*Aimbot autoshoots when it found a target*/XorStr<0xbb,41,0x8721ee21>("\xfa\xd5\xd0\xdc\xd0\xb4\xe1\xa3\xb6\xb0\xaa\xb5\xaf\xa7\xa6\xbe\xb8\xec\xba\xa6\xaa\xbe\xf1\xbb\xa7\xf4\xb3\xb9\xa2\xb6\xbd\xfa\xba\xfc\xa9\xbf\xad\x87\x84\x96"+0x8721ee21).s);
			//pMenu->AddEntry("  Knifebot", &cvars.Knifebot, 0, 1, 1);
			//pMenu->AddEntry("  Soft", &cvars.OPK, 0, 1, 1);
		}

		pMenu->AddEntry(/*-ESP*/XorStr<0x27,5,0x835A96AA>("\x0A\x6D\x7A\x7A"+0x835A96AA).s, &cvars.ESP, 0, 1, 1, /*ESP settings*/XorStr<0x4a,13,0x4612487a>("\xf\x18\x1c\x6d\x3d\x2a\x24\x25\x3b\x3d\x33\x26"+0x4612487a).s);
		if(cvars.ESP) {
			pMenu->AddEntry(/*  Enemy only*/XorStr<0x5b,13,0x57bd8ac0>("\x7b\x7c\x18\x30\x3a\xd\x18\x42\xc\xa\x9\x1f"+0x57bd8ac0).s, &cvars.EnemyOnly, 0, 1, 1, /*Display ESP only on enemies*/XorStr<0xac,28,0x43137ae0>("\xe8\xc4\xdd\xdf\xdc\xd0\xcb\x93\xf1\xe6\xe6\x97\xd7\xd7\xd6\xc2\x9c\xd2\xd0\x9f\xa5\xaf\xa7\xae\xad\xa0\xb5"+0x43137ae0).s);
			pMenu->AddEntry(/*  Name*/XorStr<0x2C,7,0x34161467>("\x0C\x0D\x60\x4E\x5D\x54"+0x34161467).s, &cvars.Name, 0, 1, 1, /*Displays the player name*/XorStr<0x4b,25,0x167ce37e>("\xf\x25\x3e\x3e\x23\x31\x28\x21\x73\x20\x3d\x33\x77\x28\x35\x3b\x22\x39\x2f\x7e\x31\x1\xc\x7"+0x167ce37e).s);
			pMenu->AddEntry(/*  Distance*/XorStr<0x3D,11,0x17FDEFEE>("\x1D\x1E\x7B\x29\x32\x36\x22\x2A\x26\x23"+0x17FDEFEE).s, &cvars.Distance, 0, 1, 1, /*Displays the player distance*/XorStr<0x4b,29,0x167d18e5>("\xf\x25\x3e\x3e\x23\x31\x28\x21\x73\x20\x3d\x33\x77\x28\x35\x3b\x22\x39\x2f\x7e\x3b\x9\x12\x16\x2\xa\x6\x3"+0x167d18e5).s);
			pMenu->AddEntry(/*  Health*/XorStr<0x9b,9,0x98cd68ff>("\xbb\xbc\xd5\xfb\xfe\xcc\xd5\xca"+0x98cd68ff).s, &cvars.Health, 0, 2, 1, /*Shows you the health and armorpoints from players. Setting 0 displays it as text and setting 1 as bars*/XorStr<0x26,103,0xbdbe568b>("\x75\x4f\x47\x5e\x59\xb\x55\x42\x5b\xf\x44\x59\x57\x13\x5c\x50\x57\x5b\x4c\x51\x1a\x5a\x52\x59\x1e\x5e\x32\x2c\x2d\x31\x34\x2a\x2f\x29\x3c\x3a\x6a\x2d\x3e\x22\x23\x6f\x20\x3d\x33\x2a\x31\x27\x25\x79\x78\xa\x3f\x2f\x28\x34\x30\x38\x40\x51\x42\x7\xd\x16\x16\xb\x9\x10\x19\x4b\x5\x19\x4e\xe\x3\x51\x6\x16\xc\x1\x56\x16\x16\x1d\x5a\x8\x19\x9\xa\x16\xee\xe6\xa2\xb2\xa4\xe4\xf5\xa7\xea\xe8\xf8\xf8"+0xbdbe568b).s);
			pMenu->AddEntry(/*  Line*/XorStr<0xaf,7,0x131447e2>("\x8f\x90\xfd\xdb\xdd\xd1"+0x131447e2).s, &cvars.Line, 0, 2, 1, /*Lines from you to the players*/XorStr<0x8b,30,0x558722be>("\xc7\xe5\xe3\xeb\xfc\xb0\xf7\xe0\xfc\xf9\xb5\xef\xf8\xed\xb9\xee\xf4\xbc\xe9\xf6\xfa\x80\xd1\xce\xc2\xdd\xc0\xd4\xd4"+0x558722be).s);
			pMenu->AddEntry(/*  Box*/XorStr<0xF4,6,0x9C0A2C6D>("\xD4\xD5\xB4\x98\x80"+0x9C0A2C6D).s, &cvars.Box, 0, 10000, 100, /*Displays a box around the player*/XorStr<0x68,33,0xff65066>("\x2c\x0\x19\x1b\x0\xc\x17\x1c\x50\x10\x52\x11\x1b\xd\x56\x16\xa\x16\xf\x15\x18\x5d\xa\x17\xe5\xa1\xf2\xef\xe5\xfc\xe3\xf5"+0xff65066).s);
			//pMenu->AddEntry("  Bone", &cvars.Bone, 0, 1, 1);
			pMenu->AddEntry(/*  Pickup*/XorStr<0x6D,9,0x40F6EE89>("\x4D\x4E\x3F\x19\x12\x19\x06\x04"+0x40F6EE89).s, &cvars.Pickup, 0, 1, 1, /*Shows all pickups in the world*/XorStr<0xd7,31,0x6d6ea03b>("\x84\xb0\xb6\xad\xa8\xfc\xbc\xb2\xb3\xc0\x91\x8b\x80\x8f\x90\x96\x94\xc8\x80\x84\xcb\x98\x85\x8b\xcf\x87\x9e\x80\x9f\x90"+0x6d6ea03b).s);
			pMenu->AddEntry(/*  Nades*/XorStr<0x35,8,0xFDE44EE8>("\x15\x16\x79\x59\x5D\x5F\x48"+0xFDE44EE8).s, &cvars.Nades, 0, 2, 1, /*Displays nades and rockets*/XorStr<0xdf,27,0xdb41e75>("\x9b\x89\x92\x92\x8f\x85\x9c\x95\xc7\x86\x88\x8e\x8e\x9f\xcd\x8f\x81\x94\xd1\x80\x9c\x97\x9e\x93\x83\x8b"+0xdb41e75).s);
			pMenu->AddEntry(/*  Radar*/XorStr<0xEC,8,0xADD3E183>("\xCC\xCD\xBC\x8E\x94\x90\x80"+0xADD3E183).s, &cvars.Radar, 0, 1, 1, /*Lets you see the players on the radar*/XorStr<0x79,38,0x4345dd44>("\x35\x1f\xf\xf\x5d\x7\x10\xf5\xa1\xf1\xe6\xe1\xa5\xf2\xef\xed\xa9\xfa\xe7\xed\xf4\xeb\xfd\xe3\xb1\xfd\xfd\xb4\xe1\xfe\xf2\xb8\xeb\xfb\xff\xfd\xef"+0x4345dd44).s);
		}

		pMenu->AddEntry(/*-Misc*/XorStr<0x6B,6,0xC0642AA5>("\x46\x21\x04\x1D\x0C"+0xC0642AA5).s, &cvars.Misc, 0, 1, 1, /*Miscellaneous stuff*/XorStr<0xad,20,0x79dfac13>("\xe0\xc7\xdc\xd3\xd4\xde\xdf\xd5\xdb\xd3\xd8\xcd\xca\x9a\xc8\xc8\xc8\xd8\xd9"+0x79dfac13).s);
		if(cvars.Misc) {
			pMenu->AddEntry(/*  Shoot Thru*/XorStr<0xe8,13,0xe44bb481>("\xc8\xc9\xb9\x83\x83\x82\x9a\xcf\xa4\x99\x80\x86"+0xe44bb481).s, &cvars.SuperBullets, 0, 1, 1, /*You can shoot through walls*/XorStr<0x51,28,0xe719b481>("\x8\x3d\x26\x74\x36\x37\x39\x78\x2a\x32\x34\x33\x29\x7e\x2b\x8\x13\xd\x16\x3\xd\x46\x10\x9\x5\x6\x18"+0xe719b481).s);
			pMenu->AddEntry(/*  Hover*/XorStr<0xfa,8,0xc62dc72e>("\xda\xdb\xb4\x92\x88\x9a\x72"+0xc62dc72e).s, &cvars.Hover, 0, 255, 1, /*You can 'hover'. Use the virtual key list to set the toggle key*/XorStr<0x28,64,0x27c026c1>("\x71\x46\x5f\xb\x4f\x4c\x40\xf\x17\x59\x5d\x45\x51\x47\x11\x19\x18\x6c\x49\x5e\x1c\x49\x56\x5a\x60\x37\x2b\x31\x30\x30\x27\x2b\x68\x22\x2f\x32\x6c\x21\x27\x3c\x24\x71\x26\x3c\x74\x26\x33\x23\x78\x2d\x32\x3e\x7c\x29\x31\x38\x7\xd\x7\x43\xf\x0\x1f"+0x27c026c1).s);
			//pMenu->AddEntry(/*  TPickup*/XorStr<0xc3,10,0xf38e2af7>("\xe3\xe4\x91\x96\xae\xab\xa2\xbf\xbb"+0xf38e2af7).s, &cvars.TelePickup, 0, 1, 1);
			pMenu->AddEntry(/*  Statsbox*/XorStr<0x5E,11,0xB4FC1527>("\x7E\x7F\x33\x15\x03\x17\x17\x07\x09\x1F"+0xB4FC1527).s, &cvars.Statsbox, 0, 1, 1, /*Displays some stats*/XorStr<0xc9,20,0x9461c72e>("\x8d\xa3\xb8\xbc\xa1\xaf\xb6\xa3\xf1\xa1\xbc\xb9\xb0\xf6\xa4\xac\xb8\xae\xa8"+0x9461c72e).s);
			//pMenu->AddEntry(/*  Antikick*/XorStr<0x90,11,0x164D0401>("\xB0\xB1\xD3\xFD\xE0\xFC\xFD\xFE\xFB\xF2"+0x164D0401).s, &cvars.Antikick, 0, 1, 1);
			//pMenu->AddEntry(/*  Unl Respawn*/XorStr<0x23,14,0x344B51F2>("\x03\x04\x70\x48\x4B\x08\x7B\x4F\x58\x5C\x4C\x59\x41"+0x344B51F2).s, &cvars.UnlRespawn, 0, 1, 1);
			//pMenu->AddEntry(/*  Inst. Spawn*/XorStr<0x6E,14,0xE73877A7>("\x4E\x4F\x39\x1F\x01\x07\x5A\x55\x25\x07\x19\x0E\x14"+0xE73877A7).s, &cvars.NoRespawnTime, 0, 1, 1);
			//pMenu->AddEntry("  Floating", &cvars.Hover, 0, 1, 1);
			pMenu->AddEntry(/*  Glitcher*/XorStr<0x9D,11,0xCDDDDC3C>("\xBD\xBE\xD8\xCC\xC8\xD6\xC0\xCC\xC0\xD4"+0xCDDDDC3C).s, &cvars.Glitcher, 0, 20, 1, /*Press X to glitch*/XorStr<0x8f,18,0x8a24f1be>("\xdf\xe2\xf4\xe1\xe0\xb4\xcd\xb6\xe3\xf7\xb9\xfd\xf7\xf5\xe9\xfd\xf7"+0x8a24f1be).s);
		//	pMenu->AddEntry("Soft", &cvars.OPK, 0, 2, 1);
			pMenu->AddEntry(/*  Telekill*/XorStr<0xCB,11,0x6B88B571>("\xEB\xEC\x99\xAB\xA3\xB5\xBA\xBB\xBF\xB8"+0x6B88B571).s, &cvars.Telekill, 0, 1, 1, /*Turn on to visually teleport you infront of an enemy*/XorStr<0x9a,53,0xcb9bcdce>("\xce\xee\xee\xf3\xbe\xf0\xce\x81\xd6\xcc\x84\xd3\xcf\xd4\xdd\xc8\xc6\xc7\xd5\x8d\xda\xca\xdc\xd4\xc2\xdc\xc6\xc1\x96\xce\xd7\xcc\x9a\xd2\xd2\xdb\xcc\xd0\xae\xb5\xe2\xac\xa2\xe5\xa7\xa9\xe8\xac\xa4\xae\xa1\xb4"+0xcb9bcdce).s);
			pMenu->AddEntry(/*  Ghostmode*/XorStr<0x0E,12,0x2E60C64E>("\x2E\x2F\x57\x79\x7D\x60\x60\x78\x79\x73\x7D"+0x2E60C64E).s, &bGhostmode, 0, 1, 1, /*Use the arrow keys to fly around the world*/XorStr<0x41,43,0xd842f75>("\x14\x31\x26\x64\x31\x2e\x22\x68\x28\x38\x39\x23\x3a\x6e\x24\x35\x28\x21\x73\x20\x3a\x76\x31\x34\x20\x7a\x3a\x2e\x32\x2b\x31\x4\x41\x16\xb\x1\x45\x11\x8\x1a\x5\xe"+0xd842f75).s);
			pMenu->AddEntry(/*  Speedhack*/XorStr<0x14,12,0xEBB2C209>("\x34\x35\x45\x67\x7D\x7C\x7E\x73\x7D\x7E\x75"+0xEBB2C209).s, &cvars.Speedhack, 0, 20, 1, /*Lets you move faster*/XorStr<0x35,21,0x990133>("\x79\x53\x43\x4b\x19\x43\x54\x49\x1d\x53\x50\x36\x24\x62\x25\x25\x36\x32\x22\x3a"+0x990133).s);
			pMenu->AddEntry(/*  Norecoil*/XorStr<0x0A,11,0x7ECCD81C>("\x2A\x2B\x42\x62\x7C\x6A\x73\x7E\x7B\x7F"+0x7ECCD81C).s, &cvars.Norecoil, 0, 1, 1, /*No weapon recoil*/XorStr<0xae,17,0x13e016e1>("\xe0\xc0\x90\xc6\xd7\xd2\xc4\xda\xd8\x97\xca\xdc\xd9\xd4\xd5\xd1"+0x13e016e1).s);
			pMenu->AddEntry(/*  Nospread*/XorStr<0x86,11,0x967ED44C>("\xA6\xA7\xC6\xE6\xF9\xFB\xFE\xE8\xEF\xEB"+0x967ED44C).s, &cvars.Nospread, 0, 1, 1, /*No weapon spread*/XorStr<0xf,17,0xab3e3f>("\x41\x7f\x31\x65\x76\x75\x65\x79\x79\x38\x6a\x6a\x69\x79\x7c\x7a"+0xab3e3f).s);
			pMenu->AddEntry(/*  Noreload*/XorStr<0xED,11,0x117CC048>("\xCD\xCE\xA1\x9F\x83\x97\x9F\x9B\x94\x92"+0x117CC048).s, &cvars.Noreload, 0, 1, 1, /*No weapon reload*/XorStr<0xbc,17,0x8454ba52>("\xf2\xd2\x9e\xc8\xa5\xa0\xb2\xac\xaa\xe5\xb4\xa2\xa4\xa6\xab\xaf"+0x8454ba52).s);
			pMenu->AddEntry(/*  Sat. Chams*/XorStr<0x91,13,0xE08E4AAC>("\xB1\xB2\xC0\xF5\xE1\xB8\xB7\xDB\xF1\xFB\xF6\xEF"+0xE08E4AAC).s, &cvars.SatChams, 0, 1, 1, /*NX Chams*/XorStr<0xd3,9,0x29ea06>("\x9d\x8c\xf5\x95\xbf\xb9\xb4\xa9"+0x29ea06).s);
			pMenu->AddEntry(/*  Weapon Range*/XorStr<0xFE,15,0x9B6E32A2>("\xDE\xDF\x57\x64\x63\x73\x6B\x6B\x26\x55\x69\x67\x6D\x6E"+0x9B6E32A2).s, &cvars.WeaponRange, 0, 1, 1, /*Weapon has no fire range. Also range knife attacks possible.*/XorStr<0x80,61,0xBDFA0F35>("\xD7\xE4\xE3\xF3\xEB\xEB\xA6\xEF\xE9\xFA\xAA\xE5\xE3\xAD\xE8\xE6\xE2\xF4\xB2\xE1\xF5\xFB\xF1\xF2\xB6\xB9\xDB\xF7\xEF\xF2\xBE\xED\xC1\xCF\xC5\xC6\x84\xCE\xC8\xCE\xCE\xCC\x8A\xCA\xD8\xD9\xCF\xCC\xDB\xC2\x92\xC3\xDB\xC6\xC5\xDE\xDA\xD5\xDF\x95"+0xBDFA0F35).s);
			pMenu->AddEntry(/*  Warnings*/XorStr<0x6C,11,0x9801492E>("\x4C\x4D\x39\x0E\x02\x1F\x1B\x1D\x13\x06"+0x9801492E).s, &cvars.Warnings, 0, 1, 1);
		}
		pMenu->AddEntry(/*-Commands*/XorStr<0xA5,10,0x3E87DFC2>("\x88\xE5\xC8\xC5\xC4\xCB\xC5\xC8\xDE"+0x3E87DFC2).s, &cvars.Commands, 0, 1, 1, /*Console commands*/XorStr<0x41,17,0xa67374a7>("\x2\x2d\x2d\x37\x2a\x2a\x22\x68\x2a\x25\x26\x21\x2c\x20\x2b\x23"+0xa67374a7).s);
		if(cvars.Commands) {
			//pMenu->AddEntry(/*  Boxes*/XorStr<0x3B,8,0x3C5B2893>("\x1B\x1C\x7F\x51\x47\x25\x32"+0x3C5B2893).s, &cvars.Boxes, 0, 1, 1);
			pMenu->AddEntry(/*  Tracers*/XorStr<0xF5,10,0xB32676B7>("\xD5\xD6\xA3\x8A\x98\x99\x9E\x8E\x8E"+0xB32676B7).s, &cvars.Tracers, 0, 1, 1, /*Shows lines where you shooted at*/XorStr<0xdd,33,0xd874ba7>("\x8e\xb6\xb0\x97\x92\xc2\x8f\x8d\x8b\x83\x94\xc8\x9e\x82\x8e\x9e\x88\xce\x96\x9f\x84\xd2\x80\x9c\x9a\x99\x83\x9d\x9d\xda\x9a\x88"+0xd874ba7).s);
			pMenu->AddEntry(/*  Superjump*/XorStr<0xD4,12,0x71E7A497>("\xF4\xF5\x85\xA2\xA8\xBC\xA8\xB1\xA9\xB0\xAE"+0x71E7A497).s, &cvars.SJump, 0, 1, 1, /*Lets you jump higher*/XorStr<0xa4,21,0xab72d8>("\xe8\xc0\xd2\xd4\x88\xd0\xc5\xde\x8c\xc7\xdb\xc2\xc0\x91\xda\xda\xd3\xdd\xd3\xc5"+0xab72d8).s);
			pMenu->AddEntry(/*  Show FPS*/XorStr<0xAC,11,0xBBD5DEB3>("\x8C\x8D\xFD\xC7\xDF\xC6\x92\xF5\xE4\xE6"+0xBBD5DEB3).s, &cvars.ShowFps, 0, 1, 1, /*View your frames per second*/XorStr<0x26,28,0x56bd5724>("\x70\x4e\x4d\x5e\xa\x52\x43\x58\x5c\xf\x56\x43\x53\x5e\x51\x46\x16\x47\x5d\x4b\x1a\x48\x59\x5e\x51\x51\x24"+0x56bd5724).s);
			pMenu->AddEntry(/*  Gravity*/XorStr<0x76,10,0x9637C68A>("\x56\x57\x3F\x0B\x1B\x0D\x15\x09\x07"+0x9637C68A).s, &cvars.Gravity, 0, 1, 1, /*Less gravity*/XorStr<0x30,13,0xc5fa9363>("\x7c\x54\x41\x40\x14\x52\x44\x56\x4e\x50\x4e\x42"+0xc5fa9363).s);
			pMenu->AddEntry(/*  Self Kill*/XorStr<0x68,12,0x7EBD8716>("\x48\x49\x39\x0E\x00\x0B\x4E\x24\x19\x1D\x1E"+0x7EBD8716).s, &cvars.SelfKill, 0, 1, 1, /*Kill yourself when stuck or whatever*/XorStr<0xbd,37,0x8823f057>("\xf6\xd7\xd3\xac\xe1\xbb\xac\xb1\xb7\xb5\xa2\xa4\xaf\xea\xbc\xa4\xa8\xa0\xef\xa3\xa5\xa7\xb0\xbf\xf5\xb9\xa5\xf8\xae\xb2\xba\xa8\xb8\xa8\xba\x92"+0x8823f057).s);
			pMenu->AddEntry(/*  Wireframe*/XorStr<0x15,12,0x9A347DF3>("\x35\x36\x40\x71\x6B\x7F\x7D\x6E\x7C\x73\x7A"+0x9A347DF3).s, &cvars.Wrireframe, 0, 1, 1, /*Wireframe mode*/XorStr<0xc4,15,0x28c329f5>("\x93\xac\xb4\xa2\xae\xbb\xab\xa6\xa9\xed\xa3\xa0\xb4\xb4"+0x28c329f5).s);
			pMenu->AddEntry(/*  Pickup*/XorStr<0x9D,9,0x0F2F1AC7>("\xBD\xBE\xCF\xC9\xC2\xC9\xD6\xD4"+0x0F2F1AC7).s, &cvars.PickupCmd, 0, 1, 1, /*Pick up weapon from far away*/XorStr<0x38,29,0x98686a2>("\x68\x50\x59\x50\x1c\x48\x4e\x1f\x37\x24\x23\x33\x2b\x2b\x66\x21\x3a\x26\x27\x6b\x2a\x2c\x3c\x6f\x31\x26\x33\x2a"+0x98686a2).s);
		}
		pMenu->AddEntry(/*-Settings*/XorStr<0x22,10,0xeced2021>("\xf\x70\x41\x51\x52\x4e\x46\x4e\x59"+0xeced2021).s, &cvars.Settings, 0, 1, 1, /*Load/Save settings*/XorStr<0x78,19,0x77a9abdd>("\x34\x16\x1b\x1f\x53\x2e\x1f\x9\xe5\xa1\xf1\xe6\xf0\xf1\xef\xe9\xef\xfa"+0x77a9abdd).s);
		if(cvars.Settings) {
			pMenu->AddEntry(/*  Load*/XorStr<0xb6,7,0x817fe91b>("\x96\x97\xf4\xd6\xdb\xdf"+0x817fe91b).s, &cvars.SettingsLoad, 0, 1, 1, /*Loads you saved settings*/XorStr<0x4f,25,0xb54c82b4>("\x3\x3f\x30\x36\x20\x74\x2c\x39\x22\x78\x2a\x3b\x2d\x39\x39\x7e\x2c\x5\x15\x16\xa\xa\x2\x15"+0xb54c82b4).s);
			pMenu->AddEntry(/*  Save*/XorStr<0xc6,7,0x8c5d5ef7>("\xe6\xe7\x9b\xa8\xbc\xae"+0x8c5d5ef7).s, &cvars.SettingsSave, 0, 1, 1, /*Saves your settings you have done on the menu*/XorStr<0xbf,46,0xbb218823>("\xec\xa1\xb7\xa7\xb0\xe4\xbc\xa9\xb2\xba\xe9\xb9\xae\xb8\xb9\xa7\xa1\xb7\xa2\xf2\xaa\xbb\xa0\xf6\xbf\xb9\xaf\xbf\xfb\xb8\xb2\xb0\xba\xc0\x8e\x8c\xc3\x90\x8d\x83\xc7\x85\x8c\x84\x9e"+0xbb218823).s);
			pMenu->AddEntry("  Clock", &cvars.Clock, 0, 1, 1, "Show a clock on the right-up corner");
			pMenu->AddEntry("  Font", &cvars.Font, 0, 10, 1, "Change font");
		}
	}

}

bool bAlive = false;
extern BYTE data[1000];

int uPrimcount = 0;
int uNumvertices = 0;

extern void __stdcall randomize();

//void MemoryDisplay(DWORD dwAddress)
//{
//	int lines = 0;
//	for(int i = 0; i < 0x150; i+=16) {
//		BYTE code[16] = {0};
//		memcpy(&code, (void*)(dwAddress+i), 16);
//
//		g_pTools->DrawFilledQuad(400, 400+(lines*15), 500, 15, black);
//		g_pTools->DrawString(400,400+(lines*15), green, DT_LEFT, "+%03X | %02X%02X%02X%02X %02X%02X%02X%02X %02X%02X%02X%02X %02X%02X%02X%02X | %c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c",
//			(i),
//			code[0],code[1],code[2],code[3],code[4],code[5],code[6],code[7],code[8],code[9],code[10],code[11],code[12],code[13], code[14], code[15], 
//			code[0],code[1],code[2],code[3],code[4],code[5],code[6],code[7],code[8],code[9],code[10],code[11],code[12],code[13], code[14], code[15]
//			);
//
//		lines++;
//	}
//}




//typedef bool (*IsConnected_t)(void);
//
//bool isIngame()
//{
//	//This is for CA EU, replace with your LTClient address
//	DWORD* dwLTBase = (DWORD*)0x377C4450;
//	IsConnected_t pConnected = *(IsConnected_t*)(*dwLTBase + 0x8C);
//
//	return pConnected();
//}

/*
004F90C6    8B86 C8000000   MOV EAX,DWORD PTR DS:[ESI+C8]
004F90CC    8B8E CC000000   MOV ECX,DWORD PTR DS:[ESI+CC]
004F90D2    8B96 D0000000   MOV EDX,DWORD PTR DS:[ESI+D0]
004F90D8    F3:0F1086 48010>MOVSS XMM0,DWORD PTR DS:[ESI+148]
004F90E0    53              PUSH EBX
004F90E1    57              PUSH EDI
*/
//DWORD xDk = 0x4F90D8;
//__declspec(naked) void OPK()
//{
//	if(g_pEngine->m_dwGamemode != 1337)
//	{
//		__asm mov eax, 0
//		__asm mov ecx, 0
//		__asm mov edx, 0
//	}
//	else {
//		__asm MOV EAX,DWORD PTR DS:[ESI+0xCC]
//		__asm MOV ECX,DWORD PTR DS:[ESI+0xC8]
//		__asm MOV EDX,DWORD PTR DS:[ESI+0xD0]
//	}
//
//	__asm jmp xDk
//}

extern DWORD dwVMTDisable;

DWORD dwInfoScreenTime = 0;

WNDPROC window;

#define HIWORDS(l) ((short)((DWORD_PTR)(l)>>16))
LRESULT CALLBACK WindowHook(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{	
	if(msg == WM_CLOSE) {
	
		return CallWindowProc(pMenu->window, hwnd, msg, wParam, lParam);
	}

	if(msg == WM_KEYDOWN && (wParam == VK_INSERT || wParam == VK_F12)) {
		pMenu->bMenuActive = !pMenu->bMenuActive;
	}

	if(pMenu->bMenuActive) {
	
		if(msg == WM_KEYDOWN) {
		
			pMenu->KeyHandling(wParam);
		}
		else if(msg == WM_MOUSEWHEEL) {
		
			if(HIWORDS(wParam) > 0) {
				pMenu->KeyHandling(VK_UP);
			}
			else {
				pMenu->KeyHandling(VK_DOWN);
			}
		}

		//uncomment this to block all other input while the menu is open
		//return DefWindowProc(hwnd, msg, wParam, lParam); 
	}

	return CallWindowProc(pMenu->window, hwnd, msg, wParam, lParam);
}

extern void HookLoadingScreen();

void GhostmodeWarning(LTVector playerposition, ClientInfo* info, ClientInfo* linfo)
{
	if(!cvars.Warnings) return;

	if(bGhostmode) {
	
		if(info->team != linfo->team && !info->isdead && !linfo->isdead) {
			bool canSeeMe = g_pEngine->IsVisible(playerposition, g_pEngine->m_vOldRealPosition, 0);
			if(canSeeMe) {
				g_pTools->DrawString2(cvars.w/2, 200, red, DT_CENTER, /*WARNING: %s CAN SEE YOUR REAL BODY !!!*/XorStr<0xD4,39,0x06704EF4>("\x83\x94\x84\x99\x91\x97\x9D\xE1\xFC\xF8\xAD\xFF\xA3\xA0\xAC\xC3\xB7\xA0\xA3\xC7\xB1\xA6\xBF\xB9\xCC\xBF\xAB\xAE\xBC\xD1\xB0\xBC\xB0\xAC\xD6\xD6\xD9\xD8"+0x06704EF4).s, info->name);
			}
		}
	}
	else {
	
		if(g_pEngine->m_dwGamemode != 5) {
			if(info->team != linfo->team && !info->isdead && !linfo->isdead) {
				bool canSeeMe = g_pEngine->IsVisible(playerposition, g_pEngine->m_vLocalPosition, 0);
				if(canSeeMe) {
					g_pTools->DrawString2(cvars.w/2, 200, red, DT_CENTER, /*WARNING: %s CAN SEE YOU !!!*/XorStr<0x19,28,0x586740C9>("\x4E\x5B\x49\x52\x54\x50\x58\x1A\x01\x07\x50\x04\x66\x67\x69\x08\x7A\x6F\x6E\x0C\x74\x61\x7A\x10\x10\x13\x12"+0x586740C9).s, info->name);
				}
			}
		}
		else {
			if(!linfo->isdead && !info) {
			
				g_pTools->DrawString2(cvars.w/2, 200, red, DT_CENTER, /*WARNING: %s CAN SEE YOU !!!*/XorStr<0x19,28,0x586740C9>("\x4E\x5B\x49\x52\x54\x50\x58\x1A\x01\x07\x50\x04\x66\x67\x69\x08\x7A\x6F\x6E\x0C\x74\x61\x7A\x10\x10\x13\x12"+0x586740C9).s, /*NPC*/XorStr<0x2F,4,0xEAE696C9>("\x61\x60\x72"+0xEAE696C9).s);
			}
		}
	}
}

void GhostmodeInfo()
{
	static DWORD dwTime = 0;

	if(bGhostmode) {
		
		if(dwTime == 0) {
			dwTime = GetTickCount();
		}

		if((dwTime+5000) > GetTickCount()) {
			g_pTools->DrawSprite(cvars.w/2-75, cvars.h-230);
		}
	}
	/*else {
		dwTime = 0;
	}*/
}

class CAutoMessage2
{
public:
	// Constructors
	CAutoMessage2() : m_pMsg(NULL)
	{
		Init();
	}
	~CAutoMessage2()
	{
		Term();
	}
	CAutoMessage2(const ILTMessage_Read &cMsg) : m_pMsg(NULL)
	{
		Init();
		CLTMsgRef_Read cReadMsg(cMsg.Clone());
		WriteMessage(cReadMsg);
	}
	// Useful constructors for writing out one element of data
	// Be careful to make sure that the type is the one you expect
	CAutoMessage2(const char *pStr) : m_pMsg(NULL)
	{
		Init();
		WriteString(pStr);
	}
	CAutoMessage2(const wchar_t *pStr) : m_pMsg(NULL)
	{
		Init();
		WriteWString(pStr);
	}
	template <class T>
		CAutoMessage2(const T &tValue) : m_pMsg(NULL)
	{
		Init();
		WriteType(tValue);
	}


	// Call this function to let go of the current message and make a new one
	// (e.g. when using the same CAutoMessage variable for multiple message calls.)
	void Reset() { Init(); }
	bool IsValid() { return m_pMsg != NULL ; }

	// Casting operators to get back to ILTMessage_Write
	inline operator ILTMessage_Write*() { return m_pMsg; }
	inline operator const ILTMessage_Write*() const { return m_pMsg; }
	inline operator ILTMessage_Write&() { return *m_pMsg; }
	inline operator const ILTMessage_Write&() const { return *m_pMsg; }

	// Wrappers for the rest of ILTMessage_Write's functions
	inline CLTMsgRef_Read Read() { return CLTMsgRef_Read(m_pMsg->Read()); }
	inline uint32 Size() const { return m_pMsg->Size(); }
	inline void WriteBits(uint32 nValue, uint32 nSize) { m_pMsg->WriteBits(nValue, nSize); }
	inline void WriteBits64(uint64 nValue, uint32 nSize) { m_pMsg->WriteBits64(nValue, nSize); }
	inline void WriteData(const void *pData, uint32 nSize) { m_pMsg->WriteData(pData, nSize); }
	inline void WriteMessage(ILTMessage_Read *pMsg) { m_pMsg->WriteMessage(pMsg); }
	inline void WriteMessageRaw(ILTMessage_Read *pMsg) { m_pMsg->WriteMessageRaw(pMsg); }
	inline void WriteString(const char *pString) { m_pMsg->WriteString(pString); }
	inline void WriteWString(const wchar_t *pString) { m_pMsg->WriteWString(pString); }
	inline void WriteCompLTVector(const LTVector &vVec) { m_pMsg->WriteCompLTVector(vVec); }
	inline void WriteCompPos(const LTVector &vPos) { m_pMsg->WriteCompPos(vPos); }
	inline void WriteCompLTRotation(const LTRotation &cRotation) { m_pMsg->WriteCompLTRotation(cRotation); }
	inline void WriteObject(HOBJECT hObj) { m_pMsg->WriteObject(hObj); }
	inline void WriteYRotation(const LTRotation &cRotation) { m_pMsg->WriteYRotation(cRotation); }
	inline void WriteDatabaseRecord( IDatabaseMgr *pDatabase, HRECORD hRecord ) { m_pMsg->WriteDatabaseRecord( pDatabase, hRecord ); }
	inline void Writebool(bool bValue) { WriteBits(bValue ? 1 : 0, 1); }
	inline void Writeuint8(uint8 nValue) { WriteBits(nValue, 8); }
	inline void Writeuint16(uint16 nValue) { WriteBits(nValue, 16); }
	inline void Writeuint32(uint32 nValue) { WriteBits(nValue, 32); }
	inline void Writeuint64(uint64 nValue) { WriteBits64(nValue, 64); }
	inline void Writeint8(int8 nValue) { WriteBits((uint32)nValue, 8); }
	inline void Writeint16(int16 nValue) { WriteBits((uint32)nValue, 16); }
	inline void Writeint32(int32 nValue) { WriteBits((uint32)nValue, 32); }
	inline void Writeint64(int32 nValue) { WriteBits64((uint64)nValue, 32); }
	inline void Writefloat(float fValue) { WriteBits(reinterpret_cast<const uint32&>(fValue), 32); }
	inline void Writedouble(double fValue) { WriteBits64(reinterpret_cast<const uint64&>(fValue), 64); }
	inline void WriteLTVector(const LTVector &vValue) { WriteType(vValue); }
	inline void WriteLTRotation(const LTRotation &cValue) { WriteType(cValue); }
	inline void WriteLTRigidTransform(const LTRigidTransform &tfValue) { WriteType( tfValue ); }
	inline void WriteLTTransform(const LTTransform &tfValue) { WriteType( tfValue ); }
	inline void WriteLTPolarCoord(const LTPolarCoord &polarCoord) { m_pMsg->WriteLTPolarCoord( polarCoord ); }
	inline void WriteCompLTPolarCoord(const LTPolarCoord &polarCoord) { m_pMsg->WriteCompLTPolarCoord( polarCoord ); }
	template <class T>
		inline void WriteType(const T &tValue) { m_pMsg->WriteType(tValue); }
private:
	inline void Init()
	{
		Term();
		m_pMsg = NULL;
		ILTMessage_Write *pMsg;

		DWORD blaa = (DWORD)g_pEngine->m_pLTClient->LTCommonClient();
		__asm mov ecx, blaa

		LTRESULT nResult = g_pEngine->m_pLTCommon->CreateMessage(pMsg);
		if (nResult == LT_OK)
			m_pMsg = pMsg;
		ASSERT(nResult == LT_OK);
	}
	inline void Term()
	{
		m_pMsg = NULL;
	}
	CLTMsgRef_Write m_pMsg;
};

//void DoServerCrash()
//{
//	if(GetAsyncKeyState(VK_NUMPAD5) & 1){
//		CAutoMessage2 Msg;
//		Msg.Writeuint8(104);
//		Msg.WriteWString((wchar_t*) L"Come get some come get some come get some come get some ");
//		g_pEngine->m_pLTBase->pLTClient->SendToServer(Msg.Read(), MESSAGE_GUARANTEED);
//	}
//}

int GetTextWidth(char* text)
{
	HDC hdc = m_pFont->GetDC();
	SIZE size;
	GetTextExtentPoint32(hdc, text, (int)strlen(text), &size);

	return size.cx;
}

HRESULT __stdcall hkEndScene(LPDIRECT3DDEVICE9 pDevice)
{

	HRESULT hRet = 0;

	static bool bInit = false;
	if(!bInit)
	{
//		keyboard = SetWindowsHookEx(WH_KEYBOARD, KeyProc, 0, GetCurrentThreadId());
		pMenu->window = (WNDPROC)SetWindowLong(FindWindow(0, /*Combat_Arms*/XorStr<0x73,12,0x2A340DB5>("\x30\x1B\x18\x14\x16\x0C\x26\x3B\x09\x11\x0E"+0x2A340DB5).s), GWL_WNDPROC, (LONG)WindowHook ); 
		dwInfoScreenTime = GetTickCount();
		bInit = true;
		pMenu->bMenuActive = false;
		cvars.Detectable = 1;
		cvars.Statsbox = 0;
		cvars.Aimpoint = 0;
		cvars.EnemyOnly = 1;
		cvars.Aimfov = 360;
		cvars.Clock = 1;
		cvars.Font = 0;
		CreateThread(0, 0, (LPTHREAD_START_ROUTINE)HookLoadingScreen, 0, 0, 0);
		//Menu_Hook();

		//g_pTools->DetourFunc((PBYTE)0x4F90C6, (PBYTE)OPK, 6);

		cmdNospread.Add(/*PerturbRotationEffect*/XorStr<0x65,22,0x622d6330>("\x35\x3\x15\x1c\x1c\x18\x9\x3e\x2\x1a\xe\x4\x18\x1d\x1d\x31\x13\x10\x12\x1b\xd"+0x622d6330).s, 0, 3);
		cmdNospread.Add(/*PerturbIncreaseSpeed*/XorStr<0x72,21,0xa43dadb>("\x22\x16\x6\x1\x3\x5\x1a\x30\x14\x18\xe\x18\x1f\xc\xe5\xd2\xf2\xe6\xe1\xe1"+0xa43dadb).s, 0, 3);
		cmdNospread.Add(/*PerturbDecreaseSpeed*/XorStr<0x4d,21,0xb04b8081>("\x1d\x2b\x3d\x24\x24\x20\x31\x10\x30\x35\x25\x3d\x38\x29\x3e\xf\x2d\x3b\x3a\x4"+0xb04b8081).s, 0, 9);
		cmdNospread.Add(/*PerturbWalkPercent*/XorStr<0x5c,19,0x5cc2f48f>("\xc\x38\x2c\x2b\x15\x13\x0\x34\x5\x9\xd\x37\xd\x1b\x9\xe\x2\x19"+0x5cc2f48f).s, 0.5f, 3);
		cmdNospread.Add(/*PerturbFiringIncreaseSpeed*/XorStr<0x91,27,0x5cf7c42a>("\xc1\xf7\xe1\xe0\xe0\xe4\xf5\xde\xf0\xe8\xf2\xf2\xfa\xd7\xf1\xc3\xd3\xc7\xc2\xd7\xc0\xf5\xd7\xcd\xcc\xce"+0x5cf7c42a).s, 0, 3);
		cmdSatChams.Add(/*SkelModelStencil*/XorStr<0x69,17,0x34cc68ce>("\x3a\x1\xe\x0\x20\x1\xb\x15\x1d\x21\x7\x11\x1b\x15\x1e\x14"+0x34cc68ce).s, 1, 0);
		//cmdBoxes.Add(/*ModelDebug_DrawBoxes*/XorStr<0x17,21,0xae14e016>("\x5a\x77\x7d\x7f\x77\x58\x78\x7c\x6a\x47\x7e\x66\x51\x45\x52\x64\x48\x50\x4c\x59"+0xae14e016).s, 1, 0);
		cmdTracers.Add(/*ShowFirePath*/XorStr<0xf6,13,0x27f4c35b>("\xa5\x9f\x97\x8e\xbc\x92\x8e\x98\xae\x9e\x74\x69"+0x27f4c35b).s, 1, -1);
		cmdSuperjump.Add(/*JumpVel*/XorStr<0x3a,8,0x66c39a0>("\x70\x4e\x51\x4d\x68\x5a\x2c"+0x66c39a0).s, 660, 330);
		cmdShowFPS.Add(/*ShowFps*/XorStr<0xed,8,0xb885ba55>("\xbe\x86\x80\x87\xb7\x82\x80"+0xb885ba55).s, 1, 0);
		cmdGravity.Add(/*PlayerGravity*/XorStr<0x61,14,0xc661c862>("\x31\xe\x2\x1d\x0\x14\x20\x1a\x8\x1c\x2\x18\x14"+0xc661c862).s, 800, -1000);
		cmdSelfKill.Add(/*FragSelf*/XorStr<0x69,9,0x31099ce>("\x2f\x18\xa\xb\x3e\xb\x3\x16"+0x31099ce).s, 1, 0);
		cmdWireframe.Add(/*WireFrame*/XorStr<0x15,10,0xe1ad7ce2>("\x42\x7f\x65\x7d\x5f\x68\x7a\x71\x78"+0xe1ad7ce2).s, 1, 0);
		cmdPickup.Add(/*ActivationDistance*/XorStr<0x6d,19,0xcf9f56c>("\x2c\xd\x1b\x19\x7\x13\x7\x1d\x1a\x18\x33\x11\xa\xe\x1a\x12\x1e\x1b"+0xcf9f56c).s, 999999999, 100); 

		for(int i = 1; i <= 20; i++) {
			cmdSpeedhack.Add(/*FRunVel*/XorStr<0xa0,8,0x9d6a6b4>("\xe6\xf3\xd7\xcd\xf2\xc0\xca"+0x9d6a6b4).s, 285 + (float)i * 85, 285, i);
			cmdSpeedhack.Add(/*BRunVel*/XorStr<0x15,8,0xab784714>("\x57\x44\x62\x76\x4f\x7f\x77"+0xab784714).s, 285 + (float)i * 85, 285, i);
			cmdSpeedhack.Add(/*SRunVel*/XorStr<0xbb,8,0x53b98554>("\xe8\xee\xc8\xd0\xe9\xa5\xad"+0x53b98554).s, 285 + (float)i * 85, 285, i);
		}

		srand(GetTickCount());
	}

	cmdNospread.Execute();
	cmdSatChams.Execute();
	//cmdBoxes.Execute();
	cmdTracers.Execute();
	cmdSuperjump.Execute();
	cmdShowFPS.Execute();
	cmdGravity.Execute();
	cmdSelfKill.Execute();
	cmdWireframe.Execute();
	cmdSpeedhack.Execute();
	cmdPickup.Execute();

	//DoServerCrash();

	if(cvars.SettingsLoad) {
		ReadConfigFile();
		cvars.SettingsLoad = 0;
	}

	if(cvars.SettingsSave) {
		WriteConfigFile();
		cvars.SettingsSave = 0;
	}

	try { 

#define SETRADAR(ww, hh, xx, yy) if(cvars.w == ww && cvars.h == hh) { cvars.radarx = xx; cvars.radary = yy; }

	SETRADAR(1680, 1050, 240, 48)
	else SETRADAR(1600, 900, 240, 30)
	else SETRADAR(1440, 900, 228, 30)
	else SETRADAR(1360, 768, 215, 15)
	else SETRADAR(1280, 1024, 205, 45)
	else SETRADAR(1280, 960, 205, 45)
	else SETRADAR(1280, 800, 205, 18)
	else SETRADAR(1280, 768, 204, 13)
	else SETRADAR(1280, 720, 204, 8)
	else SETRADAR(1152, 864, 193, 28)
	else SETRADAR(1024, 768, 180, 15)
	else SETRADAR(960, 600, 177, 9)
	else SETRADAR(800, 600, 117, 9)
	else  {cvars.radarx = 240; cvars.radary = 48;}


	/*
	$+6F     >  833D 6CD60D10 0>CMP DWORD PTR DS:[100DD66C],2
	$+76     >  74 09           JE SHORT EHSvc.100709F1
	$+78     >  833D 68D60D10 0>CMP DWORD PTR DS:[100DD668],1
	$+7F     >  74 05           JE SHORT EHSvc.100709F6
	$+81     >  E9 A5050000     JMP EHSvc.10070F9B
	*/

	//DWORD ehsvcc = (DWORD)GetModuleHandle("ehsvc.dll");

	//*(DWORD*)(ehsvcc+0xDD66C) = 2;
	if(dwVMTDisable)
		*(DWORD*)(dwVMTDisable) = 1;

	hRet = pEndScene(pDevice);

	
	if(CHECK(Red) || CHECK(Yellow) || CHECK(Blue) || CHECK(Green) || CHECK(White) || CHECK(Black))
	{
		GENERATE(Red, 255, 0, 0);
		GENERATE(Yellow, 255, 255, 0);
		GENERATE(Blue, 0, 0, 255);
		GENERATE(Green, 0, 255, 0);
		GENERATE(White, 255, 255, 255);
		GENERATE(Black, 0, 0, 0);
	}


	if(!m_pFont || IsBadReadPtr((void*)m_pFont, 4) || !m_pFont2 || IsBadReadPtr((void*)m_pFont2, 4)) {
		g_pTools->BuildFont(pDevice);
	}


	if(!pLine || IsBadReadPtr((void*)pLine, 4))
		pD3DXCreateLine(pDevice, &pLine); 

	//if(!g_pTools->m_tex || !g_pTools->m_sprite || IsBadReadPtr((void*)g_pTools->m_tex, 4) || IsBadReadPtr((void*)g_pTools->m_sprite, 4))
	//	g_pTools->CreateSprite(pDevice);
	
	pD3Ddev = pDevice;


	GhostmodeInfo();

	int textWidth = GetTextWidth(hackname);

	g_pTools->DrawFilledQuad(-1, -1, textWidth+10, 20, black);
	g_pTools->DrawString(5,4,green, 0, _s, hackname);

	if(cvars.Clock) {

		time_t timeLastUpdate = time(0);
		tm* tmLastUpdate = localtime(&timeLastUpdate);

		char buf[64];
		strftime(buf, 64, "%X", tmLastUpdate);

		textWidth = GetTextWidth(buf);
		g_pTools->DrawFilledQuad(cvars.w-(textWidth)-10, -1, textWidth+15, 20, black);
		g_pTools->DrawString(cvars.w-5, 4, green, DT_RIGHT, buf);
	}

	static int iFont = cvars.Font;

	Menu();

	if(iFont != cvars.Font) {

		m_pFont->Release();
		m_pFont = NULL;

		iFont = cvars.Font;
	}


	/*if(!g_pEngine->m_pLTBase || (g_pEngine->m_pLTBase && !g_pEngine->m_pLTBase->IsConnected())) {

		g_pTools->DrawSprite(cvars.w-130, 15);
	}*/

	/*g_pTools->DrawSprite(cvars.w-130, 15);*/

	//*(BYTE*)0x7D7130 = 0;

	//try {

	DWORD dwShell = (DWORD)GetModuleHandle(cshell);
	//


	D3DVIEWPORT9 oViewport;
	pDevice->GetViewport(&oViewport);
	cvars.w = oViewport.Width;
	cvars.h = oViewport.Height;


	//g_pTools->DrawFilledQuad(MouseX, MouseY, 5, 5, red);

	if(cvars.Crosshair) g_pTools->Crosshair(cvars.Crosshair);

	if(cvars.Statsbox) { 
		int statsbox_x = cvars.w/2 - 100;
		int statsbox_y = 70; 

		time_t rawtime;
		struct tm * timeinfo;
		time(&rawtime);
		timeinfo = localtime (&rawtime);


		if(g_pEngine->m_iDeaths == 0) {
			g_pEngine->m_fKDR = g_pEngine->m_iKills;
		} else {
			g_pEngine->m_fKDR = float(g_pEngine->m_iKills)/float(g_pEngine->m_iDeaths);
		}

		g_pTools->DrawFilledQuad(statsbox_x, statsbox_y, 180, 72, black);
		g_pTools->DrawQuad(statsbox_x, statsbox_y, 180, 72, white, 2);
		g_pTools->DrawQuad(statsbox_x, statsbox_y+20, 180, 1, white, 1);
		g_pTools->DrawString(statsbox_x+90, statsbox_y+3, green, DT_CENTER, /*Statsbox*/XorStr<0x46,9,0xac13df14>("\x15\x33\x29\x3d\x39\x29\x23\x35"+0xac13df14).s);

		g_pTools->DrawString(statsbox_x+10, statsbox_y+25, white, DT_LEFT, /*Time: %02d:%02d:%02d*/XorStr<0xF2,21,0x6D859CA8>("\xA6\x9A\x99\x90\xCC\xD7\xDD\xC9\xC8\x9F\xC6\xD8\xCE\xCD\x64\x3B\x27\x33\x36\x61"+0x6D859CA8).s, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
		g_pTools->DrawString(statsbox_x+10, statsbox_y+40, white, DT_LEFT, /*Alive: %d | Gamemode: %d*/XorStr<0x19,25,0x20FF6547>("\x58\x76\x72\x6A\x78\x24\x3F\x05\x45\x02\x23\x04\x62\x47\x4A\x4D\x44\x45\x4F\x49\x17\x0E\x0A\x54"+0x20FF6547).s, bAlive, g_pEngine->m_dwGamemode==1337?0:g_pEngine->m_dwGamemode);
		g_pTools->DrawString(statsbox_x+10, statsbox_y+55, white, DT_LEFT, /*K/D/R: %d/%d/%.2f*/XorStr<0xD0,18,0x69B3579E>("\x9B\xFE\x96\xFC\x86\xEF\xF6\xF2\xBC\xF6\xFF\xBF\xF3\xF8\xF0\xED\x86"+0x69B3579E).s, g_pEngine->m_iKills, g_pEngine->m_iDeaths, g_pEngine->m_fKDR);
	}
	
	if(g_pHackshield->isScanning == false) {
		g_pAddress->Restore();
		g_pAddress->Apply();
	}

	//WeaponInfos * info = (WeaponInfos*)0x377FD548;

	//if(info) {
	//	info->recoil1 = 0;
	//	info->recoil2 = 0;
	//	info->recoil3 = 0;
	//	info->recoil4 = 0;
	//}

	//int ohuh = 0;
	//if(g_pEngine->m_pLTBase)
	//	g_pEngine->m_pLTBase->GetGameMode(&ohuh);


	//if((dwInfoScreenTime + 10000) > GetTickCount() || !ohuh) {
		//g_pTools->DrawFilledQuad(0, 0, cvars.w, cvars.h, black);
		//g_pTools->DrawQuad(0, 0, cvars.w-3, cvars.h-3, white, 3);
	//}


	if(dwShell && dwUnlimitedRespawn && g_pEngine->m_pLTBase)
	{
		static bool bInitSpawn = false;	
		DWORD unlrespawn = *(DWORD*)dwUnlimitedRespawn;
		if(unlrespawn) {
			if(!g_pEngine->m_pLTBase->IsConnected()) {
				g_pEngine->m_dwGamemode = 1337;
				g_pEngine->m_iKills = 0;
				g_pEngine->m_iDeaths = 0;
				g_pEngine->m_fKDR = 0;
				bAlive = false;
			}
			else if(g_pEngine->m_dwGamemode == 1337) {
				g_pEngine->m_dwGamemode = *(DWORD*)(unlrespawn + dwUnlimitedRespawnOffset/*0x16F04*/);
			}
				
			if(g_pEngine->m_dwGamemode != 5) {
				*(DWORD*)(unlrespawn + dwUnlimitedRespawnOffset) = cvars.UnlRespawn?1:g_pEngine->m_dwGamemode;
			}
		}
	}

	if(dwShell) {
		/*
		374A655F    CC              INT3
		374A6560    A1 F0888037     MOV EAX,DWORD PTR DS:[378088F0]
		374A6565    8B48 10         MOV ECX,DWORD PTR DS:[EAX+10]
		374A6568    56              PUSH ESI
		374A6569    8B7424 08       MOV ESI,DWORD PTR SS:[ESP+8]
		374A656D    3B71 08         CMP ESI,DWORD PTR DS:[ECX+8]
		374A6570    75 04           JNZ SHORT cshell.374A6576
		374A6572    32C0            XOR AL,AL
		374A6574    5E              POP ESI
		374A6575    C3              RETN
		*/
	}

	
	if(!g_pEngine->Update()) return hRet;


	//bIsNotLoaded = true;

	if(g_pEngine->m_pLocal) {
		g_pEngine->FlyToView();

		if(cvars.Hover)
		{
			static bool Hover = false;
			if(GetAsyncKeyState(cvars.Hover) & 1) Hover = !Hover;
			if(Hover) g_pEngine->m_pLocal->gravity = 3;
		}

		/*if(GetAsyncKeyState(VK_NUMPAD5) & 1) {
			g_pEngine->m_pView->roll += M_PI_4;
		}*/

		/*static bool rofl = 0;
		if(!rofl && g_pEngine->m_pView) {
			rofl = 1;
			g_pTools->LogText("%X", g_pEngine->m_pView);
		}*/
	}


	DWORD dwLocalID = 0;
	g_pEngine->m_pLTBase->GetLocalClientID(&dwLocalID);
	g_pEngine->m_dwLocalIndex = dwLocalID;
	
	g_pEngine->GetCamera();

	//g_pTools->DrawString(200, 600, red, DT_LEFT, "%d", (int)g_pEngine->m_pLTBase->IsConnected());



	if(g_pEngine->m_pLTBase->IsConnected()) {

		if(!g_pAimbot->m_bLocked) {
			g_pAimbot->Reset();
		}

		if(cvars.Radar && g_pEngine->m_vLocalPosition.x != 0)
		{
			g_pTools->DrawFilledQuad((cvars.w-cvars.radarx),cvars.radary,82*2,82*2,black);
			g_pTools->DrawQuad((cvars.w-cvars.radarx),cvars.radary,82*2,82*2,white,2);
			g_pTools->DrawFilledQuad((cvars.w-cvars.radarx), cvars.radary+(82), 82*2, 1, green);
			g_pTools->DrawFilledQuad((cvars.w-cvars.radarx)+(82), cvars.radary, 1, 82*2, green);
		}

		g_pESP->Nade();
		g_pESP->Pickup();

		if(bGhostmode) {
			int x = 0, y = 0;
			if(g_pEngine->WorldToScreen(g_pEngine->m_vOldRealPosition, x, y))
			g_pTools->DrawString(x, y, yellow, DT_CENTER, /*YOUR REAL BODY*/XorStr<0x1D,15,0x29A4F27C>("\x44\x51\x4A\x72\x01\x70\x66\x65\x69\x06\x65\x67\x6D\x73"+0x29A4F27C).s);
		}

		/*Main* main = (Main*)0x377AAE80;

		if(main && main->goToInfoClass) {

			float percentage_width = float(cvars.w-20)/100;

			g_pTools->DrawFilledQuad(10, cvars.h-30, cvars.w-20, 20, black);
			g_pTools->DrawQuad(10, cvars.h-30, cvars.w-20, 20, white, 1);
			g_pTools->DrawFilledQuad(12, cvars.h-28,  (main->goToInfoClass->iCurrentHealth*percentage_width)-2, 16, green);

		}*/

	/*	for(int i = 0; i < g_pEngine->GetMaxIndex(cvars.Test); i++) {
			CEntity* pEntity = g_pEngine->GetEntityByIndex(cvars.Test, i);
			if(pEntity) {
				LTVector vPosition;
				g_pEngine->m_pLTClient->GetObjectPos(pEntity->object, &vPosition);

				int iCoords[2] = { 0 };
				if(g_pEngine->WorldToScreen(vPosition, iCoords[0], iCoords[1])) {
					g_pTools->DrawFilledQuad(iCoords[0]-3, iCoords[1]-3, 6, 6, red);
				}
			}
		}*/


		for(int i = 0; i < g_pEngine->GetMaxIndex(PLAYER); i++)
		{
			CEntity *pPlayer = g_pEngine->GetEntityByIndex(PLAYER, i);
			if(!pPlayer) continue;

			ClientInfo* info = g_pEngine->GetClientByID(pPlayer->index);
			ClientInfo* linfo = g_pEngine->GetClientByID(dwLocalID);

			if(linfo) {
				bAlive = linfo->isdead == 0;
			}

			g_pESP->Do(pPlayer, info, linfo);
			g_pESP->Fireteam(pPlayer, info);

			GhostmodeWarning(g_pESP->m_vHeadPosition, info, linfo);

			g_pAimbot->Collect(pPlayer, info, linfo, i);

			g_pAimbot->CollectFireteam(pPlayer, info, linfo, i);
		}

		g_pAimbot->Do();
	}
	else {
		g_pEngine->m_iKills = 0;
		g_pEngine->m_iDeaths = 0;
		g_pEngine->m_fKDR = 0;
		bAlive = false;
	}

	} catch(...) { /*g_pTools->LogText("exception"); */return hRet; }


	return hRet;
}

void __cdecl hkReset(LPDIRECT3DDEVICE9 pDevice, D3DPRESENT_PARAMETERS* pPresentationParameters)
{
	__asm pushad;


	//g_pTools->Crypt((DWORD)&hkReset, false);
	//CRYPT_START
		

	if(dwVMTDisable)
		*(DWORD*)(dwVMTDisable) = 2;



	g_pTools->ReleaseTextures();
	g_pTools->ReleaseFont();

	//if(pFont && !IsBadReadPtr((void*)pFont, 4)) {
	//	pFont->Invalidate();
	//}

	//delete pFont;
//	pFont = NULL;

	//if(pLine) pLine->Release();
	pLine = NULL;


	/*if(0) {
		pPresentationParameters->Windowed = 1; 
		pPresentationParameters->Flags = 0; 
		pPresentationParameters->FullScreen_RefreshRateInHz = 0; 
		pPresentationParameters->PresentationInterval = 0; 
		SetWindowPos(pPresentationParameters->hDeviceWindow, HWND_NOTOPMOST, 0, 0, pPresentationParameters->BackBufferWidth, pPresentationParameters->BackBufferHeight, SWP_SHOWWINDOW);
	}*/

	//if (pFont && !IsBadReadPtr((void*)pFont, 4)) {
	//	pFont->InvalidateDeviceObjects();
	//	pFont->DeleteDeviceObjects();
	//}

//	pFont = NULL;

	/*CRYPT_END
	g_pTools->Crypt((DWORD)&hkReset, true);*/

	__asm popad;

	return; //pReset(pDevice, pPresentationParameters);
}


#define COSTUM1 (NumVertices == 10 || NumVertices == 17 || NumVertices == 60 || NumVertices == 81 || NumVertices == 91 || NumVertices == 166 \
	|| NumVertices == 181 || NumVertices == 133 || NumVertices == 203 || NumVertices == 210 || NumVertices == 214 || NumVertices == 244 \
	|| NumVertices == 295 || NumVertices == 323)

#define c(a,b) (NumVertices == a && primCount == b)
#define CUSTOM2 c(10,8)||c(133,162)||c(153,133)||c(166, 198)||c(181,166)||c(203,227)||c(210,386)|| \
	c(214,184)||c(244,176)||c(295,171)||c(323,340)||c(17,10)||c(24,18)||c(31,18)||c(57,47)||\
	c(60,62)||c(61,44)

HRESULT __stdcall hkDrawIndexedPrimitive(LPDIRECT3DDEVICE9 pDevice, D3DPRIMITIVETYPE PrimitiveType,INT BaseVertexIndex,UINT MinVertexIndex,UINT NumVertices,UINT startIndex,UINT primCount)
{	

	__asm pushad
	/*g_pTools->Crypt((DWORD)&hkDrawIndexedPrimitive, false);
	CRYPT_START*/
		

	//pDevice->GetStreamSource(0, &la, &KE, &STRIDE);

	if(cvars.Asus) {
		pDevice->SetRenderState(D3DRS_ZENABLE, false);
	}

	if(cvars.Nofog) pDevice->SetRenderState(D3DRS_FOGENABLE, false);


	if((STRIDE == 32 && NumVertices == 1) || STRIDE == 36 || STRIDE == 44 /*|| COSTUM1|| ( (CUSTOM2)&&(cvars.Chams))*/ )
	{
		if(cvars.Wireframe) {
			pDevice->SetRenderState(D3DRS_ZENABLE, false);
			pDevice->SetRenderState(D3DRS_FOGENABLE, false);
			pDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);
		}
		
		if(cvars.Wallhack) {
			pDevice->SetRenderState(D3DRS_ZENABLE, false);
			pDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
			pDevice->SetRenderState(D3DRS_FOGENABLE, false);
			pDrawIndexedPrimitive(pDevice, PrimitiveType, BaseVertexIndex, MinVertexIndex, NumVertices, startIndex, primCount);

		}

		
		if(cvars.Chams) {
			pDevice->SetRenderState(D3DRS_FOGENABLE, false);
			pDevice->SetRenderState(D3DRS_ZENABLE, false);

			if(cvars.Chams == 7) SETTEX(Red)
			if(cvars.Chams == 8) SETTEX(Green)

			if(cvars.Chams == 3) SETTEX(Red)
			if(cvars.Chams == 4) SETTEX(Yellow)

			if(cvars.Chams == 11) SETTEX(Blue)
			if(cvars.Chams == 12) SETTEX(Red)

			if(cvars.Chams == 5) SETTEX(Blue)
			if(cvars.Chams == 6) SETTEX(Yellow)

			if(cvars.Chams == 9) SETTEX(White)
			if(cvars.Chams == 10) SETTEX(Red)

			if(cvars.Chams == 1) SETTEX(Green)
			if(cvars.Chams == 2) SETTEX(Blue)


			pDevice->SetRenderState(D3DRS_FOGENABLE, false);
			pDrawIndexedPrimitive(pDevice, PrimitiveType, BaseVertexIndex, MinVertexIndex, NumVertices, startIndex, primCount);
			pDevice->SetRenderState(D3DRS_FOGENABLE, false);
			pDevice->SetRenderState(D3DRS_ZENABLE, true);

			if(cvars.Chams == 5) SETTEX(Black)
			if(cvars.Chams == 6) SETTEX(Blue);
			
			if(cvars.Chams == 1) SETTEX(Blue)
			if(cvars.Chams == 2) SETTEX(Green)

			if(cvars.Chams == 11) SETTEX(Red)
			if(cvars.Chams == 12) SETTEX(Blue)

			if(cvars.Chams == 7) SETTEX(Green)
			if(cvars.Chams == 8) SETTEX(Red)

			if(cvars.Chams == 9) SETTEX(Black)
			if(cvars.Chams == 10) SETTEX(White)

			if(cvars.Chams == 3) SETTEX(Yellow)
			if(cvars.Chams == 4) SETTEX(Red)
			

			pDevice->SetRenderState(D3DRS_FOGENABLE, false);
			pDevice->SetRenderState(D3DRS_FILLMODE,D3DFILL_SOLID);
		}
	}


//#define HANDS STRIDE == 40 \
//   && (NumVertices == 8 || NumVertices == 18 || NumVertices == 20 || NumVertices == 23 || NumVertices == 24 \
//	|| NumVertices == 25 || NumVertices == 28 || NumVertices == 31 || NumVertices == 35 || NumVertices == 36 || NumVertices == 41 \
//	|| NumVertices == 41 || NumVertices == 49 || NumVertices == 58 || NumVertices == 59 || NumVertices == 55 || NumVertices ==63 \
//	|| NumVertices == 63 || NumVertices == 65 || NumVertices == 70 || NumVertices == 116 || NumVertices == 189 || NumVertices == 257 \
//	)


//#define HANDS \
//	((NumVertices == 8 && primCount == 3) || \
//	(NumVertices == 18 && primCount == 11) || \
//	(NumVertices == 20 && primCount == 11) || \
//	(NumVertices == 23 && primCount == 11) || \
//	(NumVertices == 24 && primCount == 17) || \
//	(NumVertices == 24 && primCount == 20) || \
//	(NumVertices == 25 && primCount == 16) || \
//	(NumVertices == 28 && primCount == 20) || \
//	(NumVertices == 31 && primCount == 16) || \
//	(NumVertices == 35 && primCount == 15) || \
//	(NumVertices == 35 && primCount == 26) || \
//	(NumVertices == 36 && primCount == 22) || \
//	(NumVertices == 41 && primCount == 25) || \
//	(NumVertices == 49 && primCount == 54) || \
//	(NumVertices == 55 && primCount == 61) || \
//	(NumVertices == 58 && primCount == 52) || \
//	(NumVertices == 59 && primCount == 64) || \
//	(NumVertices == 63 && primCount == 54) || \
//	(NumVertices == 65 && primCount == 54) || \
//	(NumVertices == 70 && primCount == 70) || \
//	(NumVertices == 116 && primCount == 111) || \
//	(NumVertices == 141 && primCount == 181) || \
//	(NumVertices == 189 && primCount == 253) || \
//	(NumVertices == 257 && primCount == 292) || \
//	(NumVertices == 305 && primCount == 364))
//
//	//if(GetAsyncKeyState(VK_NUMPAD7) & 1) {
//	//	uNumvertices--;
//	//}
//	//if(GetAsyncKeyState(VK_NUMPAD9) & 1) {
//	//	uNumvertices++;
//	//}
//	//if(GetAsyncKeyState(VK_NUMPAD4) & 1) {
//	//	uPrimcount--;
//	//}
//	//if(GetAsyncKeyState(VK_NUMPAD6) & 1) {
//	//	uPrimcount++;
//	//}
//	//if(GetAsyncKeyState(VK_NUMPAD5) & 1) {
//	//	g_pTools->LogText("Numvertice: %d Primcount: %d", uNumvertices, uPrimcount);
//	//}
//
//	if(HANDS && cvars.Nohands) {
//
//		//g_pTools->Crypt((DWORD)&hkDrawIndexedPrimitive);
//		__asm popad
//		return D3D_OK;
//	}


	if(STRIDE == 28 && cvars.Nosky) {
		if(NumVertices == 8 || NumVertices == 240 || NumVertices == 4) {
			if(primCount == 2 || primCount == 4 || primCount == 80)
				SETTEX(Black)
		}
	}

	//if(uNumvertices == NumVertices && (primCount == uPrimcount || uPrimcount == 0)) {
	//	SETTEX(Red);
	//}

	//CRYPT_END
	//g_pTools->Crypt((DWORD)&hkDrawIndexedPrimitive, true);

	__asm popad
	return pDrawIndexedPrimitive(pDevice, PrimitiveType, BaseVertexIndex, MinVertexIndex, NumVertices, startIndex, primCount);
}


HRESULT __stdcall hkSetStreamSource(LPDIRECT3DDEVICE9 pDevice, UINT StreamNumber,IDirect3DVertexBuffer9* pStreamData,UINT OffsetInBytes,UINT Stride)
{
	__asm pushad

	if(dwVMTDisable)
		*(DWORD*)(dwVMTDisable) = 2;

	_asm {
		nop
			nop
			nop
			nop
			nop
			nop
	}

	STRIDE = Stride;


	__asm popad
	return pSetStreamSource(pDevice, StreamNumber, pStreamData, OffsetInBytes, Stride);
}