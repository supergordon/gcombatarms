////////////////////////////////////////////////////////////////////////
// This font class was created by Azorbix (Matthew L) for Game-Deception
// A lot all of the GenFont() code was created by Microsoft
//
// Please note that this is NOT complete yet, colour tags and shadows
// are not implemented yet
//

#include "SDK\D3D\d3d9.h"
#include <math.h>
#include "d3dfont.h"

#define MAX(a,b) (a>b?a:b)

CD3DFont *g_pD3DFont = NULL;

//-----------------------------------------------------------------
// Name: GenFont
// Desc: Generates a font with given paramters (dwFlags does nothing)
//-----------------------------------------------------------------
HRESULT CD3DFont::GenFont(IDirect3DDevice9 *pD3Ddev, char *szFontName, UINT iFontSize, DWORD dwFlags)
{
	HRESULT hr;

	if((m_pD3Ddev = pD3Ddev) == NULL)
		return E_FAIL;

	m_iTexWidth = m_iTexHeight = 1024;

    hr = m_pD3Ddev->CreateTexture(m_iTexWidth, m_iTexHeight, 1, 0, D3DFMT_A4R4G4B4, D3DPOOL_MANAGED, &m_pD3Dtex, NULL);
    if( FAILED(hr) )
        return hr;

	hr = GenBuffers();
	if( FAILED(hr) )
	{
		m_pD3Dtex->Release();
		return hr;
	}

	DWORD *pBitmapBits;
	BITMAPINFO bmi;

	ZeroMemory(&bmi.bmiHeader,  sizeof(BITMAPINFOHEADER));
	bmi.bmiHeader.biSize        = sizeof(BITMAPINFOHEADER);
	bmi.bmiHeader.biWidth       =  (int)m_iTexWidth;
	bmi.bmiHeader.biHeight      = -(int)m_iTexHeight;
	bmi.bmiHeader.biPlanes      = 1;
	bmi.bmiHeader.biCompression = BI_RGB;
	bmi.bmiHeader.biBitCount    = 32;

	HDC hDC = CreateCompatibleDC(NULL);
	HBITMAP hbmBitmap = CreateDIBSection(hDC, &bmi, DIB_RGB_COLORS, (void**)&pBitmapBits, NULL, 0);
	SetMapMode(hDC, MM_TEXT);

	int iHeight = -MulDiv(iFontSize, (int)GetDeviceCaps(hDC, LOGPIXELSY), 72);

	HFONT hFont = CreateFont(iHeight, 0, 0, 0, FW_NORMAL, false, false, false, 
		DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, ANTIALIASED_QUALITY, 
		VARIABLE_PITCH, szFontName);

    if(hFont == NULL)
		return E_FAIL;

	SelectObject(hDC, hbmBitmap);
	SelectObject(hDC, hFont);

	SetTextColor(hDC, RGB(255,255,255));
	SetBkColor(hDC, 0x00000000);
	SetTextAlign(hDC, TA_TOP);

	unsigned int x = 0, y = 0;
	unsigned int iCharSpacing = 0;
	char szStr[2] = { ' ', 0 };
	SIZE size;

	GetTextExtentPoint32(hDC, szStr, 1, &size);
	m_fCharHeight = (float)size.cy;
	iCharSpacing = size.cx/4;
	iCharSpacing = MAX(iCharSpacing, 1);
	x = iCharSpacing;

	for(char c = 32; c < 127; c++)
	{
		szStr[0] = c;
		GetTextExtentPoint32(hDC, szStr, 1, &size);

		if( x+size.cx+iCharSpacing > m_iTexWidth )
		{
			x = iCharSpacing;
			y += size.cy + 1;
		}

		RECT rect = {x, y, x+size.cx, y+size.cy };
		ExtTextOut(hDC, x, y, ETO_OPAQUE|ETO_CLIPPED, &rect, szStr, 1, NULL);

		//tu src + dst
		m_fTexCoords[c-32][0] = (float)(x-iCharSpacing) /			(float)m_iTexWidth;
		m_fTexCoords[c-32][2] = (float)(x+size.cx+iCharSpacing) /	(float)m_iTexWidth;
        
		//tv src + dst
		m_fTexCoords[c-32][1] = (float)(y) /						(float)m_iTexHeight;
		m_fTexCoords[c-32][3] = (float)(y+size.cy) /				(float)m_iTexHeight;

		m_fCharWidth[c-32] = (float)(size.cx+2*iCharSpacing);
        
        x += size.cx + (2*iCharSpacing);
	}
	
    D3DLOCKED_RECT d3dlr;    
	m_pD3Dtex->LockRect(0, &d3dlr, 0, 0);
    
	BYTE *pDstRow = (BYTE*)d3dlr.pBits;
    WORD *pDst16;
    BYTE bAlpha;

    for(y=0; y < m_iTexHeight; y++)
    {
        pDst16 = (WORD*)pDstRow;

        for(x=0; x < m_iTexWidth; x++)
        {
			bAlpha = (BYTE)(pBitmapBits[m_iTexWidth*y+x]>>4) & 0xFF;
			
			if (bAlpha > 0x0)
				*pDst16 = (WORD)(bAlpha << 12) | 0x0FFF;
			else
				*pDst16 = 0x0000;

			pDst16++;
        }
		
		pDstRow += d3dlr.Pitch;
    }

	m_pD3Dtex->UnlockRect(0);

	DeleteObject(hbmBitmap);
	DeleteDC(hDC);
	DeleteObject(hFont);

	GenStateBlocks();
    
	m_bCanPrint = true;

	return S_OK;
}

//-----------------------------------------------------------------
// Name: GenStateBlocks
// Desc: Generates State blocks for font rendering and normal play
//-----------------------------------------------------------------
HRESULT CD3DFont::GenStateBlocks()
{
	for(int iStateBlock = 0; iStateBlock < 2; iStateBlock++)
	{
		m_pD3Ddev->BeginStateBlock();
		m_pD3Ddev->SetTexture(0, m_pD3Dtex);
		m_pD3Ddev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE );
		m_pD3Ddev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA );
		m_pD3Ddev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA );
		m_pD3Ddev->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE );
		m_pD3Ddev->SetRenderState(D3DRS_ALPHAREF, 0x08 );
		m_pD3Ddev->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL );
		m_pD3Ddev->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID );
		m_pD3Ddev->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW );
		m_pD3Ddev->SetRenderState(D3DRS_STENCILENABLE, FALSE );
		m_pD3Ddev->SetRenderState(D3DRS_CLIPPING, TRUE );
		m_pD3Ddev->SetRenderState(D3DRS_CLIPPLANEENABLE, FALSE );
		m_pD3Ddev->SetRenderState(D3DRS_VERTEXBLEND, D3DVBF_DISABLE );
		m_pD3Ddev->SetRenderState(D3DRS_INDEXEDVERTEXBLENDENABLE, FALSE );
		m_pD3Ddev->SetRenderState(D3DRS_FOGENABLE, FALSE );
		m_pD3Ddev->SetRenderState(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED|D3DCOLORWRITEENABLE_GREEN|D3DCOLORWRITEENABLE_BLUE|D3DCOLORWRITEENABLE_ALPHA);
		m_pD3Ddev->SetTextureStageState(0, D3DTSS_COLOROP,  D3DTOP_MODULATE );
		m_pD3Ddev->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE );
		m_pD3Ddev->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE );
		m_pD3Ddev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE );
		m_pD3Ddev->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE );
		m_pD3Ddev->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE );
		m_pD3Ddev->SetTextureStageState(0, D3DTSS_TEXCOORDINDEX, 0 );
		m_pD3Ddev->SetTextureStageState(0, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_DISABLE );
		m_pD3Ddev->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_DISABLE );
		m_pD3Ddev->SetTextureStageState(1, D3DTSS_ALPHAOP, D3DTOP_DISABLE );
		m_pD3Ddev->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_POINT );
		m_pD3Ddev->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_POINT );
		m_pD3Ddev->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_NONE );

		if(iStateBlock)	m_pD3Ddev->EndStateBlock(&m_pD3DstateDraw);
		else			m_pD3Ddev->EndStateBlock(&m_pD3DstateNorm);
	}

	return S_OK;
}

//-----------------------------------------------------------------
// Name: GenBuffers
// Desc: Generates vertex buffers for font and fonts fx's
//-----------------------------------------------------------------
HRESULT CD3DFont::GenBuffers()
{
	HRESULT hr;

	// generate character buffer
	hr = m_pD3Ddev->CreateVertexBuffer(m_iMaxChars*sizeof(d3dfont_s)*6, D3DUSAGE_WRITEONLY|D3DUSAGE_DYNAMIC, 0, D3DPOOL_DEFAULT, &m_pD3Dbuf, NULL);
	if( FAILED(hr) )
		return hr;
	

	// generate effects buffer
	hr = m_pD3Ddev->CreateVertexBuffer(m_iMaxFxVert * sizeof(d3dfontfx_s), D3DUSAGE_WRITEONLY|D3DUSAGE_DYNAMIC, 0, D3DPOOL_DEFAULT, &m_pD3Dfxbuf, NULL);
	if( FAILED(hr) )
		m_pD3Dbuf->Release();
	
	return hr;
}

//-----------------------------------------------------------------
// Name: DrawLength
// Desc: Returns pixel length of zero terminated string
//-----------------------------------------------------------------
float CD3DFont::DrawLength(char *szText) const
{
	float len = 0.0f;

	for(char *p=szText; *p; p++)
	{
		char c = (*p)-32;
		if(c >= 0 && c < 96) 
			len += m_fCharWidth[c];
	}

	return len;
}

//-----------------------------------------------------------------
// Name: Invalidate
// Desc: Releases all resources and prevents font from being drawn
//-----------------------------------------------------------------
HRESULT CD3DFont::Invalidate()
{
	m_bCanPrint = false;

	SAFE_RELEASE(m_pD3Dtex);
	SAFE_RELEASE(m_pD3Dbuf);
	SAFE_RELEASE(m_pD3Dfxbuf);
	SAFE_RELEASE(m_pD3DstateDraw);
	SAFE_RELEASE(m_pD3DstateNorm);

	return S_OK;
}

//-----------------------------------------------------------------
// Name: AddFont
// Desc: Adds text to the vertex buffer
//-----------------------------------------------------------------
HRESULT CD3DFont::AddFont(float x, float y, DWORD colour, DWORD flags, char *szText)
{
	if(!m_bCanPrint)
		return E_FAIL;

	if(flags&FT_CENTER)
		x -= DrawLength(szText) / 2.0f;

	//stuff for border and underline
	float ex, sx = x-1.0f;
	float ey, sy = y-1.0f;
	ey = y+m_fCharHeight+1.0f;

	d3dfont_s *pVertex = NULL;
	d3dfont_s vertex;
	vertex.z = 1.0f;
	vertex.rhw = 1.0f;
	vertex.colour = colour;	

	if( FAILED(m_pD3Dbuf->Lock(0, 0, (void**)&pVertex, D3DLOCK_DISCARD)) )
		return E_FAIL;

	int iIndex = m_iUsedChars*6;

	for(char *p=szText; *p; p++)
	{
		char c = (*p)-32;

		if(c < 0 || c > 126) 
			continue;

		if((++m_iUsedChars) > m_iMaxChars)
			return E_FAIL;
        
		//top left of Triangle 1
		vertex.x = x;
		vertex.y = y;
		vertex.tu = m_fTexCoords[c][0];
		vertex.tv = m_fTexCoords[c][1];
		pVertex[iIndex] = vertex;

		//top right of Triangle 1 & 2
		vertex.x += m_fCharWidth[c];
		vertex.tu = m_fTexCoords[c][2];
        pVertex[iIndex+1] = vertex;
		pVertex[iIndex+3] = vertex;

		//bottom right of Triangle 2
		vertex.y += m_fCharHeight;
		vertex.tv = m_fTexCoords[c][3];
		pVertex[iIndex+4] = vertex;

		//bottom left of Triangle 1 & 2
		vertex.x -= m_fCharWidth[c];
		vertex.tu = m_fTexCoords[c][0];
		pVertex[iIndex+2] = vertex;
		pVertex[iIndex+5] = vertex;

        x += m_fCharWidth[c];
		ex = x+1.0f;
		iIndex += 6;
	}

	if(flags&FT_BORDER)
	{
		d3dfontfx_s *pFxVertex = NULL;
		
		d3dfontfx_s fxvertex;
		fxvertex.z = 1.0f;
		fxvertex.rhw = 1.0f;

		if( FAILED(m_pD3Dfxbuf->Lock(0, 0, (void**)&pFxVertex, D3DLOCK_DISCARD)) )
			return S_OK;

		if(flags&FT_BORDER && m_iMaxFxVert-m_iUsedFxVert >= 6)
		{
			fxvertex.colour = 0x5F000000;

			fxvertex.x = sx;
			fxvertex.y = sy;
			pFxVertex[m_iUsedFxVert] = fxvertex;		//top left
			
			fxvertex.x = ex;
			pFxVertex[m_iUsedFxVert+1] = fxvertex;		//top right
			pFxVertex[m_iUsedFxVert+4] = fxvertex;
	
			fxvertex.x = sx;
			fxvertex.y = ey;
			pFxVertex[m_iUsedFxVert+2] = fxvertex;		//bottom left
			pFxVertex[m_iUsedFxVert+3] = fxvertex;		
		
			fxvertex.x = ex;
			fxvertex.y = ey;
			pFxVertex[m_iUsedFxVert+5] = fxvertex;		//bottom right

			m_iUsedFxVert += 6;
		}
	}

	return S_OK;
}

//-----------------------------------------------------------------
// Name: RenderAll
// Desc: Renders all effects and characters in the buffer
//-----------------------------------------------------------------
HRESULT CD3DFont::RenderAll()
{
	if(m_iUsedChars < 1)
        return S_OK;
	
	m_pD3Dbuf->Unlock();

	DWORD fvf;

	m_pD3DstateNorm->Capture();
	m_pD3DstateDraw->Apply();

	m_pD3Ddev->GetFVF(&fvf);

	if(m_iUsedFxVert)
	{
		m_pD3Ddev->SetFVF(D3DFVF_FONTFX);
		m_pD3Ddev->SetStreamSource(0, m_pD3Dfxbuf, 0, sizeof(d3dfontfx_s));
		m_pD3Ddev->SetTexture(0, NULL);
		m_pD3Ddev->DrawPrimitive(D3DPT_TRIANGLELIST, 0, (m_iUsedFxVert/3));
		m_pD3Ddev->SetTexture(0, m_pD3Dtex); //set it here for next stuff
	}

	m_pD3Ddev->SetFVF(D3DFVF_BITMAPFONT);
	m_pD3Ddev->SetStreamSource(0, m_pD3Dbuf, 0, sizeof(d3dfont_s));
	m_pD3Ddev->DrawPrimitive(D3DPT_TRIANGLELIST, 0, (m_iUsedChars*2));

	m_pD3Ddev->SetFVF(fvf);
	m_pD3DstateNorm->Apply();

	ClearBuffer();

	return S_OK;
}

//-----------------------------------------------------------------
// Name: ClearBuffer
// Desc: Clears the character and fx buffers
//-----------------------------------------------------------------
void CD3DFont::ClearBuffer()
{
	m_iUsedChars = 0;
	m_iUsedFxVert = 0;
}
