#include "sdk/Structs.h"


CAimbot *g_pAimbot = new CAimbot;

#define DotProduct(A, B) A.x * B.x + A.y * B.y + A.z * B.z

float CalcFov(LTVector pos) 
{
	LTVector vLocal, vAngles;

	/*vAngles.x = sin(g_pEngine->m_pView->yaw);
	vAngles.y = sin(-g_pEngine->m_pView->pitch);
	vAngles.z = cos(g_pEngine->m_pView->yaw);*/

	LTRotation ltRot;

	g_pEngine->m_pLTClient->GetObjectRotation(g_pEngine->m_pLTBase->GetClientObject(), &ltRot);

	vLocal = pos - g_pEngine->m_pView->cam1->pos1;

	vLocal.Normalize();

	float fov = acos(DotProduct(ltRot.Forward(), vLocal));

	return fov * (180.0 / M_PI);
}

CAimbot::CAimbot()
{
	m_fDistance = 0;
	m_fBufDistance = 0;
	m_iNearestIndex = 35;
	m_bLocked = false;
}

void CAimbot::Reset()
{
	m_fBufDistance = 0;
	m_iNearestIndex = 35;
	if(!cvars.Aimlock) m_bLocked = false;
}

void CAimbot::Collect(CEntity* pEntity, ClientInfo* pInfo, ClientInfo* pLocalInfo, int i)
{
	if(g_pEngine->m_dwGamemode == 5) return;
	if(!pEntity || !pInfo || !pLocalInfo || !pInfo->name[0]) return;
	if(pInfo->index == pLocalInfo->index) return;

	LTVector vHeadPosition = g_pESP->m_vHeadPosition;

	m_fDistance = g_pTools->GetDistance(cvars.Telekill?g_pEngine->m_pLocal->pos:g_pEngine->m_vLocalPosition, vHeadPosition);

	bool bVisible = g_pEngine->IsVisible(g_pEngine->m_vLocalPosition, vHeadPosition, pEntity->object);
	bool bCanAim = cvars.Aimthru || (!cvars.Aimthru && (cvars.Telekill || (!cvars.Telekill && bVisible)));
	bool bInRange = ((m_fDistance/35) < (cvars.WeaponRange?500:90));

	if(cvars.Aimmode == 1 && !cvars.Telekill) {
		m_fDistance = (float)pEntity->health;
	}


	if(g_pESP->IsVulnerable(pEntity->index) && pLocalInfo->team != pInfo->team) {
		if((m_fDistance < m_fBufDistance || !m_fBufDistance) && bCanAim && (cvars.Telekill || (!cvars.Telekill && bInRange)))
		{
			m_fBufDistance = m_fDistance;
			m_iNearestIndex = i;
		}
	}
}

void CAimbot::CollectFireteam(CEntity* pEntity, ClientInfo* pInfo, ClientInfo* pLocalInfo, int i)
{
	if(g_pEngine->m_dwGamemode != 5) return;
	if(!pEntity || pInfo) return;

	LTVector vHeadPosition = g_pESP->m_vHeadPosition;

	LTVector vPlayerPos;
	g_pEngine->m_pLTClient->GetObjectPos(pEntity->object, &vHeadPosition);

	m_fDistance = g_pTools->GetDistance(cvars.Telekill?g_pEngine->m_pLocal->pos:g_pEngine->m_vLocalPosition, vHeadPosition);

	bool bVisible = g_pEngine->IsVisible(g_pEngine->m_vLocalPosition, vHeadPosition, pEntity->object);
	bool bCanAim = cvars.Aimthru || (!cvars.Aimthru && (cvars.Telekill || (!cvars.Telekill && bVisible)));


	if(g_pESP->IsVulnerable(pEntity->index)) {

		if((m_fDistance < m_fBufDistance || !m_fBufDistance) && bCanAim)
		{
			m_fBufDistance = m_fDistance;
			m_iNearestIndex = i;
		}
	}
}

char *aimPoints[] = {"Head", "Neck", "Upper_Torso", "Torso"};
char *aimPointsBip[] = {"Bip01 Head", "Bip01 Neck", "Bip01 Upper_Torso", "Bip01 Torso"};

void CAimbot::Do()
{	
	CEntity* pPlayer = g_pEngine->GetEntityByIndex(PLAYER, m_iNearestIndex);
	if(!pPlayer) return;
	

	HMODELNODE node;
	HMODELNODE child;
	LTTransform transform;

	LTRESULT ltRes = g_pEngine->m_pLTModel->GetNode(pPlayer->object, aimPoints[cvars.Aimpoint], node);
	if(ltRes != LT_OK) g_pEngine->m_pLTModel->GetNode(pPlayer->object, aimPointsBip[cvars.Aimpoint], node);
	g_pEngine->m_pLTModel->GetNodeTransform(pPlayer->object, node, transform, true);
	m_vTargetHead = transform.m_vPos;


	ClientInfo* info = g_pEngine->GetClientByID(pPlayer->index);

	if(cvars.Aimlock) {
		m_bLocked = true;
	}

	if(info && info->isdead)
		m_bLocked = false;

	int x = 0, y = 0;
	if(g_pEngine->WorldToScreen(m_vTargetHead, x, y)) {
		g_pTools->DrawFilledQuad(x, y, 2, 2, white);
	}

	if(cvars.Telekill)  {		
		LTRotation ltRot;
		g_pEngine->m_pLTClient->GetObjectRotation(pPlayer->object, &ltRot);

		vTelePlayer.y+=30;
		vTelePlayer = (m_vTargetHead + (ltRot.Forward()*25));
	}

	if(((cvars.Knifebot || (cvars.Aimkey == 0 || (cvars.Aimkey > 0 && GetAsyncKeyState(cvars.Aimkey))))&&cvars.Aimtoggle) || cvars.Telekill) {

		/*LTRotation ltRot;

		g_pEngine->m_pLTClient->GetObjectRotation(pPlayer->object, &ltRot);
		LTVector aimpoint = m_vTargetHead + (ltRot.Forward()*10);*/
		g_pEngine->AimToPoint(m_vTargetHead, cvars.Aimpoint?0:-5);

		if(cvars.Autoshoot && !cvars.Knifebot) {
			mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0); 
			mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0); 
		}

		float dist = (m_vTargetHead-g_pEngine->m_vLocalPosition).Mag()/40;
		if(cvars.Knifebot && dist <= 4) {
			mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0); 
			mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0); 
		}
	}
	else {
		m_bLocked = false;
	}
}