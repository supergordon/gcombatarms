#ifndef _HACKSHIELD_
#define _HACKSHIELD_

class CHackshield
{
	public:
		CHackshield();

		void Hook();
		void TurnOnOff(bool toggle);
		DWORD dwHSCounter;
		bool scanning;

		void Unhook();
		void Rehook();

		DWORD dwSelfScan;
		DWORD dwSelfScan2;
		DWORD dwSelfScan3;
		DWORD dwMemScan;
		DWORD dwMemScan2;
		DWORD dwVmtScan;
		DWORD dwHackScan;
		DWORD dwunkscan;
		DWORD dwKomisch;

		BYTE bSelfScan[10];
		BYTE bSelfScan2[10];
		BYTE bSelfScan3[10];
		BYTE bMemScan[10];
		BYTE bMemScan2[10];
		BYTE bVmtScan[10];
		BYTE bHackScan[10];
		BYTE bunkscan[10];
		BYTE bKomisch[10];

}; extern CHackshield *g_pHackshield;

typedef int (__stdcall* HSMEMScan)(DWORD param1, DWORD param2, DWORD param3);
typedef int (__cdecl* HSD3DVMTScan)(int param1);
typedef int (__stdcall* HSSELFScan)(DWORD param1, DWORD param2, DWORD param3);
typedef int (__cdecl* HSMEMScan2)(DWORD param1, DWORD param2, DWORD param3, DWORD param4);
typedef int (__cdecl* HSSELFScan2)(DWORD param1, DWORD param2, DWORD param3, DWORD param4);
typedef int (__cdecl* HSSELFScan3)(DWORD param1, DWORD param2, DWORD param3, DWORD param4);
typedef int (__cdecl* unkscan_t)(int a, int b, char* c, int d);
typedef int (__stdcall *Komisch_t)(DWORD param1, DWORD param2);

#endif