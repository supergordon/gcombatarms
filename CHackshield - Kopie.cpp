#include "SDK\\Structs.h"

CHackshield *g_pHackshield = new CHackshield;

CCVARS gVars = {0};
int bGhostmode = false;

HSMEMScan pMemScan = 0;
HSD3DVMTScan pVMTScan = 0;
HSSELFScan pSelfScan = 0;
HSSELFScan2 pSelfScan2 = 0;
HSSELFScan3 pSelfScan3 = 0;
HSMEMScan2 pMemScan2 = 0;
unkscan_t punkscan = 0;
Komisch_t pKomisch = 0;


CHackshield::CHackshield()
{
	dwSelfScan = 0;
	dwSelfScan2 = 0;
	dwMemScan = 0;
	dwVmtScan = 0;
	dwMemScan2 = 0;
	dwunkscan = 0;
	dwKomisch = 0;

	memset(bSelfScan, 0, sizeof(bSelfScan));
	memset(bMemScan, 0, sizeof(bMemScan));
	memset(bVmtScan, 0, sizeof(bVmtScan));
}

void CHackshield::TurnOnOff(bool toggle)
{
	if(!toggle) {	
		memcpy(&gVars, (void*)&cvars, sizeof(CCVARS)); 
		memset(&cvars, 0, sizeof(CCVARS));
		g_pAddress->Restore();
		//kk(0);
	}
	else if(toggle) {
		memcpy(&cvars, &gVars, sizeof(CCVARS));
		g_pAddress->Apply();
		//kk(1);
	}
}

int __cdecl hkVMTScan(int param1)
{
	__asm pushad
		g_pTools->LogText("d3dscan");
	__asm popad
	
	return 0; //0 => nothing detected
}

int __stdcall hkMEMScan(DWORD param1, DWORD param2, DWORD param3)
{
	__asm pushad
		g_pHackshield->TurnOnOff(false);
		//g_pTools->LogText("memscan: %X", param1);
	__asm popad
	
	int k = pMemScan(param1, param2, param3);

	__asm pushad
		g_pHackshield->TurnOnOff(true);
	__asm popad

	return k;
}

int __cdecl hkMEMScan2(DWORD param1, DWORD param2, DWORD param3, DWORD param4)
{
	__asm pushad
		g_pHackshield->TurnOnOff(false);
		//g_pTools->LogText("memscan2: %X %X %X %X", param1, param2, param3, param4);
	__asm popad
	
	int k = pMemScan2(param1, param2, param3, param4);

	__asm pushad
		g_pHackshield->TurnOnOff(true);
	__asm popad

	return k;
}

bool GetModuleNameFromAddress(DWORD address, char* modulename)
{
	HMODULE module = 0;
	GetModuleHandleEx(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS, (LPCTSTR)address, &module);
	if(!module) return false;

	GetModuleFileName(module, modulename, MAX_PATH);

	for(int i = strlen(modulename); i > 0; i--)
		if(modulename[i] == '\\')
		{
			strcpy(modulename, &modulename[i+1]);
			break;
		}
	return true;
}

int __stdcall hkSelfScan(DWORD param1, DWORD param2, DWORD param3)
{
	__asm pushad
		int k = 0;
		g_pHackshield->Unhook();
		//pHackshield->TurnOnOff(false); //zur sicherheit
	/*	char temp[MAX_PATH] = "";
		bool kk = GetModuleNameFromAddress(param1, temp);
		if(kk)
			g_pTools->LogText("Hackshield is now scanning %s [%X] with a scansize of %X bytes", temp, param1, param2);
		else 
			g_pTools->LogText("Hackshield is now scanning %X with a scansize of %X bytes", param1, param2);*/
		g_pTools->LogText("Selfscan1 %X", param1);
	__asm popad
	
	pSelfScan = (HSSELFScan)g_pHackshield->dwSelfScan;
	__asm 
	{
		push param3
		push param2
		push param1
		call pSelfScan
		mov k, eax
	}
	
	__asm pushad
		g_pHackshield->Rehook();
	__asm popad

	return k;
}

int __cdecl hkSelfScan2(DWORD param1, DWORD param2, DWORD param3, DWORD param4)
{
	//if(param2 == 0x3E81) {
	//	return pSelfScan2((DWORD)GetModuleHandle("psapi.dll"), param2, param3, param4);
	//}
	__asm pushad
		int k = 0;
		g_pHackshield->Unhook();
		g_pHackshield->TurnOnOff(false);
		g_pTools->LogText("Selfscan2 %X %X %X %X", param1, param2, param3, param4);
	//	g_pTools->LogText("%X %X %X %X", param1, param2, param3, param4);
	__asm popad
	
	pSelfScan2 = (HSSELFScan2)g_pHackshield->dwSelfScan2;
	k = pSelfScan2(param1, param2, param3, param4);

	if(param2 == 0x3E81)
		return k;
	
	__asm pushad
		g_pHackshield->Rehook();
		g_pHackshield->TurnOnOff(true);
	__asm popad

	return k;
}

int __cdecl hkSelfScan3(DWORD param1, DWORD param2, DWORD param3, DWORD param4)
{
	__asm pushad
		int k = 0;
		g_pHackshield->Unhook();
		g_pHackshield->TurnOnOff(false);
		g_pTools->LogText("Selfscan3 %X %X %X %X", param1, param2, param3, param4);
	__asm popad
	
	pSelfScan3 = (HSSELFScan3)g_pHackshield->dwSelfScan3;
	k = pSelfScan3(param1, param2, param3, param4);
	
	__asm pushad
		g_pHackshield->Rehook();
		g_pHackshield->TurnOnOff(true);
	__asm popad

	return k;
}

int __stdcall hkKomisch(DWORD param1, DWORD param2) {

	__asm pushad
		int k = 0;
		g_pHackshield->Unhook();
		g_pHackshield->TurnOnOff(false);

		g_pTools->LogText("scan", param1, param2);
	__asm popad
	
	pKomisch = (Komisch_t)g_pHackshield->dwKomisch;
	//k = pSelfScan2(param1, param2, param3, param4);
	__asm {
		push param2
		push param1
		call pKomisch
		mov k, eax
	}

	__asm pushad
		g_pHackshield->Rehook();
		g_pHackshield->TurnOnOff(true);
	__asm popad

	return k;
}

int __cdecl hkunkscan(int a, int b, char* c, int d)
{
	//__asm pushad
	//g_pTools->LogText("stringscan");
	//__asm popad
	return -1;
}
bool bIsNA = false;
void CHackshield::Hook()
{
	DWORD hHS = 0; 

	while(! (hHS = (DWORD)GetModuleHandle("EHSvc.dll"))) {
		Sleep(100);
	}

	//64 89 25 00 00 00 00 81 C4 D4 FD FF FF -0x16

	//fix
	dwVmtScan = g_pTools->dwFindPattern(hHS, 0x90000, (PBYTE)"\x64\x89\x25\x00\x00\x00\x00\x81\xC4\xD4\xFD\xFF\xFF", "xxxxxxxxxxxxx");//(DWORD)GetModuleHandle("EHSvc.dll") + 0x4F90F;
	if(!dwVmtScan) {
		bIsNA = true;
	dwVmtScan = g_pTools->dwFindPattern(hHS, 0x90000, (PBYTE)"\x64\x89\x25\x00\x00\x00\x00\x81\xC4\xB4\xFC\xFF\xFF", "xxxxxxxxxxxxx");//(DWORD)GetModuleHandle("EHSvc.dll") + 0x4F90F;
	}

	if(dwVmtScan) {
		dwVmtScan -= 0x16;
		memcpy(&bVmtScan, (void*)dwVmtScan, 10);
		pVMTScan = (HSD3DVMTScan)g_pTools->DetourFunc((PBYTE)dwVmtScan, (PBYTE)hkVMTScan, 5);
	}

		//83 EC 08 B8 2C 12 00 00 -0x1D
	dwMemScan = g_pTools->dwFindPattern(hHS, 0x90000, (PBYTE)"\x83\xEC\x08\xB8\x2C\x12\x00\x00", "xxxxxxxx");//(DWORD)GetModuleHandle("EHSvc.dll") + 0x13290;
	if(dwMemScan) {
		dwMemScan -= 0x1D;
		memcpy(&bMemScan, (void*)dwMemScan, 10);
		pMemScan = (HSMEMScan)g_pTools->DetourFunc((PBYTE)dwMemScan, (PBYTE)hkMEMScan, 5);
	}
	//55 8B EC 83 EC 0C 89 4D
	dwSelfScan = g_pTools->dwFindPattern(hHS, 0x90000, PBYTE("\x55\x8B\xEC\x83\xEC\x0C\x89\x4D"), "xxxxxxxx"); //dwHS+0x5140D;
	dwSelfScan2 = g_pTools->dwFindPattern(hHS, 0x90000, PBYTE("\x55\x8B\xEC\x83\xEC\x08\x8B\x45\x08\x89"), "xxxxxxxxxx");

	if(dwSelfScan2 && dwSelfScan) {
		memcpy(&bSelfScan2, (void*)dwSelfScan2, 10);
		pSelfScan2 = (HSSELFScan2)g_pTools->DetourFunc((PBYTE)dwSelfScan2, (PBYTE)hkSelfScan2, 6);

		memcpy(&bSelfScan, (void*)dwSelfScan, 10);
		pSelfScan = (HSSELFScan)g_pTools->DetourFunc((PBYTE)dwSelfScan, (PBYTE)hkSelfScan, 6);
	} 
	else if(dwSelfScan) {
		memcpy(&bSelfScan, (void*)dwSelfScan, 10);
		pSelfScan = (HSSELFScan)g_pTools->DetourFunc((PBYTE)dwSelfScan, (PBYTE)hkSelfScan, 6);
	}

	/*dwSelfScan3 = hHS + 0x3C300;
	if(dwSelfScan3) {
		memcpy(&bSelfScan3, (void*)dwSelfScan3, 10);
		pSelfScan3 = (HSSELFScan3)g_pTools->DetourFunc((PBYTE)dwSelfScan3, (PBYTE)hkSelfScan3, 5);
	}*/
	
	dwunkscan = g_pTools->dwFindPattern(hHS, 0x90000, (PBYTE)"\x0F\x82\x00\x00\x00\x00\x85\xC9\x0F\x84", "xx????xxxx");
	if(dwunkscan) {	
		dwunkscan -= 0x3A;
		memcpy(&bunkscan, (void*)dwunkscan, 10);
		punkscan = (unkscan_t)g_pTools->DetourFunc((PBYTE)dwunkscan, (PBYTE)hkunkscan, 10);
	}

	dwKomisch = g_pTools->dwFindPattern(hHS, 0x90000, (PBYTE)"\x8B\x41\x3C\x8B\x50\x3C\x52", "xxxxxxx");//(DWORD)hHS + 0x5AD0;

	if(dwKomisch) {
		memcpy(&bKomisch, (void*)dwKomisch, 10);
		pKomisch = (Komisch_t)g_pTools->DetourFunc((PBYTE)dwKomisch, (PBYTE)hkKomisch, 6);
	}

	g_pTools->LogText("%X %X %X", dwSelfScan, dwSelfScan2, hHS);

	//g_pTools->LogText("IsNA: %X\nVMT1: %X\nSelf1: %X %X\nSelf2: %X %X\nunk: %X\nMem: %X\nKomisch: %X", 
	//	(int)bIsNA, dwVmtScan, dwSelfScan, pSelfScan, dwSelfScan2, pSelfScan2, dwunkscan, dwMemScan, dwKomisch);

	/*if(!dwVmtScan || !dwMemScan || !(dwSelfScan || dwSelfScan2) || !dwunkscan || !dwKomisch) {
		MessageBox(0, "Hack is maybe now detected!", "Warning", MB_ICONWARNING);
	}*/
}

void CHackshield::Unhook()
{
	if(dwVmtScan) {
		g_pTools->Patch(dwVmtScan, bVmtScan, 10);
	}
	if(dwMemScan) {
		g_pTools->Patch(dwMemScan, bMemScan, 10);
	}
	//if(dwMemScan2) {
	//	g_pTools->Patch(dwMemScan2, bMemScan2, 10);
	//}
	if(dwSelfScan) {
		g_pTools->Patch(dwSelfScan, bSelfScan, 10);
	}
	if(dwSelfScan2) {
		g_pTools->Patch(dwSelfScan2, bSelfScan2, 10);
	}
	if(dwSelfScan3) {
		g_pTools->Patch(dwSelfScan3, bSelfScan3, 10);
	}
	if(dwunkscan) {
		g_pTools->Patch(dwunkscan, bunkscan, 10);
	}
	if(dwKomisch) {
		g_pTools->Patch(dwKomisch, bKomisch, 10);
	}
}

void CHackshield::Rehook()
{
	if(dwVmtScan) {
		pVMTScan = (HSD3DVMTScan)g_pTools->DetourFunc((PBYTE)dwVmtScan, (PBYTE)hkVMTScan, 5);
	}
	if(dwMemScan) {
		pMemScan = (HSMEMScan)g_pTools->DetourFunc((PBYTE)dwMemScan, (PBYTE)hkMEMScan, 5);
	}
	//if(dwMemScan2) {
	//	pMemScan2 = (HSMEMScan2)g_pTools->DetourFunc((PBYTE)dwMemScan2, (PBYTE)hkMEMScan2, 5);
	//}
	if(dwSelfScan) {
		pSelfScan = (HSSELFScan)g_pTools->DetourFunc((PBYTE)dwSelfScan, (PBYTE)hkSelfScan, 6);
	}
	if(dwSelfScan2) {
		pSelfScan2 = (HSSELFScan2)g_pTools->DetourFunc((PBYTE)dwSelfScan2, (PBYTE)hkSelfScan2, 6);
	}
	if(dwSelfScan3) {
		pSelfScan3 = (HSSELFScan3)g_pTools->DetourFunc((PBYTE)dwSelfScan3, (PBYTE)hkSelfScan3, 5);
	}
	if(dwunkscan) {
		punkscan = (unkscan_t)g_pTools->DetourFunc((PBYTE)dwunkscan, (PBYTE)hkunkscan, 10);
	}
	if(dwKomisch) {
		pKomisch = (Komisch_t)g_pTools->DetourFunc((PBYTE)dwKomisch, (PBYTE)hkKomisch, 6);
	}
}

/*
		//dwMemScan2 = g_pTools->dwFindPattern(hHS, 0x90000, (PBYTE)"\xC7\x45\xE4\xFF\xFF\xFF\xFF\x8B\x7D\x14", "xxxxxxxxxx");
		//g_pTools->LogText("memscan2: %X", dwMemScan2);
		//dwMemScan2 = 0;
		//if(dwMemScan2) {
		//	dwMemScan2 -= 0x2B;
		//	memcpy(&bMemScan2, (void*)dwMemScan2, 10);
		//	pMemScan2 = (HSMEMScan2)g_pTools->DetourFunc((PBYTE)dwMemScan2, (PBYTE)hkMEMScan2, 5);

		//}

	//1003C300    55              PUSH EBP
	//DR0 on READ [BYTE] at EHSvc.1003C38C
*/