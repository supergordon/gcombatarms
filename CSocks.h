#ifndef SOCKS_H
#define SOCKS_H


class CSocks
{
public:
	SOCKET sock;
	CSocks();
	~CSocks();
	int ConnectToServer(char *szServer, UINT uPort);
	int Send(void* data, int size);
	int Recv(void* data, int size);
}; extern CSocks* pSocks;

#endif