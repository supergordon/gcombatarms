#ifndef _HACKSHIELD_
#define _HACKSHIELD_

enum {
	D3DSCAN,
	MEMORYSCAN,
	STRINGSCAN
};

class CHackshield {
public:
	CHackshield();
	void Initialize();
	void SetBreakpoint(BYTE type);
	void RemoveBreakpoint(BYTE type);
	bool isScanning;
	DWORD dwSetObjectPos;
	BYTE bSetObjectPos[10];
};

extern CHackshield* g_pHackshield;

#endif