#ifndef _ESP_
#define _ESP_

#include "CColor.h"

class CESP 
{
public:
	CESP();
	void Nade();
	void Pickup();
	bool IsVulnerable(CEntity* pEntity, ClientInfo* pInfo);
	bool IsVulnerable(int i);
	void Do(CEntity* pEntity, ClientInfo* pInfo, ClientInfo* pLocalInfo);
	void Fireteam(CEntity* pEntity, ClientInfo* pInfo);
	LTVector m_vHeadPosition;
	LTVector m_vTorsoPosition;
	LTVector m_vRootPosition;
private:
	bool m_bSpawnProtected;
	CColor m_cColor;
	float m_fDistance;
};

extern CESP* g_pESP;

#endif