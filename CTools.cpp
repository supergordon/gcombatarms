#include "SDK\\Structs.h"

#include "picture.h"

CTools *g_pTools = new CTools;

ID3DXFont* m_pFont		= NULL;
ID3DXFont* m_pFont2		= NULL;
ID3DXLine *pLine		= NULL;

CTools::CTools()
{
	m_tex = NULL;
	m_sprite = NULL;
	m_pFont = NULL;
	m_pFont2 = NULL;
}

void CTools::LogText(char* szText, ...)
{
	char szBuffer[1024];
	va_list args;
    va_start(args, szText);
    memset(szBuffer, 0, sizeof(szBuffer));
    _vsnprintf(szBuffer, sizeof(szBuffer) - 1, szText, args);
    va_end(args);

	std::ofstream LOG(/*D:\\ca.txt*/XorStr<0x3A,10,0xF20D65C0>("\x7E\x01\x60\x5E\x5F\x11\x34\x39\x36"+0xF20D65C0).s,std::ios::app);
	LOG << szBuffer << std::endl;
	LOG.close();
}

void *CTools::DetourFunc(BYTE *src, const BYTE *dst, const int len)
{
	BYTE *jmp = (BYTE*)malloc(len+5);
	DWORD dwback;

	VirtualProtect(src, len, PAGE_READWRITE, &dwback);

	memcpy(jmp, src, len);	jmp += len;
	
	jmp[0] = 0xE9;
	*(DWORD*)(jmp+1) = (DWORD)(src+len - jmp) - 5;

	src[0] = 0xE9;
	*(DWORD*)(src+1) = (DWORD)(dst - src) - 5;

	VirtualProtect(src, len, dwback, &dwback);

	return (jmp-len);
}

bool CTools::RetourFunc(BYTE *src, BYTE *restore, const int len)
{
	DWORD dwback;
		
	if(!VirtualProtect(src, len, PAGE_READWRITE, &dwback))	{ return false; }
	if(!memcpy(src, restore, len))							{ return false; }

	restore[0] = 0xE9;
	*(DWORD*)(restore+1) = (DWORD)(src - restore) - 5;

	if(!VirtualProtect(src, len, dwback, &dwback))			{ return false; }
	
	return true;
}

bool bDataCompare(const BYTE* pData, const BYTE* bMask, const char* szMask)
{
	for(;*szMask;++szMask,++pData,++bMask)
		if(*szMask=='x' && *pData!=*bMask ) 
			return false;
	return (*szMask) == NULL;
}

DWORD CTools::dwFindPattern(DWORD dwAddress,DWORD dwLen,BYTE *bMask,char * szMask)
{
	for(DWORD i=0; i < dwLen; i++)
		if( bDataCompare( (BYTE*)( dwAddress+i ),bMask,szMask) )
			return (DWORD)(dwAddress+i);

	return 0;
}

void CTools::Patch(DWORD dwAddress, BYTE* lala, int size)
{
	DWORD dwProtect;
	VirtualProtect((void*)dwAddress, size, PAGE_EXECUTE_READWRITE, &dwProtect);
	memcpy((void*)dwAddress, lala, size);
	VirtualProtect((void*)dwAddress, size, dwProtect, 0);
}

HRESULT CTools::GenerateTexture(LPDIRECT3DDEVICE9 pDevice,IDirect3DTexture9 **ppD3Dtex, DWORD colour32)
{
	if( FAILED(pDevice->CreateTexture(8, 8, 1, 0, D3DFMT_A4R4G4B4, D3DPOOL_MANAGED, ppD3Dtex, 0)) )
		return E_FAIL;

	WORD colour16 =    ((WORD)((colour32>>28)&0xF)<<12)
		|(WORD)(((colour32>>20)&0xF)<<8)
		|(WORD)(((colour32>>12)&0xF)<<4)
		|(WORD)(((colour32>>4)&0xF)<<0);

	D3DLOCKED_RECT d3dlr;    
	(*ppD3Dtex)->LockRect(0, &d3dlr, 0, 0);
	WORD *pDst16 = (WORD*)d3dlr.pBits;

	for(int xy=0; xy < 64; xy++)
		*pDst16++ = colour16;

	(*ppD3Dtex)->UnlockRect(0);

	return S_OK;
}

void CTools::ReleaseFont()
{
	if(m_pFont && !IsBadReadPtr((void*)m_pFont, 4))
	{
		m_pFont->Release();
		
	}m_pFont = NULL;

	if(m_pFont2 && !IsBadReadPtr((void*)m_pFont2, 4))
	{
		m_pFont2->Release();
		
	}m_pFont2 = NULL;

	/*if(m_sprite && !IsBadReadPtr((void*)m_sprite, 4)) {
		m_sprite->Release();	
	}
	m_sprite = NULL;

	if(m_tex && !IsBadReadPtr((void*)m_tex, 4)) {
		m_tex->Release();	
	}
	m_tex = NULL;	*/
}

void CTools::CreateSprite(LPDIRECT3DDEVICE9 pDevice)
{
	//BYTE* src = pictureCode;
	//int size = sizeof(pictureCode);
	//
	////char path[] = "C:\\baum.png";

	//D3DXIMAGE_INFO imageInfo;
	//HRESULT res = pD3DXGetImageInfoFromFileInMemory((void*)src, size, &imageInfo);

	//if(!SUCCEEDED(res)) {
	//	return;
	//}

	//res = pD3DXCreateTextureFromFileInMemoryEx(pDevice, (void*)src, size, imageInfo.Width, imageInfo.Height, D3DX_FROM_FILE, 0, D3DFMT_UNKNOWN, D3DPOOL_MANAGED, D3DX_DEFAULT, D3DX_DEFAULT, D3DCOLOR_ARGB(255, 255, 0, 255), 0, 0, &m_tex);

	//if(!SUCCEEDED(res)) {
	//	return;
	//}

	//res = pD3DXCreateSprite(pDevice, &m_sprite);

	//if(!SUCCEEDED(res)) {
	//	return;
	//}

}

void CTools::DrawSprite(int x, int y)
{

	/*if(!m_tex || !m_sprite || IsBadReadPtr((void*)m_tex, 4) || IsBadReadPtr((void*)m_sprite, 4))
		return;*/

	D3DXVECTOR3 pos = D3DXVECTOR3((FLOAT)x, (FLOAT)y, 0);

	//if(x == 0 && y == 0) pos = sprite->Position;

	m_sprite->Begin(D3DXSPRITE_SORT_TEXTURE);
	m_sprite->Draw(m_tex, 0, 0, &pos, D3DCOLOR_ARGB(180, 255, 255, 255));
	m_sprite->End();
}

void CTools::ReleaseTextures()
{
	RELEASE(Red)
	RELEASE(Yellow)
	RELEASE(Blue)
	RELEASE(Green)
	RELEASE(White)
	RELEASE(Black)
}

char* szFontName[] = {"Verdana", "Arial", "Courier New", "Georgia", "Calibri", "Cambria", "Comic Sans MS", "Corbel", "Segoe Print",
"Times New Roman", "Tahoma"};

struct Font_t {
	char face[32];
	int size;
	int weight;
};

Font_t font[] = {"Verdana", 12, FW_BOLD,
				"Arial", 14, FW_NORMAL, 
				"Courier New", 14, FW_NORMAL,
				"Georgia", 12, FW_NORMAL,
				"Calibri", 14, FW_NORMAL,
				"Cambria", 14, FW_NORMAL,
				"Comic Sans MS", 14, FW_NORMAL,
				"Corbel", 14, FW_NORMAL,
				"Segoe Print", 14, FW_NORMAL,
				"Times New Roman", 14, FW_NORMAL,
				"Tahoma", 12, FW_BOLD
};

void CTools::BuildFont(LPDIRECT3DDEVICE9 pDevice)
{
	m_pFont = NULL;
	m_pFont2 = NULL;

	pD3DXCreateFont(pDevice, font[cvars.Font].size,  0,
		font[cvars.Font].weight,
		1,
		false,
		DEFAULT_CHARSET,
		OUT_DEFAULT_PRECIS,
		ANTIALIASED_QUALITY,
		DEFAULT_PITCH|FF_DONTCARE,
		font[cvars.Font].face,
		&m_pFont);

	pD3DXCreateFont(pDevice, 26,  0,
		FW_NORMAL,
		1,
		false,
		DEFAULT_CHARSET,
		OUT_DEFAULT_PRECIS,
		ANTIALIASED_QUALITY,
		DEFAULT_PITCH|FF_DONTCARE,
		fontname,
		&m_pFont2);
}

//extern CD3DFont* pFont;

void CTools::DrawString(int x, int y, CColor color, int flag, char *text, ...)
{
	if(m_pFont == NULL) return;
	//if(!text) return;
	if(IsBadReadPtr((void*)m_pFont, 0x4)) return;

	va_list va_alist;
	char logbuf[256] = {0};
	va_start (va_alist, text);
	_vsnprintf (logbuf+strlen(logbuf), sizeof(logbuf) - strlen(logbuf), text, va_alist);
	va_end (va_alist);


	DWORD iflag = flag;
	if(iflag == 8)
		iflag = DT_RIGHT;

	RECT font_rect;
	font_rect.top = y;
	font_rect.left= x;
	font_rect.right = x-0.001;
	font_rect.bottom = y-0.001;
	m_pFont->DrawText( NULL, logbuf, -1, &font_rect, iflag|DT_NOCLIP, color.Get());	

	//if(pFont == NULL) return;
	//if(!text) return;
	//if(IsBadReadPtr((void*)pFont, 0x4)) return;

	//pFont->DrawText(x,y, color.Get(),logbuf,flag);
}

void CTools::DrawString2(int x, int y, CColor color, int flag, char *text, ...)
{
	if(m_pFont == NULL) return;
	if(!text) return;
	if(IsBadReadPtr((void*)m_pFont, 0x4)) return;

	va_list va_alist;
	char logbuf[256] = {0};
	va_start (va_alist, text);
	_vsnprintf (logbuf+strlen(logbuf), sizeof(logbuf) - strlen(logbuf), text, va_alist);
	va_end (va_alist);


	DWORD iflag = flag;
	if(iflag == 8)
		iflag = DT_RIGHT;

	RECT font_rect;
	font_rect.top = y;
	font_rect.left= x;
	font_rect.right = x-0.001;
	font_rect.bottom = y-0.001;
	m_pFont2->DrawText( NULL, logbuf, -1, &font_rect, iflag|DT_NOCLIP, color.Get());	

	//if(pFont == NULL) return;
	//if(!text) return;
	//if(IsBadReadPtr((void*)pFont, 0x4)) return;

	//pFont->DrawText(x,y, color.Get(),logbuf,flag);
}

struct QuadVertex
{
	float x,y,z,rhw;
	DWORD dwColor;
};
void CTools::DrawFilledQuad(int x, int y, int w, int h, CColor color)
{
	/*D3DRECT box = {	x, y, x+w, y+h	};
	pD3Ddev->Clear(1, &box, D3DCLEAR_TARGET, color.Get(), 0, 0);*/
	const DWORD D3D_FVF = D3DFVF_XYZRHW | D3DFVF_DIFFUSE;
	DWORD colore = color.Get();
	QuadVertex qV[4] =	
	{
		{ (float)x    , (float)(y+h), 0.0f, 0.0f, colore},
		{ (float)x    , (float)y    , 0.0f, 0.0f, colore},
		{ (float)(x+w), (float)(y+h), 0.0f, 0.0f, colore},
		{ (float)(x+w), (float)y    , 0.0f, 0.0f, colore}
	};
	//allow for translucency

	pD3Ddev->SetRenderState( D3DRS_ALPHABLENDENABLE, TRUE );
	pD3Ddev->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA );
	pD3Ddev->SetRenderState( D3DRS_ZENABLE, D3DZB_FALSE );
	pD3Ddev->SetRenderState( D3DRS_FOGENABLE, false );

	pD3Ddev->SetFVF(D3D_FVF);
	pD3Ddev->SetTexture(0, NULL);
	pD3Ddev->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP,2,qV,sizeof(QuadVertex));
}

void CTools::DrawQuad(int x, int y, int w, int h, CColor color, int thickness)
{
	D3DCOLOR rgba = color.Get();

	D3DRECT oben =		{ x,	y,		x+w,		   y+thickness		};
	D3DRECT unten =		{ x,	y+h,	x+w,		   y+h+thickness	};
	D3DRECT links =		{ x,	y,		x+thickness,   y+h				};
	D3DRECT rechts =	{ x+w,	y,		x+w+thickness, y+h+thickness	};

	pD3Ddev->Clear(1, &oben, D3DCLEAR_TARGET, rgba, 0, 0);
	pD3Ddev->Clear(1, &links, D3DCLEAR_TARGET, rgba, 0, 0);
	pD3Ddev->Clear(1, &rechts, D3DCLEAR_TARGET, rgba, 0, 0);
	pD3Ddev->Clear(1, &unten, D3DCLEAR_TARGET, rgba, 0, 0);
}

void CTools::DrawPixel(int x, int y, CColor color)
{
	D3DRECT pixel = { x, y, x+1, y+1	};
	pD3Ddev->Clear(1, &pixel, D3DCLEAR_TARGET, color.Get(), 0, 0);
}

void CTools::Crosshair(int type)
{
	D3DVIEWPORT9 oViewport;
	pD3Ddev->GetViewport(&oViewport);
	
	int centerX = oViewport.Width/2;
	int centerY = oViewport.Height/2;
	
	if(type == 1)
	{
		DrawFilledQuad(centerX - 14, centerY, 9, 1,white);
		DrawFilledQuad(centerX +5, centerY, 9, 1,white);
		DrawFilledQuad(centerX, centerY - 14, 1, 9,white);
		DrawFilledQuad(centerX, centerY + 5, 1, 9,white);
		DrawFilledQuad(centerX, centerY , 1, 1,red);
	}
	else if(type == 2)
	{
		DrawFilledQuad(centerX - 14, centerY, 9, 2,white);
		DrawFilledQuad(centerX +5, centerY, 9, 2,white);
		DrawFilledQuad(centerX, centerY - 14, 2, 9,white);
		DrawFilledQuad(centerX, centerY + 5, 2, 9,white);
		DrawFilledQuad(centerX, centerY , 1, 2,red);
	}
	else if(type == 3)
	{
		DrawFilledQuad(centerX-25,centerY,50,1,white);
		DrawFilledQuad(centerX-5,centerY,10,1,red);
		DrawFilledQuad(centerX,centerY-25,1,50,white);
		DrawFilledQuad(centerX,centerY-5,1,10,red);
	}
	else if(type == 4)
	{
		DrawFilledQuad(centerX-25,centerY,50,2,white);
		DrawFilledQuad(centerX-5,centerY,10,2,red);
		DrawFilledQuad(centerX,centerY-25,2,50,white);
		DrawFilledQuad(centerX,centerY-5,2,10,red);
	}
	else if(type == 5)
	{
		DrawQuad(centerX,0,0,centerY*2,white,1);
		DrawQuad(0,centerY,centerX*2,0,white,1);
		DrawFilledQuad(centerX,centerY-5,1,10,red);
		DrawFilledQuad(centerX-5,centerY,10,1,red);	
	}
}

float CTools::GetDistance(LTVector Local, LTVector Player)
{
	return (Player - Local).Mag();
}


void CTools::DrawLine(int ax, int ay, int bx, int by, CColor color, float width)
{
	if(!pLine || IsBadReadPtr((void*)pLine, 4)) return;

	pLine->SetWidth(width);
	pLine->SetAntialias(false);
	pLine->SetGLLines(true);

	D3DXVECTOR2 point[2];
	point[0].x = ax;
	point[0].y = ay;
	point[1].x = bx;
	point[1].y = by;

	pLine->Begin();
	pLine->Draw(point, 2, color.Get());
	pLine->End();
}

int range = 3000;
//void CTools::DrawRadarPoint(LTVector vecOriginx, LTVector vecOriginy, LTVector view, int iTeam, int iMyTeam)
//{	
//	if(g_pEngine->m_pView)
//	{
//		float dy  = vecOriginx.x - vecOriginy.x;
//		float dx  = vecOriginx.z - vecOriginy.z;
//		
//		float dyview = view.x - vecOriginy.x;
//		float dxview = view.z - vecOriginy.z;
//
//		dy *= -1;
//		dyview *= -1;
//
//		float yaw = -g_pEngine->m_pView->yaw;
//
//		float CosYaw = cos(yaw);
//		float SinYaw = sin(yaw);
//
//		float x =  dy*(-CosYaw)  + dx*SinYaw;
//		float y =  dx*(-CosYaw)  - dy*SinYaw;
//
//		float xview =  dyview*(-CosYaw)  + dxview*SinYaw;
//		float yview =  dxview*(-CosYaw)  - dyview*SinYaw;
//
//
//		if(fabs(x)>range || fabs(y)>range)
//		{ 
//			if(y>x)
//			{
//				if(y>-x) {
//					x = range*x/y;
//					y = range;
//				}  else  {
//					y = -range*y/x; 
//					x = -range; 
//				}
//			} else {
//				if(y>-x) {
//					y = range*y/x; 
//					x = range; 
//				}  else  {
//					x = -range*x/y;
//					y = -range;
//				}
//			}
//		}
//
//		if(fabs(xview)>range || fabs(yview)>range)
//		{ 
//			if(yview>xview)
//			{
//				if(yview>-xview) {
//					xview = range*xview/yview;
//					yview = range;
//				}  else  {
//					yview = -range*yview/xview; 
//					xview = -range; 
//				}
//			} else {
//				if(yview>-xview) {
//					yview = range*yview/xview; 
//					xview = range; 
//				}  else  {
//					xview = -range*xview/yview;
//					yview = -range;
//				}
//			}
//		}
//
//
//		int ScreenX = ((cvars.w-cvars.radarx)+82) + int( x/range*float(82));
//		int ScreenY = ((cvars.radary)+82) + int( y/range*float(82));
//
//		int ScreenXview = ((cvars.w-cvars.radarx)+82) + int( xview/range*float(82));
//		int ScreenYview = ((cvars.radary)+82) + int( yview/range*float(82));
//
//		CColor color;
//
//		if(iTeam != iMyTeam) {
//			color = CColor(255, 2, 0);
//		}
//		if(iTeam == iMyTeam) {
//			color = CColor(0, 100, 255);
//		}
//		g_pTools->DrawFilledQuad(ScreenX, ScreenY, 3, 3, color);
//		g_pTools->DrawLine(ScreenX+1, ScreenY+1, ScreenXview, ScreenYview, color, 1);
//		
//	}
//}

void CTools::DrawRadarPoint(LTVector vecOriginx, LTVector vecOriginy, LTVector view, CColor color)
{	
	if(g_pEngine->m_pView)
	{
		float dy  = vecOriginx.x - vecOriginy.x;
		float dx  = vecOriginx.z - vecOriginy.z;

		float dyview = view.x - vecOriginy.x;
		float dxview = view.z - vecOriginy.z;

		dy *= -1;
		dyview *= -1;

		float yaw = -g_pEngine->m_pView->yaw;

		float CosYaw = cos(yaw);
		float SinYaw = sin(yaw);

		float x =  dy*(-CosYaw)  + dx*SinYaw;
		float y =  dx*(-CosYaw)  - dy*SinYaw;

		float xview =  dyview*(-CosYaw)  + dxview*SinYaw;
		float yview =  dxview*(-CosYaw)  - dyview*SinYaw;


		if(fabs(x)>range || fabs(y)>range)
		{ 
			if(y>x)
			{
				if(y>-x) {
					x = range*x/y;
					y = range;
				}  else  {
					y = -range*y/x; 
					x = -range; 
				}
			} else {
				if(y>-x) {
					y = range*y/x; 
					x = range; 
				}  else  {
					x = -range*x/y;
					y = -range;
				}
			}
		}

		if(fabs(xview)>range || fabs(yview)>range)
		{ 
			if(yview>xview)
			{
				if(yview>-xview) {
					xview = range*xview/yview;
					yview = range;
				}  else  {
					yview = -range*yview/xview; 
					xview = -range; 
				}
			} else {
				if(yview>-xview) {
					yview = range*yview/xview; 
					xview = range; 
				}  else  {
					xview = -range*xview/yview;
					yview = -range;
				}
			}
		}


		int ScreenX = ((cvars.w-cvars.radarx)+82) + int( x/range*float(82));
		int ScreenY = ((cvars.radary)+82) + int( y/range*float(82));

		int ScreenXview = ((cvars.w-cvars.radarx)+82) + int( xview/range*float(82));
		int ScreenYview = ((cvars.radary)+82) + int( yview/range*float(82));

		//if(iTeam != iMyTeam) {
		//	color = CColor(255, 2, 0);
		//}
		//if(iTeam == iMyTeam) {
		//	color = CColor(0, 100, 255);
		//}
		g_pTools->DrawFilledQuad(ScreenX, ScreenY, 3, 3, color);
		g_pTools->DrawLine(ScreenX+1, ScreenY+1, ScreenXview, ScreenYview, color, 1);

	}
}

BYTE bCryptStart[4] = {0xEB, 0x02, 0x90, 0x91};
BYTE bCryptEnd[4] = {0xEB, 0x02, 0x90, 0x92};

BYTE xorKey = 0x34;

void CTools::Crypt(DWORD startAddress, bool protect)
{
	DWORD dwStart = dwFindPattern(startAddress, 0xFFFFFFFF, bCryptStart, "xxxx") + 4;
	DWORD dwEnd = dwFindPattern(dwStart, 0xFFFFFFFF, bCryptEnd, "xxxx");

	DWORD dwSize = dwEnd - dwStart;

	if(protect) {
		VirtualProtect((void*)dwStart, dwSize, PAGE_EXECUTE, 0);
	}
	else
		VirtualProtect((void*)dwStart, dwSize, PAGE_EXECUTE, 0);

	/*DWORD dwProtect;
	VirtualProtect((void*)dwStart, dwSize, PAGE_EXECUTE_READWRITE, &dwProtect);

	for(int i = 0; i < dwSize; i++) {

		BYTE code = *(BYTE*)(dwStart + i);

		*(BYTE*)(dwStart + i) = code ^ xorKey;
	}

	VirtualProtect((void*)dwStart, dwSize, dwProtect, 0);*/
}