#include "SDK\\Structs.h"

CMenu* pMenu = new CMenu(hackname, 100, 60, 120, false, true);
//CMenu* pFriends = new CMenu("Friends", 300, 100, 100, true, false);

char* keylist[] = {"", "Left mouse button", "Right mouse button", "Control-break", 
"Middle mouse button", "X1 mouse button", "X2 mouse button", "Not available", "Backspace", 
"TAB", "Not available", "Not available", "Clear", "Return/Enter", 
"Not available", "Not available", "Shift", "CTRL", "ALT", 
"PAUSE", "CAPS LOCK", "Not available", "Not available", "Not available", 
"Not available", "Not available", "Not available", "ESC", "Not available", "Not available", 
"Not available", "Not available", "Spacebar", "Page up", "Page down", 
"End", "Home", "Left arrow", "Up arrow", 
"Right arrow", "Down arrow", "Select", "Print", "Execute", 
"Print screen", "Insert", "Delete", "Help", 
"0", "1", "2", "3", "4", 
"5", "6", "7", "8", "9", 
"Not available", "Not available", "Not available", "Not available", 
"Not available", "Not available", "Not available", "A", "B", 
"C", "D", "E", "F", "G", 
"H", "I", "J", "K", "L", 
"M", "N", "O", "P", "Q", 
"R", "S", "T", "U", "V", 
"W", "X", "Y", "Z", "Left windows key", 
"Right windows key", "Application key", "Not available", "Sleep", "Numpad 0", 
"Numpad 1", "Numpad 2", "Numpad 3", "Numpad 4", "Numpad 5", 
"Numpad 6", "Numpad 7", "Numpad 8", "Numpad 9", "Multiply", 
"Add", "Seperator", "Subtract", "Decimal", 
"Divide", "F1", "F2", "F3", "F4", 
"F5", "F6", "F7", "F8", "F9", 
"F10", "F11", "F12", "F13", "F14", 
"F15", "F16", "F17", "F18", "F19", 
"F20", "F21", "F22", "F23", "F24", 
"Not available", "Not available", "Not available", "Not available", "Not available", 
"Not available", "Not available", "Not available", "Numlock", "Scroll lock", 
"Not available", "Not available", "Not available", "Not available", 
"Not available", "Not available", "Not available", "Not available", "Not available", 
"Not available", "Not available", "Not available", "Not available", "Not available", 
"Left Shift", "Right Shift", "Left Control", "Right Control", 
"Left Menu", "Right Menu", "Browser back", "Browser forward", "Browser refresh"};

CMenu::CMenu(char* szTitelName, int x, int y, int w, bool bOnHighlighting, bool bShowValue)
{
	iElements = 0;
	iSelected = 0;
	this->x = x;
	this->y = y;
	this->w = w;
	this->bOnHighlighting = bOnHighlighting;
	this->bShowValue = bShowValue;
	this->bMenuActive = false;
	bActive = true;
	strcpy(szTitel, szTitelName);
	memset(&subitem, 0, sizeof(subitem));
}

CMenu::~CMenu()
{
	iElements = 0;
	iSelected = 0;
}

void CMenu::AddEntry(char* szName, int* pValue, int iMin, int iMax, int iStep, char* desc)
{
	strcpy(list[iElements].szName, szName);
	list[iElements].pValue = pValue;
	list[iElements].iMin = iMin;
	list[iElements].iMax = iMax;
	list[iElements].iStep = iStep;
	if(desc) {
		strcpy(list[iElements].szDescription, desc);
	}

	++iElements;
}

bool CMenu::KeyHandling(int key)
{
	if(!bActive) return false;

	if(key == VK_UP || (!window && (GetAsyncKeyState(VK_UP) & 1)))
	{
		if(iSelected <= 0) iSelected = iElements-1;
		else iSelected--;
		return true;
	}
	if(key == VK_DOWN || (!window && (GetAsyncKeyState(VK_DOWN) & 1)))
	{
		if(iSelected >= (iElements-1)) iSelected = 0;
		else iSelected++;
		return true;
	}
	if(key == VK_LEFT || (!window && (GetAsyncKeyState(VK_LEFT) & 1)))
	{
		if(!IsBadReadPtr((void*)list[iSelected].pValue, 4)) {
			*list[iSelected].pValue -= list[iSelected].iStep;
			if(*list[iSelected].pValue < list[iSelected].iMin) *list[iSelected].pValue = list[iSelected].iMax;
			return true;
		}
	}
	if(key == VK_RIGHT || (!window && (GetAsyncKeyState(VK_RIGHT) & 1)))
	{
		if(!IsBadReadPtr((void*)list[iSelected].pValue, 4)) {
			*list[iSelected].pValue += list[iSelected].iStep;
			if(*list[iSelected].pValue > list[iSelected].iMax) *list[iSelected].pValue = list[iSelected].iMin;
			return true;
		}
	}

	return false;
}


void CMenu::Draw()
{
	g_pTools->DrawFilledQuad(x+2, y+15, 123, 12*iElements+37, black_menu);
	g_pTools->DrawQuad(x+2, y+15, 123, 12*iElements+37, white_menu, 2);
	g_pTools->DrawFilledQuad(x+2, y+40, 123, 2, white_menu);
	g_pTools->DrawString(x+123*0.5, y+22, green_menu, DT_CENTER, _s, szTitel);

	if(list[iSelected].szDescription) {
		g_pTools->DrawFilledQuad(0, cvars.h-20, cvars.w, 20, black);
		g_pTools->DrawFilledQuad(0, cvars.h-20, cvars.w, 1, white);
		g_pTools->DrawString(cvars.w/2, cvars.h-15, white, DT_CENTER, _s, list[iSelected].szDescription);
	}

	for(int i = 0; i < iElements; i++)
	{
		if(i != iSelected)
		{
			/*if(bOnHighlighting && *list[i].pValue)
			{
				g_pTools->DrawString(x+10, y+46+(12*i), 255, 255, 0, 255, DT_LEFT, "%s", list[i].szName);
				if(bShowValue && list[i].szName[0] != '-') g_pTools->DrawString(x+130, y+46+(12*i), 0, 255, 0, 255, D3DFONT_RIGHT, "%d", *list[i].pValue);
			}
			else
			{*/
				if(!IsBadReadPtr((void*)list[iSelected].pValue, 4)) {
					g_pTools->DrawString(x+10, y+46+(12*i), white_menu, DT_LEFT, _s, list[i].szName);
					if(bShowValue && list[i].szName[0] != '-') g_pTools->DrawString(x+120, y+46+(12*i), white_menu, DT_RIGHT, _d, *list[i].pValue);
				}
			//}
		}
		else
		{
			if(!IsBadReadPtr((void*)list[iSelected].pValue, 4)) {
				g_pTools->DrawString(x+10, y+46+(12*i), green_menu, DT_LEFT, _s, list[i].szName);
				if(bShowValue && list[i].szName[0] != '-')  {
					
					g_pTools->DrawString(x+120, y+46+(12*i), green_menu, DT_RIGHT, _d, *list[i].pValue);
					if(list[i].iMax == 255) {
						if(*list[i].pValue <= 0xA8 && strlen(keylist[*list[i].pValue])) {
							g_pTools->DrawFilledQuad(x+137, y+44+(12*i), 120, 17, black);
							g_pTools->DrawQuad(x+137, y+44+(12*i), 120, 17, white, 1);
							g_pTools->DrawQuad(x+w+5, y+44+(12*i)+8, 12, 0, white, 1);
							g_pTools->DrawString(x+141, y+46+(12*i), green_menu, DT_LEFT, _s, keylist[*list[i].pValue]);
						}
					}
				}
			}
		}
	}
}
