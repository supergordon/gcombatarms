#ifndef _ADDRESS_
#define _ADDRESS_

struct addresss {
		DWORD address;
		int* cvar; //cvar pointer
		BYTE patch[100];
		BYTE backup[100];
		int size;
		int old;
		void* function;
		bool hook;
	};

class CAddress
{
public:
	CAddress();
	addresss addy[100];
	void Add(DWORD adresse, int* var, BYTE* p, BYTE* b, int size);
	void AddNakedHook(DWORD adresse, int* var, void* function, BYTE* b);
	void Apply();
	void Restore();
	int count;
private:
	
}; extern CAddress* g_pAddress;

#endif