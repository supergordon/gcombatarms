#ifndef _D3D_H_
#define _D3D_H_

#include "SDK\\D3D\\d3d9.h"


extern HRESULT __stdcall hkSetStreamSource(LPDIRECT3DDEVICE9 pDevice, UINT StreamNumber,IDirect3DVertexBuffer9* pStreamData,UINT OffsetInBytes,UINT Stride);
extern HRESULT __stdcall hkEndScene(LPDIRECT3DDEVICE9 pDevice);
extern void __cdecl hkReset(LPDIRECT3DDEVICE9 pDevice, D3DPRESENT_PARAMETERS* pPresentationParameters);
extern HRESULT __stdcall hkDrawIndexedPrimitive(LPDIRECT3DDEVICE9 pDevice, D3DPRIMITIVETYPE PrimitiveType,INT BaseVertexIndex,UINT MinVertexIndex,UINT NumVertices,UINT startIndex,UINT primCount);

typedef void (__cdecl* Reset_t)(LPDIRECT3DDEVICE9 pDevice, D3DPRESENT_PARAMETERS* pPresentationParameters);
typedef HRESULT	(__stdcall* EndScene_t)(LPDIRECT3DDEVICE9 pDevice);
typedef HRESULT (__stdcall* DrawIndexedPrimitive_t)(LPDIRECT3DDEVICE9, D3DPRIMITIVETYPE PrimitiveType,INT BaseVertexIndex,UINT MinVertexIndex,UINT NumVertices,UINT startIndex,UINT primCount);
typedef HRESULT (__stdcall* DrawIndexedPrimitive_t2)(LPDIRECT3DDEVICE9, D3DPRIMITIVETYPE PrimitiveType,INT BaseVertexIndex,UINT MinVertexIndex,UINT NumVertices,UINT startIndex,UINT primCount);
typedef HRESULT (__stdcall* SetStreamSource_t)(LPDIRECT3DDEVICE9 pDevice, UINT StreamNumber,IDirect3DVertexBuffer9* pStreamData,UINT OffsetInBytes,UINT Stride);

typedef HRESULT (__stdcall* SetTexture_t)(DWORD State, IDirect3DBaseTexture9* pTexture);


extern EndScene_t pEndScene;
extern Reset_t pReset;
extern DrawIndexedPrimitive_t pDrawIndexedPrimitive;
extern SetStreamSource_t pSetStreamSource;

extern DrawIndexedPrimitive_t2 pDrawIndexedPrimitive2;

extern void InitializeOffsets();

extern LPDIRECT3DDEVICE9 pD3Ddev;

#endif