#ifndef _TOOLS_H_
#define _TOOLS_H_

#include "SDK\\Structs.h"

#define PROJECT_FEAR
#define _FINAL
#define INCLUDE_AS_ENUM
#include "sdk/game/shared/debugnew.h"
#include "sdk/engine/sdk/inc/iltmessage.h"
#include "sdk/game/clientshelldll/gameclientshell.h"

#include "CColor.h"

class CTools
{
public:
	CTools();
	void LogText(char* szText, ...);
	void *DetourFunc(BYTE *src, const BYTE *dst, const int len);
	bool RetourFunc(BYTE *src, BYTE *restore, const int len);
	DWORD dwFindPattern(DWORD dwAddress,DWORD dwLen,BYTE *bMask,char * szMask);
	void Patch(DWORD dwAddress, BYTE* lala, int size);
	HRESULT GenerateTexture(LPDIRECT3DDEVICE9 pDevice,IDirect3DTexture9 **ppD3Dtex, DWORD colour32);
	void ReleaseFont();
	void ReleaseTextures();
	void BuildFont(LPDIRECT3DDEVICE9 pDevice);
	void CreateSprite(LPDIRECT3DDEVICE9 pDevice);
	void DrawSprite(int x, int y);
	void DrawString(int x, int y, CColor color, int flag, char *text, ...);
	void DrawString2(int x, int y, CColor color, int flag, char *text, ...);
	void DrawFilledQuad(int x, int y, int w, int h, CColor color);
	void DrawQuad(int x, int y, int w, int h, CColor color, int thickness);
	void DrawPixel(int x, int y, CColor color);
	void Crosshair(int type);
	float GetDistance(LTVector Local, LTVector Player);
	void DrawRadarPoint(LTVector vecOriginx, LTVector vecOriginy, LTVector view, CColor color);
	void DrawLine(int ax, int ay, int bx, int by, CColor color, float width);

	void Crypt(DWORD startAddress, bool protect);

	ID3DXSprite* m_sprite;
	IDirect3DTexture9* m_tex;
}; 

extern CTools* g_pTools;

extern ID3DXFont* m_pFont;
extern ID3DXFont* m_pFont2;
extern ID3DXLine* pLine;
extern LPDIRECT3DTEXTURE9 texRed, texYellow, texBlue, texGreen, texWhite, texBlack;

#endif