#ifndef _ENGINE_H_
#define _ENGINE_H_

#include "sdk/structs.h"


class CEngine
{
public:
	CEngine();
	bool Initialize();
	bool Update();
	void Hook();
	void Unhook();
	bool IsVisible(LTVector start, LTVector end, HOBJECT obj);
	bool WorldToScreen(LTVector vPlayer, int &x, int &y);
	void AimToPoint(LTVector vPos, int fCorrection);
	void AimToPoint(float pitch, float yaw);
	ClientInfo* GetClientByID(DWORD index);
	int GetMaxIndex(DWORD type);
	CEntity *GetEntityByIndex(DWORD offset, int index);
	void GetCamera();
	void FlyToView(LTVector &pos);
	void FlyToView();

	CGameClientShell_t* m_pGameClientShell;
	CLTBase* m_pLTBase;
	CLTModel* m_pLTModel;
	CLTClient* m_pLTClient;
	CLTPhysicClient *m_pLTPhysic;
	CLTCommon* m_pLTCommon ;
	CLTCursor* m_pLTCursor;
	CLTDrawPrim* m_pLTDrawPrim;
	CLTServer* m_pLTServer;
	CPlayerManager* m_pPlayerManager;
	CViewangles* m_pView;
	CLocal* m_pLocal;

	DWORD m_dwView;
	DWORD m_dwLocal;
	DWORD m_dwGameClientShell;
	DWORD m_dwGamemode;
	DWORD m_dwLocalIndex;
	DWORD m_dwWorldToScreen;
	HOBJECT m_hCamera;
	LTVector m_vLocalPosition;
	LTVector m_vOldRealPosition;

	int m_iKills;
	int m_iDeaths;
	float m_fKDR;
	
	DWORD m_dwHSScans;

private:
	
};

extern CEngine *g_pEngine;

typedef LTRESULT (__stdcall *SendToServer_t)(ILTMessage_Read *pMsg, uint32 flags);
typedef LTRESULT (__stdcall* SetObjectPos_t)(HLOCALOBJ hObj, LTVector& vPos, DWORD bla);

extern D3DVIEWPORT9 viewPort;
extern D3DXMATRIX projection, view, world, identity;

#endif
