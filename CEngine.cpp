#include "sdk/structs.h"

CEngine *g_pEngine = new CEngine;

SendToServer_t pSendToServer = 0;
SetObjectPos_t pSetObjectPos = 0;

CEngine::CEngine()
{
	m_pGameClientShell = NULL;
	m_pLTBase = NULL;
	m_pLTModel = NULL;
	m_pLTClient = NULL;
	m_pLTPhysic = NULL;
	m_pLTCommon = NULL;
	m_pLTCursor = NULL;
	m_pLTDrawPrim = NULL;
	m_pLTServer = NULL;
	m_pPlayerManager = NULL;
	m_pView = NULL;
	m_pLocal = NULL;

	m_hCamera = NULL;

	m_dwView = NULL;
	m_dwLocal = NULL;
	m_dwGameClientShell = NULL;
	m_dwGamemode = 1337;
	m_dwHSScans = 0;
}

DWORD kko = 0;

bool CEngine::Initialize()
{
	DWORD dwCShell = (DWORD)GetModuleHandle(cshell);
	DWORD dwClientFX = (DWORD)GetModuleHandle(clientfx);
//83 EC 28 F3 0F 10 44 24 ?? 56
	m_dwWorldToScreen = g_pTools->dwFindPattern((DWORD)GetModuleHandle(0), 0x760000, (PBYTE)"\x83\xEC\x28\xF3\x0F\x10\x44\x24\x00\x56", /*xxxxxxxx?x*/XorStr<0x03,11,0xD4B51337>("\x7B\x7C\x7D\x7E\x7F\x70\x71\x72\x34\x74"+0xD4B51337).s);
	
	//DWORD dwTempViewAngles = g_pTools->dwFindPattern(dwCShell, 0x990000, (PBYTE)"\x8B\x0D\x00\x00\x00\x00\x8B\x54\x24\x20\x8B\x49", /*xx????xxxxxx*/XorStr<0x19,13,0xB989EC70>("\x61\x62\x24\x23\x22\x21\x67\x58\x59\x5A\x5B\x5C"+0xB989EC70).s); 
	//if(!dwTempViewAngles) {

		DWORD dwTempViewAngles = g_pTools->dwFindPattern(dwCShell, 0x990000, (PBYTE)"\x8B\x0D\x00\x00\x00\x00\x8B\x54\x24\x10\x8B\x49", /*xx????xxxxxx*/XorStr<0x19,13,0xB989EC70>("\x61\x62\x24\x23\x22\x21\x67\x58\x59\x5A\x5B\x5C"+0xB989EC70).s); 
	//}
	//
	if(dwTempViewAngles) {
		m_dwView = *(DWORD*)(dwTempViewAngles + 2);
	}

	//MessageBox(0, "1", 0, 0);
	//\xA1\x00\x00\x00\x00\x8B\x40\x30\x83\xF8\x06"
	//ASCII Strings to find: name AmmoWnd fire_icon AmmoClip Ammo
	//ScopePosOff
	//CCLientWeaponMgr::ForceChangeWeapon
	//m_dwView = 0x37829338;


	DWORD dwTempPlayerPointer = g_pTools->dwFindPattern(dwClientFX, 0x77000, (PBYTE)"\x8B\x44\x24\x04\xA3\x00\x00\x00\x00\xC3\xCC\xCC\xCC\xCC\xCC\xCC\x0F", /*xxxxx????xxxxxxxx*/XorStr<0xBB,18,0x5B72F1AA>("\xC3\xC4\xC5\xC6\xC7\xFF\xFE\xFD\xFC\xBC\xBD\xBE\xBF\xB0\xB1\xB2\xB3"+0x5B72F1AA).s);
	if(dwTempPlayerPointer) {
		m_dwLocal = *(DWORD*)(dwTempPlayerPointer+5);
	}


	DWORD dwpLTClient = g_pTools->dwFindPattern(dwCShell, 0x990000, (PBYTE)"\x8B\x0D\x00\x00\x00\x00\x8B\x11\x50\x8B\x42\x18",/*xx????xxxxxx*/XorStr<0x16,13,0x860A665A>("\x6E\x6F\x27\x26\x25\x24\x64\x65\x66\x67\x58\x59"+0x860A665A).s); 
	if(dwpLTClient) {
		dwpLTClient = *(DWORD*)(dwpLTClient+0x2);
		dwpLTClient = *(DWORD*)(dwpLTClient);
	}

	////falsch, alt DC, neu E4, 
	DWORD dwpGameClientShell = g_pTools->dwFindPattern(dwCShell, 0x990000, (PBYTE)"\x8B\x0D\x00\x00\x00\x00\x8B\x01\x8B\x90\xE4",/*xx????xxxxx*/XorStr<0xC0,12,0x3933FEBB>("\xB8\xB9\xFD\xFC\xFB\xFA\xBE\xBF\xB0\xB1\xB2"+0x3933FEBB).s); //alt E4
	if(dwpGameClientShell) {
		dwpGameClientShell = *(DWORD*)(dwpGameClientShell+0x2);
		dwpGameClientShell = *(DWORD*)(dwpGameClientShell); // 376E71F0, 37720F54
		m_dwGameClientShell = dwpGameClientShell;
		dwpGameClientShell = *(DWORD*)(dwpGameClientShell);
	}

	if(!dwTempViewAngles || !dwTempPlayerPointer || !dwpLTClient || !dwpGameClientShell) {
		MessageBox(0, /*Hack stopped working!\nPatched!*/XorStr<0x4B,31,0xF420E057>("\x03\x2D\x2E\x25\x6F\x23\x25\x3D\x23\x24\x30\x32\x77\x2F\x36\x28\x30\x35\x33\x39\x7E\x6A\x31\x03\x17\x07\x0D\x03\x03\x49"+0xF420E057).s, /*Warning*/XorStr<0x3D,8,0xF6988FF3>("\x6A\x5F\x4D\x2E\x28\x2C\x24"+0xF6988FF3).s, MB_ICONWARNING);
		ExitProcess(0x1337);
		return false;
	}

	kko = dwpLTClient;
	m_pLTBase = (CLTBase*)dwpLTClient;
	m_pGameClientShell = (CGameClientShell_t*)dwpGameClientShell;
	m_pLocal = (CLocal*) *(DWORD*)m_dwLocal;

	//g_pTools->LogText("gameclientshell: %X", m_pGameClientShell);

	//m_pLTServer = (CLTServer*)0x678568;

	while(!m_pLTBase) { m_pLTBase = (CLTBase*)dwpLTClient; Sleep(100); }
	while(!m_pLTClient) { m_pLTClient = m_pLTBase->pLTClient; Sleep(100); }
	while(!m_pLTModel) { m_pLTModel = *m_pLTClient->LTModel(); Sleep(100); }
	while(!m_pLTCommon) { m_pLTCommon = *m_pLTClient->LTCommonClient(); Sleep(100); }
	while(!m_pView) { m_pView = (CViewangles*)*(DWORD*)m_dwView; Sleep(100); }
	while(!m_pLTDrawPrim) { m_pLTDrawPrim = *m_pLTClient->LTDrawPrim(); Sleep(100); }

	//DWORD dwGameClientshell = m_dwGameClientShell;
	//__asm mov ecx, dwGameClientshell
	//while(!m_pPlayerManager) { m_pPlayerManager = m_pGameClientShell->GetPlayerManager(); }

	return true;
}

bool CEngine::Update()
{
	m_pLTBase = (CLTBase*)kko;
	if(!m_pLTBase) return false;

	m_pLTClient = m_pLTBase->pLTClient;
	m_pLTModel = *m_pLTClient->LTModel();
	m_pLTPhysic = *m_pLTClient->LTPhysicsClient();
	m_pLTCommon = *m_pLTClient->LTCommonClient();
	m_pView = (CViewangles*)*(DWORD*)m_dwView;
	m_pLocal = (CLocal*) *(DWORD*)m_dwLocal;
	m_pLTDrawPrim = *m_pLTClient->LTDrawPrim();

	DWORD dwGameClientshell = m_dwGameClientShell;
	__asm mov ecx, dwGameClientshell
	m_pPlayerManager = m_pGameClientShell->GetPlayerManager();

	if(!m_pLTBase || !m_pLTClient || !m_pLTModel || !m_pLTCommon || !m_pView || !m_pGameClientShell || !m_pLocal || !m_pLTDrawPrim || !m_pPlayerManager) {
		return false;
	}

	return true;
}
 
//#pragma intrinsic(_ReturnAddress)
//extern "C" void* _ReturnAddress();

LTRESULT __stdcall hkSendToServer(ILTMessage_Read *pMsg, uint32 flags)
{
	BYTE data[1000] = "";
	pMsg->ReadData(&data, pMsg->Size());


	//C8
	if(data[0] == 0xD1) {

		return LT_INVALIDDATA;
	}

	return pSendToServer(pMsg, flags);
}

extern "C" void *_ReturnAddress(void);
#pragma intrinsic(_ReturnAddress)

LTRESULT __stdcall hkSetObjectPos(HLOCALOBJ hObj, LTVector& vPos, DWORD bla)
{
	__asm pushad
	////DWORD ret = (DWORD)_ReturnAddress();
	static LTVector curpos;
	uint32 dwType = 1337;
		
	DWORD blaa = (DWORD)g_pEngine->m_pLTClient->LTCommonClient();
	__asm mov ecx, blaa
	g_pEngine->m_pLTCommon->GetObjectType(hObj, &dwType);


	if(dwType == 5) g_pEngine->m_hCamera = hObj;

	//if(dwType == 5) {
	//	g_pTools->LogText("Returnaddress: %X", ret);

	//}
	//bAlive = true;
	
	static bool bOnce = false;
	if(bGhostmode && !cvars.Telekill && bAlive)
	{	
		if(dwType == 5)
		{
			if(!bOnce)
			{
				bOnce = true;
				curpos = vPos;
				g_pEngine->m_vOldRealPosition = vPos;
			}
			g_pEngine->FlyToView(curpos);
			__asm popad
			return pSetObjectPos(hObj, curpos, bla);
		}
	}
	else bOnce = false;

	
	if(cvars.Telekill && bAlive)
	{
		LTVector blaa(vTelePlayer.x, vTelePlayer.y, vTelePlayer.z);
		/*if(cvars.Telekill == 2)
		{
			blaa.x = vTelePlayer.x;
		}*/

		if(dwType == 5 && (vTelePlayer.x != 0 || vTelePlayer.y != 0 || vTelePlayer.z != 0))
		{
			vTelePlayer = LTVector(0,0,0);
			__asm popad
			return pSetObjectPos(hObj, blaa, bla);
		}
	}

	__asm popad
	return pSetObjectPos(hObj, vPos, bla);
}

__declspec (naked) void SetObjectPos_Gate()
{
	__asm {

		call hkSetObjectPos
		mov dword ptr ds: [esi+0x9C], 1
		pop esi
		add esp, 0x2C
		retn
	}
}

void CEngine::Hook()
{
	DWORD dwSOPGate = g_pTools->dwFindPattern((DWORD)GetModuleHandle(cshell), 0x990000, PBYTE("\xFF\xD0\xC7\x86\x9C\x00"), /*xxxxxx*/XorStr<0x1E,7,0x4EC7ED2E>("\x66\x67\x58\x59\x5A\x5B"+0x4EC7ED2E).s);
	DWORD dwSetObjectPos = (DWORD)m_pLTClient + (4*49);
	pSetObjectPos = (SetObjectPos_t) *(DWORD*)dwSetObjectPos;

	g_pAddress->AddNakedHook(dwSOPGate, &cvars.Detectable, SetObjectPos_Gate, PBYTE("\xFF\xD0\xC7\x86\x9C"));
	//g_pTools->LogText("LTClient: %X", m_pLTClient);
	/*DWORD dwSetObjectPos = (DWORD)m_pLTClient + (4*49);
	pSetObjectPos = (SetObjectPos_t) *(DWORD*)dwSetObjectPos;*/

	//g_pTools->LogText("SOP: %X", dwSetObjectPos);
	//g_pHackshield->dwSetObjectPos = *(DWORD*)dwSetObjectPos;

	//g_pTools->LogText("%X %X", dwSetObjectPos, g_pHackshield->dwSetObjectPos);

	//memcpy(&g_pHackshield->bSetObjectPos, (void*)g_pHackshield->dwSetObjectPos, 10);

	//pSetObjectPos = (SetObjectPos_t)g_pTools->DetourFunc((PBYTE)g_pHackshield->dwSetObjectPos, (PBYTE)hkSetObjectPos, 5);

	//pSetObjectPos = (SetObjectPos_t) *(DWORD*)dwSetObjectPos;
	
	/*DWORD dwProtect;
	VirtualProtect((void*)dwSetObjectPos, 4, PAGE_EXECUTE_READWRITE, &dwProtect);	
	*(DWORD*)dwSetObjectPos = (DWORD)&hkSetObjectPos;
	VirtualProtect((void*)dwSetObjectPos, 4, dwProtect, 0);*/
	
	/*DWORD dwSendToServer = (DWORD)m_pLTClient + (4*86);

	VirtualProtect((void*)dwSendToServer, 4, PAGE_EXECUTE_READWRITE, &dwProtect);	
	pSendToServer = (SendToServer_t) *(DWORD*)dwSendToServer;
	VirtualProtect((void*)dwSendToServer, 4, dwProtect, 0);

	*(DWORD*)dwSendToServer = (DWORD)&hkSendToServer;*/
}

void CEngine::Unhook()
{
	//g_pTools->Patch(g_pHackshield->dwSetObjectPos, g_pHackshield->bSetObjectPos, 10);
	/*DWORD dwSetObjectPos = (DWORD)m_pLTClient + (4*49);
	*(DWORD*)dwSetObjectPos = (DWORD)pSetObjectPos;

	DWORD dwSendToServer = (DWORD)m_pLTClient + (4*86);
	*(DWORD*)dwSendToServer = (DWORD)pSendToServer;*/
}

bool CEngine::IsVisible(LTVector start, LTVector end, HOBJECT obj)
{
	IntersectQuery iQuery;
	IntersectInfo iInfo;	
	memset(&iQuery, 0, sizeof(iQuery));

	iQuery.m_From = start;
	iQuery.m_To = end;

	//uint32 dwFlags;
	//DWORD bla = (DWORD)pLTClient->LTCommonClient();
	//__asm mov ecx, bla
	//pLTCommon->GetObjectFlags(obj, OFT_Flags, dwFlags);
	//if(dwFlags & FLAG_VISIBLE) return true;
	//if((dwFlags & FLAG_RAYHIT)) return false;

	return !m_pLTBase->IntersectSegment(iQuery, &iInfo);
}

/*
0048FE90    83EC 28               SUB ESP,28
0048FE93    F3:0F104424 2C        MOVSS XMM0,DWORD PTR SS:[ESP+2C]
0048FE99    56                    PUSH ESI
0048FE9A    8D81 54430000         LEA EAX,DWORD PTR DS:[ECX+4354]
0048FEA0    50                    PUSH EAX
0048FEA1    8DB1 94430000         LEA ESI,DWORD PTR DS:[ECX+4394]
0048FEA7    56                    PUSH ESI
0048FEA8    8D91 D4430000         LEA EDX,DWORD PTR DS:[ECX+43D4]
0048FEAE    52                    PUSH EDX
0048FEAF    81C1 3C430000         ADD ECX,433C
0048FEB5    51                    PUSH ECX
0048FEB6    F3:0F114424 14        MOVSS DWORD PTR SS:[ESP+14],XMM0
0048FEBC    F3:0F104424 44        MOVSS XMM0,DWORD PTR SS:[ESP+44]
0048FEC2    8D4424 14             LEA EAX,DWORD PTR SS:[ESP+14]
0048FEC6    50                    PUSH EAX
0048FEC7    8D4C24 24             LEA ECX,DWORD PTR SS:[ESP+24]
0048FECB    F3:0F114424 1C        MOVSS DWORD PTR SS:[ESP+1C],XMM0
0048FED1    F3:0F104424 4C        MOVSS XMM0,DWORD PTR SS:[ESP+4C]
0048FED7    51                    PUSH ECX
0048FED8    F3:0F114424 24        MOVSS DWORD PTR SS:[ESP+24],XMM0
0048FEDE    E8 816A0C00           CALL <JMP.&d3dx9_35.D3DXVec3Project>
0048FEE3    8B4424 3C             MOV EAX,DWORD PTR SS:[ESP+3C]
0048FEE7    F3:0F104424 10        MOVSS XMM0,DWORD PTR SS:[ESP+10]
0048FEED    F3:0F1100             MOVSS DWORD PTR DS:[EAX],XMM0
0048FEF1    F3:0F104424 14        MOVSS XMM0,DWORD PTR SS:[ESP+14]
0048FEF7    F3:0F1140 04          MOVSS DWORD PTR DS:[EAX+4],XMM0
0048FEFC    F3:0F104424 18        MOVSS XMM0,DWORD PTR SS:[ESP+18]
0048FF02    56                    PUSH ESI
0048FF03    F3:0F1140 08          MOVSS DWORD PTR DS:[EAX+8],XMM0
0048FF08    0F57C0                XORPS XMM0,XMM0
0048FF0B    8D5424 08             LEA EDX,DWORD PTR SS:[ESP+8]
0048FF0F    52                    PUSH EDX
0048FF10    8D4424 24             LEA EAX,DWORD PTR SS:[ESP+24]
0048FF14    50                    PUSH EAX
0048FF15    F3:0F114424 28        MOVSS DWORD PTR SS:[ESP+28],XMM0
0048FF1B    F3:0F114424 2C        MOVSS DWORD PTR SS:[ESP+2C],XMM0
0048FF21    F3:0F114424 30        MOVSS DWORD PTR SS:[ESP+30],XMM0
0048FF27    F3:0F114424 34        MOVSS DWORD PTR SS:[ESP+34],XMM0
0048FF2D    E8 2C6A0C00           CALL <JMP.&d3dx9_35.D3DXVec3Transform>
0048FF32    8B4C24 40             MOV ECX,DWORD PTR SS:[ESP+40]
0048FF36    F3:0F104424 24        MOVSS XMM0,DWORD PTR SS:[ESP+24]
0048FF3C    F3:0F1101             MOVSS DWORD PTR DS:[ECX],XMM0
0048FF40    5E                    POP ESI
0048FF41    83C4 28               ADD ESP,28
0048FF44    C2 1400               RETN 14
*/

#define ADDR_W2S			0x48FE90 //0x48ED60
#define ADDR_DRAWPRIM		0x7DF964 //0x6DD078

//83 EC 28 F3 0F 10 44 24 ?? 56
bool CEngine::WorldToScreen(LTVector vPlayer, int &a, int &b)
{
	//D3DXVECTOR3 vWorld(vPlayer.x,vPlayer.y,vPlayer.z);
	//
	//pD3Ddev->GetViewport(&viewPort);
	//
	//D3DXVECTOR3 vScreen;
	//pD3DXVec3Project(&vScreen, &vWorld, &viewPort, &g_pEngine->m_pLTDrawPrim->Projection, &g_pEngine->m_pLTDrawPrim->View, &g_pEngine->m_pLTDrawPrim->World);
	//if(vScreen.z < 1)
	//{
	//	a = (int)vScreen.x;
	//	b = (int)vScreen.y;
	//	return true;
	//}
	//return false;

	float x = vPlayer.x;
	float y = vPlayer.y;
	float z = vPlayer.z;

	DWORD dwDrawPrim = (DWORD)g_pEngine->m_pLTClient->LTDrawPrim();
	DWORD dwW2S = m_dwWorldToScreen;
	LTVector Screen;

	LTVector Unk;
	/*LTVector Screen;
	this->m_pLTDrawPrim->Project(vPlayer.x, vPlayer.y, vPlayer.z, &Screen, &Unk);
	
	if(Screen.z < 1.0f) {
		a = Screen.x;
		b = Screen.y;
		return true;
	}

	return false;*/

	_asm
	{
		mov ecx, dwDrawPrim
		lea  eax, Unk
		push eax
		lea eax, dword ptr [Screen]
		push eax
		push z
		push y
		push x
		mov eax, dwW2S
		call eax
	}

	bool Ret =  (Screen.z < 1.0f);

	if(Ret) {
		a = Screen.x;
		b = Screen.y;
		//delete Screen;
		return true;
	}

	//delete Screen;

	return false;
}

void CEngine::AimToPoint(float pitch, float yaw)
{
	if(!g_pEngine->m_pView) return;

	g_pEngine->m_pView->pitch = pitch;
	g_pEngine->m_pView->yaw = yaw;
}

void CEngine::AimToPoint(LTVector vPos, int fCorrection)
{
	if(!g_pEngine->m_pView) return;
	
	LTVector Length;
	LTVector kamera(0,0,0);
	//m_pLTClient->GetObjectPos(g_pEngine->m_hCamera, &kamera);	
	kamera = m_vLocalPosition;
	
	Length.x = vPos.x - kamera.x;
	Length.y = vPos.z - kamera.z;
	Length.z = (vPos.y-fCorrection) - kamera.y;

	double dist = (double)sqrt((Length.x*Length.x) + (Length.y*Length.y));
	double pitch = (double)atan2f(Length.z, dist);
	double yaw = (double)atanf(Length.y/Length.x);

	if(Length.x >= 0 && Length.y >= 0) //1
		yaw -= (M_PI_2); //-90�

	if(Length.x < 0 && Length.y >= 0) //2
		yaw += (M_PI_2); //+90�

	if(Length.x < 0 && Length.y < 0) //3
		yaw += (M_PI_2); //+90�

	if(Length.x >= 0 && Length.y < 0) //4
		yaw -= (M_PI_2); //-90�

	pitch *= -1;
	yaw *= -1;

	if(cvars.Aimsmooth) {

		double diff[3];
		diff[0] = pitch - g_pEngine->m_pView->pitch;
		diff[1] = yaw - g_pEngine->m_pView->yaw;

		diff[0] /= cvars.Aimsmooth;
		diff[1] /= cvars.Aimsmooth;

		pitch = g_pEngine->m_pView->pitch + diff[0];
		yaw = g_pEngine->m_pView->yaw + diff[1];
	}

	AimToPoint(pitch, yaw);
}

extern DWORD dwNextOffset;

//8D 8E ?? ?? ?? ?? E8 ?? ?? ?? ?? 8B 06 8B 90
//373A5466    68 60446737     PUSH cshell.37674460                 ; ASCII "CGameClientShell::OnEngineInitialized if(!GetPlayerMgr() || !GetPlayerMgr()->Init())"
//373A5474    68 E8436737     PUSH cshell.376743E8                 ; ASCII "Failed to init UISystem"
ClientInfo* CEngine::GetClientByID(DWORD index)
{
	if(index >= 0xFF) return 0;

	DWORD dwGameClientshell = m_dwGameClientShell;

	__asm mov ecx, dwGameClientshell
	ClientInfo* ptr = m_pGameClientShell->GetPlayerInfo()->user;
	if(!ptr) return 0;

	while(ptr)
	{
		if(ptr->index == index) return ptr;
		if(dwNextOffset) {
			ptr = (ClientInfo*)( *(DWORD*)(((DWORD)ptr + dwNextOffset)) );
		}
		else {
			ptr = ptr->next;
		}
	}
	return 0;
}

CEntity *CEngine::GetEntityByIndex(DWORD offset, int index)
{	
	DWORD dwGameClientshell = m_dwGameClientShell;

	__asm mov ecx, dwGameClientshell
	return m_pGameClientShell->GetSFXMgr()->pList[offset].pEntity[index];
}

int CEngine::GetMaxIndex(DWORD type)
{
	DWORD dwGameClientshell = m_dwGameClientShell;

	__asm mov ecx, dwGameClientshell
	return m_pGameClientShell->GetSFXMgr()->pList[type].maxindex;
}

void CEngine::GetCamera()
{
	//m_pLTClient->GetObjectPos(m_hCamera, &m_vLocalPosition);
	m_vLocalPosition = m_pView->cam1->pos1;
}

void CEngine::FlyToView()
{
	if(!m_pLocal || !cvars.Glitcher || cvars.Ghostmode) return;

	LTVector move;
	float Speed = 1.0 + float(cvars.Glitcher/5);
	move.x = sin(m_pView->yaw)*Speed;
	move.z = cos(m_pView->yaw)*Speed;
	move.y = sin(-m_pView->pitch)*Speed;

	if(!GetAsyncKeyState(0x58)) return;

	m_pLocal->pos += move;
}


void CEngine::FlyToView(LTVector &pos)
{
	if(!bGhostmode) return;
	if(!m_pView) return;
	if(pMenu->bMenuActive) return;

	LTVector move;
	float Speed = 15.0;
	move.x = sin(m_pView->yaw)*Speed;
	move.z = cos(m_pView->yaw)*Speed;
	move.y = sin(-m_pView->pitch)*Speed;

	if(GetAsyncKeyState(VK_LEFT)) {
		move.x = sin(m_pView->yaw-M_PI_2)*Speed;
		move.z = cos(m_pView->yaw-M_PI_2)*Speed;
		pos += move;
	}
	if(GetAsyncKeyState(VK_RIGHT)) {
		move.x = sin(m_pView->yaw+M_PI_2)*Speed;
		move.z = cos(m_pView->yaw+M_PI_2)*Speed;
		pos += move;
	}
	if(GetAsyncKeyState(VK_UP)) pos += move;
	if(GetAsyncKeyState(VK_DOWN)) pos -= move;
}
