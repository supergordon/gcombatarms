#include <winsock2.h>
#include <iostream>
#include "CSocks.h"
#include "xorstr.h"

#pragma comment(lib, "ws2_32.lib")

CSocks *pSocks = new CSocks;

CSocks::CSocks()
{
	sock = 0;
}

CSocks::~CSocks()
{
	closesocket(sock);
}

int CSocks::ConnectToServer(char *szServer, UINT uPort)
{
	char serverip[20];
	sockaddr_in addr;
	WSADATA wsaData;

	WSAStartup(MAKEWORD(2,0), &wsaData);
	hostent *dns = gethostbyname(szServer);

	sprintf(serverip, /*%u.%u.%u.%u*/XorStr<0x26,12,0x23286F6E>("\x03\x52\x06\x0C\x5F\x05\x09\x58\x00\x0A\x45"+0x23286F6E).s,
		(unsigned char) dns->h_addr_list[0][0],	(unsigned char) dns->h_addr_list[0][1],
		(unsigned char) dns->h_addr_list[0][2], (unsigned char) dns->h_addr_list[0][3]);

	addr.sin_family = AF_INET;
	addr.sin_port = htons(uPort);
	addr.sin_addr.s_addr = inet_addr(serverip);

	sock = socket(AF_INET, SOCK_STREAM, 0);
	if(sock == 0 || sock == -1) return 0;

	int k =  connect(sock, (sockaddr *) &addr, sizeof(addr));

	//u_long iMode = 1;
	//ioctlsocket(sock, FIONBIO, &iMode);

	return k;
}

int CSocks::Send(void* data, int size)
{	
	return send(sock, (char*)data, size, 0);
}

int CSocks::Recv(void *data, int size)
{
	//BYTE decrypt[10000] = { 0 };

	int lol = recv(sock, (char*)data, size, 0);

	return lol;
}