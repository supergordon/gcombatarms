#include "SDK/Structs.h"

CESP *g_pESP = new CESP;

CESP::CESP()
{
	m_bSpawnProtected = false;
	m_fDistance = 0;
}

void CESP::Nade()
{	
	if(!cvars.Nades) return;

	for(int i = 0; i < g_pEngine->GetMaxIndex(GRENADE); i++)
	{
		CEntity* pEntity = g_pEngine->GetEntityByIndex(GRENADE, i);
		if(pEntity) {
			LTVector vPosition;
			g_pEngine->m_pLTClient->GetObjectPos(pEntity->object, &vPosition);

			int iCoords[2] = { 0 };

			ClientInfo* info = g_pEngine->GetClientByID(pEntity->ownerid);
			ClientInfo* linfo = g_pEngine->GetClientByID(g_pEngine->m_dwLocalIndex);

			if(g_pEngine->WorldToScreen(vPosition, iCoords[0], iCoords[1])) {

				if(info && linfo) {

					CColor team;
					if(info->team == linfo->team) 
						team = CColor(162, 200, 232);
					else
						team = CColor(150, 0, 0);

					if(cvars.Nades == 2)
						g_pTools->DrawFilledQuad(iCoords[0]-3, iCoords[1]-2, 4, 4, team);
					else if(cvars.Nades == 1) 
						g_pTools->DrawString(iCoords[0], iCoords[1], team, DT_CENTER, pEntity->name->name);
				}
				else {
					
					if(cvars.Nades == 2)
						g_pTools->DrawFilledQuad(iCoords[0]-3, iCoords[1]-2, 4, 4, CColor(138, 43, 226));
					else if(cvars.Nades == 1) 
						g_pTools->DrawString(iCoords[0], iCoords[1], CColor(138, 43, 226), DT_CENTER, pEntity->name->name);
				}
			}
		}
	}
}

void CESP::Pickup()
{	
	if(!cvars.Pickup) return;

	for(int i = 0; i < g_pEngine->GetMaxIndex(WEAPON); i++)
	{
		CEntity* pEntity = g_pEngine->GetEntityByIndex(WEAPON, i);
		if(pEntity) {
			LTVector vPosition;
			DWORD dwIndex = 0;

			if(cvars.Pickup) {
				g_pEngine->m_pLTClient->GetObjectPos(pEntity->object, &vPosition);

				int iCoords[2] = { 0 };
				if(g_pEngine->WorldToScreen(vPosition, iCoords[0], iCoords[1])) {
					g_pTools->DrawFilledQuad(iCoords[0]-2, iCoords[1]-2, 4, 4, yellow);
					//g_pTools->DrawString(iCoords[0], iCoords[1], yellow, DT_CENTER, pEntity->name?pEntity->name->name:"wpn");
				}
			}

			/*if(cvars.TelePickup) {

				LTVector pos;
				LTRotation ltRot;
				g_pEngine->m_pLTClient->GetObjectRotation(g_pEngine->m_pLTBase->GetClientObject(), &ltRot);

				LTVector poss = (g_pEngine->m_vLocalPosition + (ltRot.Forward()*60));

				g_pEngine->m_pLTClient->SetObjectPos(pEntity->object, poss, 0);
			}*/
		}
	}
}

struct CPlayer
{
	HOBJECT obj1;
	HOBJECT obj2;
	DWORD tick;

	DWORD dwTick;
}; CPlayer g_pPlayer[50] = {0};

bool CESP::IsVulnerable(CEntity* pEntity, ClientInfo* pInfo)
{
	if(pInfo->isdead) {
		g_pPlayer[pEntity->index].tick = GetTickCount();
	}

	g_pPlayer[pEntity->index].obj1 = pEntity->object;

	if(g_pPlayer[pEntity->index].obj1 != g_pPlayer[pEntity->index].obj2) {
		g_pPlayer[pEntity->index].tick = GetTickCount();
	}

	g_pPlayer[pEntity->index].obj2 = pEntity->object;

	if((g_pPlayer[pEntity->index].tick+3800) < GetTickCount()) {
		return false;
	}

	return true;
}

bool CESP::IsVulnerable(int i) 
{
	if((g_pPlayer[i].tick+3800) < GetTickCount()) {
		return true;
	}

	return false;
}

void CESP::Do(CEntity* pEntity, ClientInfo* pInfo, ClientInfo* pLocalInfo)
{
	if(!pEntity) return;
	
	HMODELNODE node;
	HMODELNODE child;
	LTTransform transform;

	g_pEngine->m_pLTModel->GetNode(pEntity->object, Torso, node);
	g_pEngine->m_pLTModel->GetNodeTransform(pEntity->object, node, transform, true);
	m_vTorsoPosition = transform.m_vPos;

	LTRESULT ltRes = g_pEngine->m_pLTModel->GetNode(pEntity->object, Head, node);
	if(ltRes != LT_OK) g_pEngine->m_pLTModel->GetNode(pEntity->object, Bip_Head, node);
	g_pEngine->m_pLTModel->GetNodeTransform(pEntity->object, node, transform, true);
	m_vHeadPosition = transform.m_vPos;

	g_pEngine->m_pLTModel->GetNextNode(pEntity->object, -1, node);
	g_pEngine->m_pLTModel->GetNodeTransform(pEntity->object, node, transform, true);
	m_vRootPosition = transform.m_vPos;

	if(!pInfo || !pLocalInfo || !pInfo->name[0]) return;
	if(pInfo->index == pLocalInfo->index) return;

	//bAlive = pLocalInfo->isdead == 0;
	g_pEngine->m_iKills = pLocalInfo->kills;
	g_pEngine->m_iDeaths = pLocalInfo->deaths;

	int visible = 0;

	if(IsVulnerable(pEntity, pInfo) && pLocalInfo->team != pInfo->team) {
		m_cColor = CColor(255, 120, 0);
	}
	else if(g_pEngine->IsVisible(g_pEngine->m_vLocalPosition, m_vHeadPosition, pEntity->object) && pLocalInfo->team != pInfo->team) {
		m_cColor = green;
		visible = true;
	}
	else {
		if(pLocalInfo->team == pInfo->team) {
			m_cColor = CColor(162, 200, 232);
		}
		else {
			m_cColor = CColor(150, 0, 0);
		}
	}

	if(pInfo->isdead) return;

	if(cvars.EnemyOnly) {
		if(pInfo->team == pLocalInfo->team) return;
	}

	m_fDistance = g_pTools->GetDistance(cvars.Telekill?g_pEngine->m_pLocal->pos:g_pEngine->m_vLocalPosition, m_vHeadPosition);

	//is aiming at you
	/*#define DotProduct(x,y)			(x.x*y.x + x.y*y.y + x.z*y.z)

	LTRotation ltRot;
	LTVector kamera(0,0,0);
	g_pEngine->m_pLTClient->GetObjectRotation(pEntity->object, &ltRot);
	g_pEngine->m_pLTClient->GetObjectPos(g_pEngine->m_pLTBase->GetClientObject(), &kamera);

	LTVector buf = m_vHeadPosition - kamera;

	LTVector right, up, look;
	ltRot.GetVectors(right, up, look);

	float viewSpace[3] = { 0 };

	viewSpace[0] = -(buf.x*right.x + buf.y*right.y + buf.z*right.z);
	viewSpace[1] = (buf.x*up.x + buf.y*up.y + buf.z*up.z);
	viewSpace[2] = (buf.x*look.x + buf.y*look.y + buf.z*look.z);

	float buffer = 320;

	if((1) && 
		(viewSpace[0] > -buffer) && (viewSpace[0] < buffer) && 
		(viewSpace[1] > -buffer) && (viewSpace[1] < buffer))
	{
		if(visible) {
			D3DVIEWPORT9 oViewport;
			pD3Ddev->GetViewport(&oViewport);

			g_pTools->DrawString2(oViewport.Width/2, 150, red, DT_CENTER, "%s is aiming at you", pInfo->name);
		}
	}*/

	int iCoords[2] = { 0 };

	m_vHeadPosition.y += 30;
	int abstand = 0;
	if(g_pEngine->WorldToScreen(m_vHeadPosition, iCoords[0], iCoords[1])) {

		if(cvars.Line) {
			if(cvars.Line == 1)
				g_pTools->DrawLine(cvars.w/2, cvars.h-50, iCoords[0], iCoords[1], m_cColor, 1);
			else 
				g_pTools->DrawLine(cvars.w/2, cvars.h/2, iCoords[0], iCoords[1], m_cColor, 1);
		}

		if(cvars.Name && !cvars.Distance) {
			g_pTools->DrawString(iCoords[0], iCoords[1], m_cColor, DT_CENTER, _s, pInfo->name);
			abstand += 14;
		}
		if(!cvars.Name && cvars.Distance) {
			g_pTools->DrawString(iCoords[0], iCoords[1], m_cColor, DT_CENTER, /*%.2f*/XorStr<0x18,5,0x20F34D18>("\x3D\x37\x28\x7D"+0x20F34D18).s, m_fDistance/35);
			abstand += 14;
		}
		if(cvars.Name && cvars.Distance) {
			g_pTools->DrawString(iCoords[0], iCoords[1], m_cColor, DT_CENTER, /*%s [%.0f]*/XorStr<0x84,10,0x29490C6B>("\xA1\xF6\xA6\xDC\xAD\xA7\xBA\xED\xD1"+0x29490C6B).s, pInfo->name, m_fDistance/35);
			abstand += 14;
		}
		if(cvars.Health) {
			//(float)pEntity->health
			if(cvars.Health == 1) {
				g_pTools->DrawString(iCoords[0], iCoords[1]+abstand, m_cColor, DT_CENTER, /*H: %d  A: %d*/XorStr<0x80,13,0x74DCAFB1>("\xC8\xBB\xA2\xA6\xE0\xA5\xA6\xC6\xB2\xA9\xAF\xEF"+0x74DCAFB1).s, pEntity->health, pEntity->armor);
			}
			else if(cvars.Health == 2) {
				int iMaxHealth = pEntity->health>100?350:100;
				if(iMaxHealth > 500) iMaxHealth = 1000;
				float percentage_width = (float)70 / (float)iMaxHealth;
				g_pTools->DrawFilledQuad(iCoords[0]-35, iCoords[1]+abstand, float((float)pEntity->health*(float)percentage_width), 5, m_cColor);
				g_pTools->DrawQuad(iCoords[0]-35, iCoords[1]+abstand, 70, 5, white, 1);

				float percentage_width_a = (float)70 / (float)100;
				g_pTools->DrawFilledQuad(iCoords[0]-35, iCoords[1]+abstand+7, float((float)pEntity->armor*(float)percentage_width_a), 5, m_cColor);
				g_pTools->DrawQuad(iCoords[0]-35, iCoords[1]+abstand+7, 70, 5, white, 1);
			}
		}
	}
	m_vHeadPosition.y -= 30;

	
	if(g_pEngine->WorldToScreen(m_vTorsoPosition, iCoords[0], iCoords[1])) {



		if(cvars.Box > 100) {
			int radius = cvars.Box / (m_fDistance/35);
			g_pTools->DrawQuad(iCoords[0]-(radius/2), iCoords[1]-(radius/2), radius, radius, m_cColor, 1);
		}
		else if(cvars.Box && cvars.Box <= 100 && bAlive) {

			float Speed = 20; //breite angegeben im worldspace

			LTVector vRootNode;
			g_pEngine->m_pLTClient->GetObjectPos(pEntity->object, &vRootNode);
			
			LTVector moveUpLeft = m_vRootPosition;
			LTVector moveUpRight = m_vRootPosition;
			LTVector moveDownLeft = m_vRootPosition;
			LTVector moveDownRight = m_vRootPosition;

			moveUpLeft.y = m_vHeadPosition.y+10;
			moveUpRight.y = m_vHeadPosition.y+10;

			moveUpLeft.x += sin(g_pEngine->m_pView->yaw-M_PI_2)*Speed;
			moveUpLeft.z += cos(g_pEngine->m_pView->yaw-M_PI_2)*Speed;

			moveUpRight.x += sin(g_pEngine->m_pView->yaw+M_PI_2)*Speed;
			moveUpRight.z += cos(g_pEngine->m_pView->yaw+M_PI_2)*Speed;

			moveDownLeft.x += sin(g_pEngine->m_pView->yaw-M_PI_2)*Speed;
			moveDownLeft.z += cos(g_pEngine->m_pView->yaw-M_PI_2)*Speed;

			moveDownRight.x += sin(g_pEngine->m_pView->yaw+M_PI_2)*Speed;
			moveDownRight.z += cos(g_pEngine->m_pView->yaw+M_PI_2)*Speed;

		
			int iDownLeft[2] = { 0 },
				iDownRight[2] = { 0 },
				iUpLeft[2] = { 0 },
				iUpRight[2] = { 0 };
			
			bool res1 = g_pEngine->WorldToScreen(moveUpLeft, iUpLeft[0], iUpLeft[1]);
			bool res2 = g_pEngine->WorldToScreen(moveUpRight, iUpRight[0], iUpRight[1]);
			bool res3 = g_pEngine->WorldToScreen(moveDownLeft, iDownLeft[0], iDownLeft[1]);
			bool res4 = g_pEngine->WorldToScreen(moveDownRight, iDownRight[0], iDownRight[1]);

			if(res1 && res2 && res3 && res4) {		
				g_pTools->DrawLine(iUpLeft[0], iUpLeft[1], iUpRight[0], iUpRight[1], m_cColor, 1);
				g_pTools->DrawLine(iDownLeft[0], iDownLeft[1], iDownRight[0], iDownRight[1], m_cColor, 1);
				g_pTools->DrawLine(iUpLeft[0], iUpLeft[1], iDownLeft[0], iDownLeft[1], m_cColor, 1);
				g_pTools->DrawLine(iUpRight[0], iUpRight[1], iDownRight[0], iDownRight[1], m_cColor, 1);
			}

			//int _x = iUpLeft[0];
			//int _y = iUpLeft[1];
			//int _w = iUpRight[0]-iUpLeft[0];
			//int _h = iDownRight[1]-iUpRight[1];

			//g_pTools->DrawQuad(_x, _y, _w, _h, m_cColor, 2);
		}
	}

	if(cvars.Radar) {
		LTRotation ltRot;

		g_pEngine->m_pLTClient->GetObjectRotation(pEntity->object, &ltRot);
		LTVector vPlayerVec = m_vTorsoPosition + (ltRot.Forward()*250);

		g_pTools->DrawRadarPoint(m_vTorsoPosition, g_pEngine->m_vLocalPosition, vPlayerVec, m_cColor);
	}
}

void CESP::Fireteam(CEntity* pEntity, ClientInfo* pInfo)
{
	if(!pEntity || pInfo || g_pEngine->m_dwGamemode != 5) return;


	m_fDistance = g_pTools->GetDistance(cvars.Telekill?g_pEngine->m_pLocal->pos:g_pEngine->m_vLocalPosition, m_vHeadPosition);


	int iCoords[2] = { 0 };
	
	if(g_pEngine->WorldToScreen(m_vTorsoPosition, iCoords[0], iCoords[1])) {
			int abstand = 0;

			if(cvars.Line) {
				if(cvars.Line == 1)
					g_pTools->DrawLine(cvars.w/2, cvars.h-50, iCoords[0], iCoords[1], yellow, 1);
				else 
					g_pTools->DrawLine(cvars.w/2, cvars.h/2, iCoords[0], iCoords[1], yellow, 1);
			}

			if(cvars.Name && !cvars.Distance) {
				g_pTools->DrawString(iCoords[0], iCoords[1], yellow, DT_CENTER, _s, /*NPC*/XorStr<0x32,4,0xA2B0CAC6>("\x7C\x63\x77"+0xA2B0CAC6).s);
				abstand += 14;
			}
			if(!cvars.Name && cvars.Distance) {
				g_pTools->DrawString(iCoords[0], iCoords[1], yellow, DT_CENTER, /*%.2f*/XorStr<0x0E,5,0xAFF461E0>("\x2B\x21\x22\x77"+0xAFF461E0).s, m_fDistance/35);
				abstand += 14;
			}
			if(cvars.Name && cvars.Distance) {
				g_pTools->DrawString(iCoords[0], iCoords[1], yellow, DT_CENTER, /*%s [%.0f]*/XorStr<0x72,10,0x490A30DA>("\x57\x00\x54\x2E\x53\x59\x48\x1F\x27"+0x490A30DA).s, /*NPC*/XorStr<0x32,4,0xA2B0CAC6>("\x7C\x63\x77"+0xA2B0CAC6).s, m_fDistance/35);
				abstand += 14;
			}
			//if(cvars.Health) {
			//	//(float)pEntity->health
			//	if(cvars.Health == 1) {
			//		g_pTools->DrawString(iCoords[0], iCoords[1]+abstand, m_cColor, DT_CENTER, "H: %d  A: %d", pEntity->health, pEntity->armor);
			//	}
			//	else if(cvars.Health == 2) {
			//		int iMaxHealth = pEntity->health>100?350:100;
			//		float percentage_width = (float)70 / (float)iMaxHealth;
			//		g_pTools->DrawFilledQuad(iCoords[0]-35, iCoords[1]+abstand, float((float)pEntity->health*(float)percentage_width), 5, yellow);
			//		g_pTools->DrawQuad(iCoords[0]-35, iCoords[1]+abstand, 70, 5, white, 1);

			//		float percentage_width_a = (float)70 / (float)100;
			//		g_pTools->DrawFilledQuad(iCoords[0]-35, iCoords[1]+abstand+7, float((float)pEntity->armor*(float)percentage_width_a), 5, yellow);
			//		g_pTools->DrawQuad(iCoords[0]-35, iCoords[1]+abstand+7, 70, 5, white, 1);
			//	}
			//}
			
			if(cvars.Box > 100) {
				int radius = cvars.Box / (m_fDistance/35);
				g_pTools->DrawQuad(iCoords[0]-(radius/2), iCoords[1]-(radius/2), radius, radius, yellow, 1);
			}
			else if(cvars.Box && cvars.Box <= 100 && bAlive) {

				float Speed = 20; //breite angegeben im worldspace

				LTVector vRootNode;
				g_pEngine->m_pLTClient->GetObjectPos(pEntity->object, &vRootNode);
				
				LTVector moveUpLeft = m_vRootPosition;
				LTVector moveUpRight = m_vRootPosition;
				LTVector moveDownLeft = m_vRootPosition;
				LTVector moveDownRight = m_vRootPosition;

				moveUpLeft.y = m_vHeadPosition.y+10;
				moveUpRight.y = m_vHeadPosition.y+10;

				moveUpLeft.x += sin(g_pEngine->m_pView->yaw-M_PI_2)*Speed;
				moveUpLeft.z += cos(g_pEngine->m_pView->yaw-M_PI_2)*Speed;

				moveUpRight.x += sin(g_pEngine->m_pView->yaw+M_PI_2)*Speed;
				moveUpRight.z += cos(g_pEngine->m_pView->yaw+M_PI_2)*Speed;

				moveDownLeft.x += sin(g_pEngine->m_pView->yaw-M_PI_2)*Speed;
				moveDownLeft.z += cos(g_pEngine->m_pView->yaw-M_PI_2)*Speed;

				moveDownRight.x += sin(g_pEngine->m_pView->yaw+M_PI_2)*Speed;
				moveDownRight.z += cos(g_pEngine->m_pView->yaw+M_PI_2)*Speed;

			
				int iDownLeft[2] = { 0 },
					iDownRight[2] = { 0 },
					iUpLeft[2] = { 0 },
					iUpRight[2] = { 0 };
				
				bool res1 = g_pEngine->WorldToScreen(moveUpLeft, iUpLeft[0], iUpLeft[1]);
				bool res2 = g_pEngine->WorldToScreen(moveUpRight, iUpRight[0], iUpRight[1]);
				bool res3 = g_pEngine->WorldToScreen(moveDownLeft, iDownLeft[0], iDownLeft[1]);
				bool res4 = g_pEngine->WorldToScreen(moveDownRight, iDownRight[0], iDownRight[1]);

				if(res1 && res2 && res3 && res4) {		
					g_pTools->DrawLine(iUpLeft[0], iUpLeft[1], iUpRight[0], iUpRight[1], yellow, 1);
					g_pTools->DrawLine(iDownLeft[0], iDownLeft[1], iDownRight[0], iDownRight[1], yellow, 1);
					g_pTools->DrawLine(iUpLeft[0], iUpLeft[1], iDownLeft[0], iDownLeft[1], yellow, 1);
					g_pTools->DrawLine(iUpRight[0], iUpRight[1], iDownRight[0], iDownRight[1], yellow, 1);
				}
			}

			if(cvars.Radar) {
				LTRotation ltRot;

				g_pEngine->m_pLTClient->GetObjectRotation(pEntity->object, &ltRot);
				LTVector vPlayerVec = m_vTorsoPosition + (ltRot.Forward()*250);

				g_pTools->DrawRadarPoint(m_vTorsoPosition, g_pEngine->m_vLocalPosition, vPlayerVec, yellow);
			}
		}
}