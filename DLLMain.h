#ifndef _DLLMAIN_H_
#define _DLLMAIN_H_

#pragma warning(disable: 4311 4312 4267 4101 4244 4518 4228 4405 4305 4533)

struct CCVARS
{
	int Wallhack;
	int Chams;
	int Fullbright;
	int Nofog;
	int Crosshair;
	int OPK;
	int Norecoil;
	int Nospread;
	int Box;
	int Speedhack;
	int radarx;
	int radary;
	int Nosky;
	int Nohands;
	int Noflash;
	int Noscope;
	int Godmode;
	int NoCDam;
	int Windowed;
	int Aimkey;
	int Aimthru;
	int Autoshoot;
	//int Aimfov;
	int Glitcher;
	int Knifebot;
	int Telekill;
	int Ghostmode;
	int Radar;
	int w, h;
	int Friends;
	int Aimtoggle;
	int Wireframe;
	int Asus;
int Test;
	int Aimbot;
	int Aimpoint;
	int ESP;
	int DirectX;
	int Misc;
	int Name;
	int Distance;
	int Pickup;
	int Nades;
	int Health;
	int EnemyOnly;
	int Antikick;
	int Hover;
	int UnlRespawn;
	int Bone;
	int NoRespawnTime;
	int SatChams;
	int Statsbox;
	int WeaponSway;
	int Noreload;
	int PickupCmd;
	int TelePickup;

	int Commands;
	int Boxes;
	int Tracers;
	int SJump;
	int ShowFps;
	int Respawn;
	int Act;
	int Nosway;
	int Gravity;
	int NoGravity;
	int SelfKill;
	int Ammo;
	int Wrireframe;
	int Breath;
	int Aimsmooth;
	int Aimmode;
	int Aimfov;
	int Aimlock;
	int SuperBullets;
	int Settings;
	int SettingsLoad;
	int SettingsSave;
	int Line;
	int WeaponRange;
	int Warnings;
	int Clock;
	int Font;

	int Detectable;
}; extern CCVARS cvars;

extern DWORD WINAPI InitializeGameHook(LPVOID);

extern void WriteConfigFile();
extern void ReadConfigFile();

#endif