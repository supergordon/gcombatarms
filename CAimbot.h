#ifndef _AIMBOT_
#define _AIMBOT_

class CAimbot
{
public:
	CAimbot();
	void Reset();
	void Collect(CEntity* pEntity, ClientInfo* pInfo, ClientInfo* pLocalInfo, int i);
	void CollectFireteam(CEntity* pEntity, ClientInfo* pInfo, ClientInfo* pLocalInfo, int i);
	void Do();
	int GetNearestPlayer();
	bool m_bLocked;
private:
	float m_fDistance;
	float m_fBufDistance;
	int m_iNearestIndex;
	LTVector m_vTargetHead;
};

extern CAimbot *g_pAimbot;

#endif