#ifndef _STRUCTS_H_
#define _STRUCTS_H_

struct CEntity;
struct ClientInfo;
struct CLocal;
struct CViewangles;
struct IntersectInfo2;
struct CEntityInfo;
class CGameClientShell_t;
class CLTBase;
class CLTModel;
class CLTClient;
class CLTPhysicClient;
class CLTCommon;
class CLTCursor;
class CLTDrawPrim;
class CLTServer;
class CPlayerManager;
struct CViewangles;
struct CViewangles;
struct CCamera1;
struct CCamera2;
struct dummy;

#define PROJECT_FEAR
#define _FINAL
#define INCLUDE_AS_ENUM
#define _WIN32_WINNT 0x0501
//#define _WIN32_WINNT 0x0501

//#include <winsock.h>
#include <windows.h>
//#include <vector>
#include <stdio.h>
#include <fstream>
#include <tlhelp32.h>
#include <time.h>

#include "SDK\\D3D\\d3d9.h"
#include "SDK\\D3D\\d3dx9.h"


#include "CSocks.h"
#include "DLLMain.h"
#include "D3D.h"
#include "CTools.h"
#include "CMenu.h"
#include "CHackshield.h"
#include "CESP.h"
#include "CColor.h"
#include "CAimbot.h"
#include "CAddress.h"
#include "CEngine.h"
#include "xorstr.h"
//#include "d3dfont.h"


#pragma comment( lib, "SDK\\D3D\\d3d9.lib" )
#pragma comment( lib, "SDK\\D3D\\d3dx9.lib" )


#include "sdk/game/shared/debugnew.h"
#include "sdk/engine/sdk/inc/iltmessage.h"
#include "sdk/game/clientshelldll/gameclientshell.h"


using namespace std;

typedef unsigned int        uint;
typedef char				int8;
typedef unsigned char		uint8;
typedef short int			int16;
typedef unsigned short int	uint16;
typedef int					int32;
typedef unsigned int		uint32;


#define Head /*Head*/XorStr<0x1A,5,0xFAA6EABD>("\x52\x7E\x7D\x79"+0xFAA6EABD).s
#define Bip_Head /*Bip01 Head*/XorStr<0x2F,11,0xD397E4BB>("\x6D\x59\x41\x02\x02\x14\x7D\x53\x56\x5C"+0xD397E4BB).s
#define Torso /*Torso*/XorStr<0x1D,6,0xE48C89C9>("\x49\x71\x6D\x53\x4E"+0xE48C89C9).s
#define Bip_Torso /*Bip01 Torso*/XorStr<0x1E,12,0xEFE8DA22>("\x5C\x76\x50\x11\x13\x03\x70\x4A\x54\x54\x47"+0xEFE8DA22).s

#define cshell /*cshell.dll*/XorStr<0x0B,11,0xEAA35944>("\x68\x7F\x65\x6B\x63\x7C\x3F\x76\x7F\x78"+0xEAA35944).s
#define clientfx /*ClientFX.fxd*/XorStr<0x19,13,0x778A8C74>("\x5A\x76\x72\x79\x73\x6A\x59\x78\x0F\x44\x5B\x40"+0x778A8C74).s
#define ehsvc /*EHSvc.dll*/XorStr<0x29,10,0xDC82629F>("\x6C\x62\x78\x5A\x4E\x00\x4B\x5C\x5D"+0xDC82629F).s
#define d3d9dll /*d3d9.dll*/XorStr<0xA7,9,0x116078BA>("\xC3\x9B\xCD\x93\x85\xC8\xC1\xC2"+0x116078BA).s
#define d3dx9 /*d3dx9_35.dll*/XorStr<0xEF,13,0x46FB4E32>("\x8B\xC3\x95\x8A\xCA\xAB\xC6\xC3\xD9\x9C\x95\x96"+0x46FB4E32).s

#define _vec3project /*D3DXVec3Project*/XorStr<0x70,16,0x5024FB6E>("\x34\x42\x36\x2B\x22\x10\x15\x44\x28\x0B\x15\x11\x19\x1E\x0A"+0x5024FB6E).s
#define _createline /*D3DXCreateLine*/XorStr<0xDD,15,0x4EB79F48>("\x99\xED\x9B\xB8\xA2\x90\x86\x85\x91\x83\xAB\x81\x87\x8F"+0x4EB79F48).s
#define _createfont /*D3DXCreateFontA*/XorStr<0xA5,16,0x28E45D31>("\xE1\x95\xE3\xF0\xEA\xD8\xCE\xCD\xD9\xCB\xE9\xDF\xDF\xC6\xF2"+0x28E45D31).s

#define hackname "Gordon CA 1.5.5"

#define _s /*%s*/XorStr<0xE6,3,0x14758C79>("\xC3\x94"+0x14758C79).s
#define _d /*%d*/XorStr<0xAC,3,0xFC1F7B9D>("\x89\xC9"+0xFC1F7B9D).s
#define _sf /*%s %f*/XorStr<0x55,6,0x6EA18046>("\x70\x25\x77\x7D\x3F"+0x6EA18046).s

#define _open /*open*/XorStr<0xCB,5,0x2247DA33>("\xA4\xBC\xA8\xA0"+0x2247DA33).s
#define website /*http://gordonsys.net*/XorStr<0x43,21,0x6F8D2EB4>("\x2B\x30\x31\x36\x7D\x67\x66\x2D\x24\x3E\x29\x21\x21\x23\x28\x21\x7D\x3A\x30\x22"+0x6F8D2EB4).s

#define fontname /*Courier New*/XorStr<0xEA,12,0xE2F7EDDA>("\xA9\x84\x99\x9F\x87\x8A\x82\xD1\xBC\x96\x83"+0xE2F7EDDA).s

#define CRYPT_START \
	__asm jmp abc \
	__asm _emit 0x90 \
	__asm _emit 0x91 \
	__asm abc:

#define CRYPT_END \
	__asm jmp abcd \
	__asm _emit 0x90 \
	__asm _emit 0x92 \
	__asm abcd:

extern int bGhostmode;

class CLTPhysicClient
{
public:
	/*0x00*/	char* (*GetObjectType)();
	/*0x04*/	LTRESULT (__stdcall* IsWorldObject)(HOBJECT hObj);
	/*0x08*/	LTRESULT (__stdcall* GetStairHeight)(float &fHeight);
	/*0x0C*/	LTRESULT (__stdcall* SetStairHeight)(float fHeight);
	/*0x10*/	LTRESULT (__stdcall* GetMass)(HOBJECT hObj, float *m);
	/*0x14*/	LTRESULT (__stdcall* SetMass)(HOBJECT hObj, float m);
	/*0x18*/	LTRESULT (__stdcall* GetFrictionCoefficient)(HOBJECT hObj, float* u);
	/*0x1C*/	LTRESULT (__stdcall* SetFrictionCoefficient)(HOBJECT hObj, float u); 
	/*0x20*/	LTRESULT (__stdcall* GetObjectDims)(HOBJECT hObj, LTVector *d);
	/*0x24*/	LTRESULT (__stdcall* SetObjectDims)(HOBJECT hObj, LTVector *d, uint32 flag);
	/*0x28*/	LTRESULT (__stdcall* GetVelocity)(HOBJECT hObj, LTVector *v);
	/*0x2C*/	LTRESULT (__stdcall* SetVelocity)(HOBJECT hObj, const LTVector& v);
	/*0x30*/	LTRESULT (__stdcall* GetForceIgnoreLimit)(HOBJECT hObj, float &f);
	/*0x34*/	LTRESULT (__stdcall* SetForceIgnoreLimit)(HOBJECT hObj, float f);
	/*0x38*/	LTRESULT (__stdcall* GetAcceleration)(HOBJECT hObj, LTVector *a);
	/*0x3C*/	LTRESULT (__stdcall* SetAcceleration)(HOBJECT hObj, const LTVector& a);
	/*0x40*/	LTRESULT (__stdcall* MoveObject)(HOBJECT hObj, const LTVector& p, uint32 flag);
	/*0x44*/	LTRESULT (__stdcall* GetStandingOn)(HOBJECT hObj, CollisionInfo *info);
	/*0x48*/	LTRESULT (__stdcall* GetGlobalForce)(LTVector &a);
	/*0x4C*/	LTRESULT (__stdcall* SetGlobalForce)(const LTVector &a); 
	/*0x50*/	LTRESULT (__stdcall* UpdateMovement)(MoveInfo* pInfo);
	/*0x54*/	LTRESULT (__stdcall* MovePushObjects)(HOBJECT hToMove,const LTVector& p,HOBJECT* hPushObjects,uint32 nPushObjects);
	/*0x58*/	LTRESULT (__stdcall* RotatePushObjects)(HOBJECT hToMove,const LTRotation& q,HOBJECT* hPushObjects,uint32 nPushObjects);
};

//class CLTDrawPrim
//{
//public:
//	/*0x00*/	char* (*GetObjectType)();
//	/*0x04*/	LTRESULT (__stdcall *BeginDrawPrimBlock)();
//	/*0x08*/	LTRESULT (__stdcall *EndDrawPrimBlock)();
//
//	/*
//	0x20 => devicepointer
//	*/
//};

class CLTDrawPrim
{
public:
	/*0x00*/	char* (*GetObjectType)();
	/*0x04*/	LTRESULT (__stdcall *BeginDrawPrimBlock)();
	/*0x08*/	LTRESULT (__stdcall *EndDrawPrimBlock)();
	/*0x0C*/	void (*function3)(); 
	/*0x10*/	void (*function4)(); 
	/*0x14*/	void (*function5)(); 
	/*0x18*/	void (*function6)(); 
	/*0x1C*/	void (*function7)(); 
	/*0x20*/	void (*function8)(); 
	/*0x24*/	void (*function9)(); 
	/*0x28*/	void (*SetRenderMode)(int mode);
	/*0x2C*/	void (*function11)(); 
	/*0x30*/	void (*function12)();  
	DWORD ID03A2DAD0; //0x0034  
	DWORD ID03A21438; //0x0038  
	DWORD ID03A214B8; //0x003C  
	DWORD ID03A21538; //0x0040  
	DWORD ID03A215B8; //0x0044  
	DWORD ID03A21638; //0x0048  
	DWORD ID03A216B8; //0x004C  
	DWORD ID03A21738; //0x0050  
	DWORD ID03A217B8; //0x0054  
	DWORD ID03A21838; //0x0058  
	DWORD ID03A218B8; //0x005C  
	DWORD ID03A21938; //0x0060  
	DWORD ID03A219B8; //0x0064  
	DWORD ID03A21A38; //0x0068  
	DWORD ID03A21AB8; //0x006C  
	DWORD ID03A21B38; //0x0070  
	DWORD ID03A21BB8; //0x0074  
	DWORD ID03A21C38; //0x0078  30
	DWORD ID03A21CB8; //0x007C  
	//DWORD ID03A21D38; //0x0080  
	void (*DrawPrimitive) (void* q, unsigned int count);
	DWORD ID03A21DB8; //0x0084  
	DWORD ID03A21E38; //0x0088  
	DWORD ID03A21EB8; //0x008C  
	DWORD ID03A21F38; //0x0090  
	DWORD ID03A21FB8; //0x0094  
	DWORD ID03A22038; //0x0098  
	DWORD ID03A220B8; //0x009C  
	DWORD ID03A22138; //0x00A0 40 
	DWORD ID03A221B8; //0x00A4  
	DWORD ID03A22238; //0x00A8  
	DWORD ID03A222B8; //0x00AC  
	DWORD ID03A22338; //0x00B0  
	DWORD ID03A223B8; //0x00B4  
	DWORD ID03A22438; //0x00B8  
	DWORD ID03A224B8; //0x00BC  
	DWORD ID03A22538; //0x00C0  
	DWORD ID03A225B8; //0x00C4  
	//DWORD ID03A22638; //0x00C8 50 
	HRESULT (*Project)(float x, float y, float z, LTVector* pProjectOut, LTVector* pTransformOut);
	DWORD ID03A226B8; //0x00CC  
	DWORD ID03A22738; //0x00D0  
	DWORD ID03A227B8; //0x00D4  
	DWORD ID03A22838; //0x00D8  
	DWORD ID03A228B8; //0x00DC  
		char unknown224[17012]; //0x00E0
	D3DXMATRIX World; //0x4354  
	D3DXMATRIX View; //0x4394  
	D3DXMATRIX Projection; //0x43D4  
};//Size=0x4414(17428)

class CLTCursor
{ 
public:
	/*0x00*/	char* (*GetObjectType)();
	/*0x04*/	LTRESULT (__stdcall *SetCursorMode)(CursorMode cMode, bool bForce);
	/*0x08*/	LTRESULT (__stdcall *GetCursorMode)(CursorMode &cMode);
	/*0x0C*/	LTRESULT (__stdcall *IsCursorModeAvailable)(CursorMode cMode);
	/*0x10*/	LTRESULT (__stdcall *LoadCursorFromFile)(const char *pszFilename, HLTCURSOR &hCursor);
	/*0x14*/	LTRESULT (__stdcall *FreeCursor)(const HLTCURSOR hCursor);
	/*0x18*/	LTRESULT (__stdcall *SetCursor)(HLTCURSOR hCursor);
	/*0x1C*/	LTRESULT (__stdcall *RefreshCursor)();
};

class CLTServer
{
	public:
		/*0x00*/	void (*function0)(); 
		/*0x04*/	void (*function1)(); 
		/*0x08*/	void (*function2)(); 
		/*0x0C*/	void (*function3)(); 
		/*0x10*/	void (*function4)(); 
		/*0x14*/	void (*function5)(); 
		/*0x18*/	void (*function6)(); 
		/*0x1C*/	void (*function7)(); 
		/*0x20*/	void (*function8)(); 
		/*0x24*/	void (*function9)(); 
		/*0x28*/	void (*function10)(); 
		/*0x2C*/	void (*function11)(); 
		/*0x30*/	void (*function12)(); 
		/*0x34*/	void (*function13)(); 
		/*0x38*/	void (*function14)(); 
		/*0x3C*/	void (*function15)(); 
		/*0x40*/	void (*function16)(); 
		/*0x44*/	void (*function17)(); 
		/*0x48*/	void (*function18)(); 
		/*0x4C*/	void (*function19)(); 
		/*0x50*/	void (*function20)(); 
		/*0x54*/	void (*function21)(); 
		/*0x58*/	void (*function22)(); 
		/*0x5C*/	void (*function23)(); 
		/*0x60*/	void (*function24)(); 
		/*0x64*/	void (*function25)(); 
		/*0x68*/	void (*function26)(); 
		/*0x6C*/	void (*function27)(); 
		/*0x70*/	void (*function28)(); 
		/*0x74*/	void (*function29)(); 
		/*0x78*/	void (*function30)(); 
		/*0x7C*/	void (*function31)(); 
		/*0x80*/	void (*function32)(); 
		/*0x84*/	void (*function33)(); 
		/*0x88*/	void (*function34)(); 
		/*0x8C*/	void (*function35)(); 
		/*0x90*/	void (*function36)(); 
		/*0x94*/	void (*function37)(); 
		/*0x98*/	void (*function38)(); 
		/*0x9C*/	void (*function39)(); 
		/*0xA0*/	void (*function40)(); 
		/*0xA4*/	void (*function41)(); 
		/*0xA8*/	void (*function42)(); 
		/*0xAC*/	void (*function43)(); 
		/*0xB0*/	void (*function44)(); 
		/*0xB4*/	void (*function45)(); 
		/*0xB8*/	void (*function46)(); 
		/*0xBC*/	void (*function47)(); 
		/*0xC0*/	void (*function48)(); 
		/*0xC4*/	void (*function49)(); 
		/*0xC8*/	void (*function50)(); 
		/*0xCC*/	void (*function51)(); 
		/*0xD0*/	void (*function52)(); 
		/*0xD4*/	void (*function53)(); 
		/*0xD8*/	void (*function54)(); 
		/*0xDC*/	void (*function55)(); 
		/*0xE0*/	void (*function56)(); 
		/*0xE4*/	void (*function57)(); 
		/*0xE8*/	void (*function58)(); 
		/*0xEC*/	void (*function59)(); 
		/*0xF0*/	void (*function60)(); 
		/*0xF4*/	void (*function61)(); 
		/*0xF8*/	void (*function62)(); 
		/*0xFC*/	void (*function63)();
		/*0x100*/	void (*function64)(); 
		/*0x104*/	LTRESULT (__stdcall *GetObjectName)(HOBJECT hObject, char *pName, uint32 nameBufSize); 
		/*0x108*/	void (*function66)(); 
		/*0x10C*/	void (*function67)(); 
};	

class CGameClientShell_t
{
	public:
		/*0x00*/	char* (*GetObjectType)();
		/*0x04*/	void (*function1)(); 
		/*0x08*/	void (*function2)(); 
		/*0x0C*/	void (*function3)(); 
		/*0x10*/	void (*function4)(); 
		/*0x14*/	void (*function5)(); 
		/*0x18*/	void (*function6)(); 
		/*0x1C*/	void (*function7)(); 
		/*0x20*/	void (*function8)(); 
		/*0x24*/	void (*function9)(); 
		/*0x28*/	void (*function10)(); 
		/*0x2C*/	void (*function11)(); 
		/*0x30*/	void (*function12)(); 
		/*0x34*/	void (*function13)(); 
		/*0x38*/	void (*function14)(); 
		/*0x3C*/	void (*function15)(); 
		/*0x40*/	void (*function16)(); 
		/*0x44*/	void (*function17)(); 
		/*0x48*/	void (*function18)(); 
		/*0x4C*/	void (*function19)(); 
		/*0x50*/	void (*function20)(); 
		/*0x54*/	void (*function21)(); 
		/*0x58*/	void (*function22)(); 
		/*0x5C*/	void (*function23)(); 
		/*0x60*/	void (*function24)(); 
		/*0x64*/	void (*function25)(); 
		/*0x68*/	void (*function26)(); 
		/*0x6C*/	void (*function27)(); 
		/*0x70*/	void (*function28)(); 
		/*0x74*/	void (*function29)(); 
		/*0x78*/	void (*function30)(); 
		/*0x7C*/	void (*function31)(); 
		/*0x80*/	void (*function32)(); 
		/*0x84*/	void (*function33)(); 
		/*0x88*/	void (*function34)();
		/*0x8C*/	void (*function35)();  
		/*0x90*/	void (*function144)(); 
		/*0x94*/	void (*function145)(); 
		/*0x98*/	void (*function146)(); 
		/*0x9C*/	void (*function147)();
		/*0xA0*/	void (*function36)(); 
		/*0xA4*/	void (__stdcall *PauseGame)(bool bPause, bool bPauseSound);
		/*0xA8*/	void (*function38)(); 
		/*0xAC*/	void (*function300)();

		/*0xAC*/	void (*function301)(); 
		/*0xAC*/	void (*function302)(); 

		/*0xB0*/	dummy* (*GetPlayerInfo)();
		/*0xB4*/	void (*function40)();  //interfacemgr
		/*0xB8*/	CPlayerManager* (*GetPlayerManager)();  //playermgr
		/*0xBC*/	void (*function42)(); 
		/*0xC0*/	void (*function43)(); 
		/*0xC4*/	void (*function44)(); 
		/*0xC8*/	void (*function45)(); 
		/*0xCC*/	void (*function46)(); 
		/*0xD0*/	void (*function47)(); 
		/*0xD4*/	void (*function148)(); 
		/*0xD8*/	void (*function149)(); 
		/*0xDC*/	CEntityInfo* (*GetSFXMgr)(); 
		/*0xE4*/	void (*function51)();
		/*0xE8*/	void (*function52)(); 
		/*0xEC*/	void (*function53)(); 
		/*0xF0*/	void (*function54)(); 
		/*0xF4*/	void (*function55)(); 
		/*0xF8*/	void (*function56)(); 
		/*0xFC*/	void (*function57)(); 
		/*0xFC*/	void (*function58)(); 
		/*0x100*/	void (*function59)(); 
		/*0x104*/	void (*function60)(); 
		/*0x108*/	void (*function61)(); 
		/*0x10C*/	void (*function62)(); 
		/*0x110*/	void (*function63)(); 
		/*0x114*/	void (*function64)(); 
		/*0x118*/	void (*function65)(); 
		/*0x11C*/	void (*function66)(); 
		/*0x120*/	void (*function67)(); 
		/*0x124*/	void (*function68)(); 
		/*0x128*/	void (*function69)(); 
		/*0x12C*/	void (*function70)(); 
		/*0x130*/	void (*function71)(); 
};

class CLTModel
{
public:
	/*0x00*/	char* (*GetObjectType)();
	/*0x04*/	void (*function1)(); 
	/*0x08*/	void (*function2)(); 
	/*0x0C*/	void (*function3)(); 
	/*0x10*/	virtual void *function4(); 
	/*0x14*/	void (*function5)(); 
	/*0x18*/	LTRESULT (__stdcall *GetSocket)(HOBJECT hObj, const char *pSocketName, HMODELSOCKET &hSocket); 
	/*0x1C*/	LTRESULT (__stdcall *GetSocketTransform)(HOBJECT hObj, HMODELSOCKET hSocket, LTTransform &transform, bool bWorldSpace); 
	/*0x20*/	LTRESULT (__stdcall *GetPiece)(int, int); 
	/*0x24*/	LTRESULT (__stdcall *GetPiece2)(int, int, int);  
	/*0x28*/	LTRESULT (__stdcall *GetPieceHideStatus)(HOBJECT hObj, HMODELPIECE hPiece, bool &bHidden); 
	/*0x2C*/	LTRESULT (__stdcall *SetPieceHideStatus)(HOBJECT hObj, HMODELPIECE hPiece, bool bHidden);
	/*0x30*/	LTRESULT (__stdcall *GetNode2)(HOBJECT hObj, const char *pNodeName, HMODELNODE &hNode);
	/*0x34*/	LTRESULT (__stdcall *GetNode)(HOBJECT hObj, const char *pNodeName, HMODELNODE &hNode);
	/*0x38*/	LTRESULT (__stdcall *GetNodeName)(HOBJECT hObj, HMODELNODE hNode, char *name, uint32 maxlen );
	/*0x3C*/	LTRESULT (__stdcall *GetNodeTransform)(HOBJECT hObj, HMODELNODE hNode, LTTransform &transform, bool bWorldSpace);
	/*0x40*/	LTRESULT (__stdcall *GetNextNode)(HOBJECT hObject, HMODELNODE hNode, HMODELNODE &pNext);
	/*0x44*/	LTRESULT (__stdcall *GetRootNode)(HOBJECT hObj, HMODELNODE &hNode);
	/*0x48*/	LTRESULT (__stdcall *GetNumChildren)(HOBJECT hObj, HMODELNODE hNode, uint32 &NumChildren); 
	/*0x4C*/	LTRESULT (__stdcall *GetChild)(HOBJECT hObj, HMODELNODE parent, uint32 index, HMODELNODE &child); 
	/*0x50*/	LTRESULT (__stdcall *GetParent)(HOBJECT hObj, HMODELNODE node, HMODELNODE &parent); 
	/*0x54*/	LTRESULT (__stdcall *GetNumNodes)(HOBJECT hObj, uint32 &num_nodes); 
	/*0x58*/	LTRESULT (__stdcall *GetNumNodes2)(HOBJECT hObj, uint32 &num_nodes, int a3); 
	/*0x5C*/	LTRESULT (__stdcall *SetNodeControlFn1)(int,int,int,int); //4 params, stdcall 
	/*0x60*/	LTRESULT (__stdcall *SetNodeControlFn2)(int,int,int,int); //4 params, stdcall 
	/*0x64*/	LTRESULT (__stdcall *SetNodeControlFn3)(int,int,int); //3 params, stdcall 
	/*0x68*/	LTRESULT (__stdcall *SetNodeControlFn4)(int,int,int,int); //4 params, stdcall 
	/*0x6C*/	LTRESULT (__stdcall *UpdateMainTracker)(HOBJECT hObj, float fUpdateDelta); 
	/*0x70*/	LTRESULT (__stdcall *GetCurAnimLength)(HOBJECT hModel, ANIMTRACKERID TrackerID, uint32 &length); 
	/*0x74*/	LTRESULT (__stdcall *GetWeightSet)(HOBJECT hModel, ANIMTRACKERID TrackerID, HMODELWEIGHTSET &hSet); 
	/*0x78*/	LTRESULT (__stdcall *GetPlaybackState)(int, int, int);
	/*0x7C*/	LTRESULT (__stdcall *GetPlaybackState2)(int, int, int);
	/*0x80*/	void (*function32)();
	/*0x84*/	void (*function33)();
	/*0x88*/	LTRESULT (__stdcall *AddTracker)(HOBJECT hModel, ANIMTRACKERID TrackerID, bool bNetwork);
	/*0x8C*/	LTRESULT (__stdcall *RemoveTracker)(HOBJECT hModel, ANIMTRACKERID TrackerID);
	/*0x90*/	LTRESULT (__stdcall *GetAnimIndex)(int, int, int);
	/*0x94*/	LTRESULT (__stdcall *GetCurAnim)(int, int, int);
	/*0x98*/	LTRESULT (__stdcall *SetCurAnim)(HOBJECT hModel, ANIMTRACKERID TrackerID, HMODELANIM hAnim, bool bInterpolate);
	/*0x9C*/	LTRESULT (__stdcall *ResetAnim)(int, int);
	/*0xA0*/	LTRESULT (__stdcall *GetLooping)(int, int);
	/*0xA4*/	LTRESULT (__stdcall *SetLooping)(int, int, int);
	/*0xA8*/	LTRESULT (__stdcall *GetPlaying)(int, int);
	/*0xAC*/	LTRESULT (__stdcall *SetPlaying)(int, int, int);
	/*0xB0*/	LTRESULT (__stdcall *GetCurAnimLength2)(HOBJECT hModel, ANIMTRACKERID TrackerID, uint32 &length); 
	/*0xB4*/	LTRESULT (__stdcall *GetCurAnimTime)(int, int, int);
	/*0xB8*/	LTRESULT (__stdcall *SetCurAnimTime)(int, int, int);
};


//F8 => GetRenderStyle
//FC => SetRenderStyle 0052EE40   68 F85E6700      PUSH Engine.00675EF8                     ; ASCII "CLTModelClient::SetRenderStyle"

class CLTCommon
{
public:
	/*0x00*/	void (*function1)(); 
	/*0x04*/	LTRESULT (__stdcall* GetObjectType)(HOBJECT hObj, uint32 *type);
	/*0x08*/	void (*function3)(); 
	/*0x0C*/	void (*function4)(); 
	/*0x10*/	void (*function5)(); 
	/*0x14*/	void (*function6)(); 
	/*0x18*/	void (*function7)();  
	/*0x1C*/	void (*function8)(); 
	/*0x20*/	void (*function9)(); 
	/*0x24*/	void (*function10)(); 
	/*0x28*/	void (*function11)(); 
	/*0x2C*/	void (*function12)(); 
	/*0x30*/	void (*function13)(); 
	/*0x34*/	void (*function14)(); 
	/*0x38*/	void (*function15)();
	/*0x3C*/	void (*function16)();
	/*0x40*/	void (*function17)();
	/*0x44*/	void (*function18)(); 
	/*0x48*/	void (*function19)();  
	/*0x4C*/	void (*function20)(); 
	/*0x50*/	void (*function21)(); 
	/*0x54*/	void (*function22)();
	/*0x58*/	void (*function23)();
	/*0x5C*/	void (*function24)(); 
	/*0x60*/	void (*function25)(); 
	/*0x64*/	void (*function26)(); 
	/*0x68*/	void (*function27)(); 
	/*0x6C*/	void (*function28)();
	/*0x70*/	void (*function29)();   
	/*0x74*/	void (*function30)(); 
	/*0x78*/	void (*function31)();
	/*0x7C*/	void (*function32)(); 
	/*0x80*/	void (*function33)(); 
	/*0x84*/	void (*function34)(); 
	/*0x88*/	void (*function35)(); 
	/*0x8C*/	void (*function36)(); 
	/*0x90*/	void (*function37)(); 
	/*0x94*/	void (*function38)(); 
	/*0x98*/	void (*function39)(); 
	/*0x9C*/	void (*function40)(); 
	/*0xA0*/	void (*function41)(); 
	/*0xA4*/	void (*function42)(); 
	/*0xA8*/	void (*function43)(); 
	/*0xAC*/	void (*function44)(); 
	/*0xB0*/	void (*function45)(); 
	/*0xB4*/	void (*function46)(); 
	/*0xB8*/	void (*function47)(); 
	/*0xBC*/	void (*function48)(); 
	/*0xC0*/	void (*function49)(); 
	/*0xC4*/	void (*function50)(); 
	/*0xC8*/	void (*function51)(); 
	/*0xCC*/	void (*function52)(); 
	/*0xD0*/	void (*function53)(); 
	/*0xD4*/	void (*function54)(); 
	/*0xD8*/	void (*function55)(); 
	/*0xDC*/	void (*function56)(); 
	/*0xE0*/	void (*function57)(); 
	/*0xE4*/	void (*function58)(); 
	/*0xE8*/	void (*function59)(); 
	/*0xEC*/	void (*function60)(); 
	/*0xF0*/	void (*function61)(); 
	/*0xF4*/	void (*function62)(); 
	/*0xF8*/	void (*function63)(); 
	/*0xFC*/	void (*function64)(); 
	/*0x100*/	void (*function65)(); 
	/*0x104*/	void (*function66)();
	/*0x108*/	void (*function67)(); 
	/*0x10C*/	void (*function68)(); 
	/*0x110*/	void (*function69)(); 
	/*0x114*/	void (*function70)(); 
	/*0x118*/	void (*function71)(); 
	/*0x11C*/	void (*function72)(); 
	/*0x120*/	void (*function73)(); 
	/*0x124*/	void (*function74)(); 
	/*0x128*/	void (*function75)(); 
	/*0x12C*/	void (*function76)(); 
	/*0x130*/	void (*function77)(); 
	/*0x134*/	void (*function78)(); 
	/*0x138*/	void (*function79)(); 
	/*0x13C*/	void (*function80)(); 
	/*0x140*/	void (*function81)(); 
	/*0x144*/	void (*function82)(); 
	/*0x148*/	void (*function83)(); 
	/*0x14C*/	void (*function84)(); 
	/*0x150*/	/*void (*function85)(); */ LTRESULT (__stdcall* CreateMessage)(ILTMessage_Write* pMsg);
	/*0x154*/	void (*function86)(); 
	/*0x158*/	void (*function87)(); 
	/*0x15C*/	void (*function88)(); 
	/*0x160*/	void (*function89)(); 
	/*0x164*/	void (*function90)(); 
	/*0x168*/	void (*function91)(); 
	/*0x16C*/	void (*function92)(); 
	/*0x170*/	void (*function93)(); 
	/*0x174*/	LTRESULT (__stdcall *GetObjectFlags)(HOBJECT hObj, ObjFlagType flagType, uint32 &dwFlags);
	/*0x178*/	LTRESULT (__stdcall *SetObjectFlags)(HOBJECT hObj, ObjFlagType flagType, uint32 dwFlags, uint32 dwMask); 
	/*0x17C*/	void (*function96)(); 
	/*0x180*/	void (*function97)(); 
	/*0x184*/	void (*function98)(); 
	/*0x188*/	void (*function99)(); 
	/*0x18C*/	void (*function100)(); 
	/*0x190*/	void (*function101)(); 
	/*0x194*/	void (*function102)(); 
	/*0x198*/	void (*function103)(); 
	/*0x19C*/	void (*function104)(); 
	/*0x1A0*/	void (*function105)(); 
	/*0x1A4*/	void (*function106)(); 
	/*0x1A8*/	void (*function107)(); 
	/*0x1AC*/	void (*function108)(); 
	/*0x1B0*/	void (*function109)(); 
	/*0x1B4*/	void (*function110)(); 
	/*0x1B8*/	void (*function111)(); 
	/*0x1BC*/	void (*function112)(); 
	/*0x1C0*/	void (*function113)(); 
	/*0x1C4*/	void (*function114)(); 
	/*0x1C8*/	void (*function115)(); 
	/*0x1CC*/	void (*function116)(); 
	/*0x1D0*/	void (*function117)(); 
	/*0x1D4*/	void (*function118)(); 
	/*0x1D8*/	void (*function119)(); 
	/*0x1DC*/	void (*function120)(); 
	/*0x1E0*/	void (*function121)(); 
	/*0x1E4*/	void (*function122)(); 
	/*0x1E8*/	void (*function123)(); 
	/*0x1EC*/	void (*function124)(); 
	/*0x1F0*/	void (*function125)(); 
	/*0x1F4*/	void (*function126)(); 
};	

class CLTClient
{
public:
	/*0x00*/	char* (*GetObjectType)();
	/*0x04*/	CLTCommon** (*LTCommonClient)(); 
	/*0x08*/	CLTPhysicClient** (*LTPhysicsClient)();
	/*0x0C*/	DWORD (*LTTransform)(); 
	/*0x10*/	CLTModel **(*LTModel)();
	/*0x14*/	void (*function6)();  //soundmgr
	/*0x18*/	void (*function7)(); 
	/*0x1C*/	void (*function8)(); 
	/*0x20*/	void (*function9)(); 
	/*0x24*/	void (*function10)(); 
	/*0x28*/	void (*function11)(); 
	/*0x2C*/	void (*function12)(); 
	/*0x30*/	void (*function13)(); 
	/*0x34*/	void (*function14)(); 
	/*0x38*/	void (*function15)(); 
	/*0x3C*/	void (*function16)(); 
	/*0x40*/	void (*function17)(); 
	/*0x44*/	void (*function18)(); 
	/*0x48*/	void (*function19)(); 
	/*0x4C*/	void (*function20)(); 
	/*0x50*/	void (*function21)(); 
	/*0x54*/	void (*function22)(); 
	/*0x58*/	void (*function23)(); 
	/*0x5C*/	void (*function24)(); 
	/*0x60*/	void (*function25)(); 
	/*0x64*/	void (*function26a)(); 
	/*0x68*/	void (*function26b)(); 
	/*0x6C*/	void (*function27)(); 
	/*0x70*/	void (*function28)(); 
	/*0x74*/	LTRESULT (__stdcall *RemoveObject)(HOBJECT hObj); 
	/*0x78*/	void (*function30)(); 
	/*0x7C*/	void (*function31)(); 
	/*0x80*/	void (*function32)(); 
	/*0x84*/	void (*function33)(); 
	/*0x88*/	void (*function3f3)(); 
	/*0x8C*/	void (*function34)(); 
	/*0x90*/	void (*function35)(); 
	/*0x94*/	void (*function36)(); 
	/*0x98*/	void (*function37)(); 
	/*0x9C*/	LTRESULT (__stdcall *GetObjectRotation)(HLOCALOBJ hObj, LTRotation *pRotation);
	/*0xA0*/	LTRESULT (__stdcall *GetObjectPos)(HLOCALOBJ hObj, LTVector *pPos); 
	/*0xA0*/	void (*function40)(); 
	/*0xA4*/	void (*function41)(); 
	/*0xA8*/	void (*function42)(); 
	/*0xAC*/	void (*function43)(); 
	/*0xB0*/	void (*function44)(); 
	/*0xB4*/	void (*function45)(); 
	/*0xB8*/	void (*function46)(); 
	/*0xBC*/	void (*function47)(); 
	///*0xBC*/	void (*function48)(); 
	/*0xC0*/	LTRESULT (__stdcall *SetObjectPos)(HLOCALOBJ hObj, LTVector& vPos, DWORD bla); 
	/*0xC4*/	void (*function49)(); 
	/*0xC8*/	DWORD (*LTVideoMgr)(); 
	/*0xCC*/	CLTDrawPrim** (*LTDrawPrim)(); 
	/*0xD0*/	DWORD (*LTTexInterface)(); 
	/*0xD4*/	void (*function53)(); 
	/*0xD8*/	DWORD (*LTFontManager)(); 
	/*0xDC*/	CLTCursor **(*LTCursor)(); 
	/*0xE0*/	void (*function56)(); 
	/*0xE4*/	void (*function57)(); 
	/*0xE8*/	void (*function58)(); 
	/*0xEC*/	void (*function59)(); 
	/*0xF0*/	void (*function60)();
	/*0xF4*/	void (*function61)(); 
	/*0xF8*/	void (*function62)(); 
	/*0xFC*/	void (*function63)(); 
	/*0x100*/	void (*function64)(); 
	/*0x104*/	void (*function65)();
	/*0x108*/	void (*function66)(); 
	/*0x10C*/	void (*function67)(); 
	/*0x110*/	void (*function68)(); 
	/*0x114*/	void (*function69)(); 
	/*0x118*/	void (*function70)();
	/*0x11C*/	void (*function71)(); 
	/*0x120*/	void (*function72)(); 
	/*0x124*/	void (*function73)(); 
	/*0x128*/	void (*function74)(); 
	/*0x12C*/	void (*function75)();
	/*0x130*/	LTRESULT (*GetSConValueFloat)(const char *pName, float &val); 
	/*0x134*/	LTRESULT (*GetSConValueString)(const char *pName, char *valBuf, uint32 bufLen); 
	/*0x138*/	void (*function78)(); 
	/*0x13C*/	void (*function79)(); 
	/*0x140*/	void (*function80)();
	/*0x144*/	void (*function81)(); 
	/*0x148*/	void (*function82)(); 
	/*0x14C*/	void (*function83)(); 
	/*0x150*/	void (*function84)();  
	/*0x154*/	LTRESULT (__stdcall *SendToServer)(ILTMessage_Read *pMsg, uint32 flags);
	/*0x158*/	void (*function86)(); 
	/*0x15C*/	void (*function87)(); 
	/*0x160*/	void (*function88)(); 
	/*0x164*/	LTRESULT (*StartQuery)(const char *pInfo); 
	/*0x168*/	LTRESULT (*UpdateQuery)();
	/*0x16C*/	LTRESULT (*GetQueryResults)(NetSession* &pListHead); 
	/*0x170*/	LTRESULT (*EndQuery)(); 
	/*0x174*/	void (*function93)(); 
	/*0x178*/	void (*function94)(); 
	/*0x17C*/	void (*function95)();
	/*0x180*/	void (*function96)(); 
	/*0x184*/	void (*function97)(); 
	/*0x188*/	void (*function98)(); 
	/*0x18C*/	void (*function99)(); 
	/*0x190*/	void (*function100)();
};	

class CLTBase
{
public:
	/*0x00*/	CLTClient* pLTClient;
	/*0x04*/	LTRESULT (*GetTcpIpAddress)(char* sAddress, uint32 dwBufferSize, uint16 &hostPort); 
	/*0x08*/	LTRESULT (*SendTo)(ILTMessage_Read *pMsg, const char *pAddr, uint16 nPort); 
	/*0x0C*/	LTRESULT (*StartPing)(const char *pAddr, uint16 nPort, uint32 *pPingID); 
	/*0x10*/	LTRESULT (*GetPingStatus)(uint32 nPingID, uint32 *pStatus, uint32 *pLatency);
	/*0x14*/	LTRESULT (*RemovePing)(uint32 nPingID); 
	/*0x18*/	HOBJECT (*GetMainWorldModel)(); 
	/*0x1C*/	void (*function8)(); 
	/*0x20*/	void (*function9)(); 
	/*0x24*/	void (*function10)(); 
	/*0x28*/	void (*function11)(); 
	/*0x2C*/	void (*function12)(); 
	/*0x30*/	void (*function13)(); 
	/*0x34*/	void (*function14)(); 
	/*0x38*/	void (*function15)();
	/*0x3C*/	void (*function16)();
	/*0x40*/	void (*function17)();
	/*0x44*/	void (*function18)(); 
	/*0x48*/	void (*function19)();  
	/*0x4C*/	void (*function20)(); 
	/*0x50*/	LTRESULT (*GetSoundTimer)(HLTSOUND hSound, LTFLOAT *fTimer); 
	/*0x54*/	void (*function22)(); //keine funktion
	/*0x58*/	void (*function23)(); //keine funktion
	/*0x5C*/	void (*function24)(); //
	/*0x60*/	void (*function25)();
	/*0x64*/	bool (*IntersectSegment)(IntersectQuery& Query, IntersectInfo *pInfo);
	/*0x68*/	bool (*IntersectSegmentAgainst)(IntersectQuery& Query, IntersectInfo *pInfo, HOBJECT hObject); //drawd3d
	/*0x6C*/	void (*function28)(); //GetSourceWorldOffset
	/*0x70*/	void (*function29)();
	/*0x74*/	void (*function30)(); 
	/*0x78*/	void (*function31)();
	/*0x7C*/	LTRESULT (*GetGameMode)(int *mode);
	/*0x80*/	void (*function32)(); 
	/*0x84*/	LTRESULT (*GetLocalClientID)(DWORD* pid); 
	/*0x88*/	void (*Disconnect)(); 
	/*0x8C*/	bool (*IsConnected)();
	/*0x90*/	void (*ShutDown)(); 
	/*0x94*/	void (*ShutdownWithMessage)(const wchar_t *pMsg, ...); //
	/*0x98*/	void (*function39)(); 
	/*0x9C*/	void (*function40)(); 
	/*0xA0*/	void (*function41)(); 
	/*0xA4*/	void (*function42)(); 
	/*0xA8*/	void (*function43)(); 
	/*0xAC*/	void (*function421)(); 
	/*0xB0*/	void (*function44)(); 
	/*0xB4*/	void (*function45)(); 
	/*0xB8*/	void (*function46)(); 
	/*0xBC*/	void (*function47)(); 
	/*0xC0*/	void (*function48)(); 
	/*0xC4*/	void (*function49)(); 
	/*0xC8*/	void (*function50)(); 
	/*0xCC*/	void (*function51)(); 
	/*0xD0*/	void (*function52)(); 
	/*0xD4*/	void (*function53)(); 
	/*0xD8*/	void (*function54)(); 
	/*0xDC*/	LTRESULT (*SetRenderMode)(RMode *pMode); 
	/*0xE0*/	void (*function56)(); 
	/*0xE4*/	void (*function57)(); 
	/*0xE8*/	void (*function58)(); 
	/*0xEC*/	void (*function59)(); 
	/*0xF0*/	void (*function60)(); 
	/*0xF4*/	void (*function61)(); 
	/*0xF8*/	void (*function62)(); 
	/*0xFC*/	void (*function63)(); 
	/*0x100*/	LTRESULT (*RegisterConsoleProgram)(const char *pName, ConsoleProgramFn fn);   
	/*0x104*/	LTRESULT (*UnregisterConsoleProgram)(const char* pName); 
	/*0x108*/	void (*function66)();
	/*0x10C*/	void (*function67)(); 
	/*0x110*/	void (*function68)(); 
	/*0x114*/	void (*function69)(); 
	/*0x118*/	void (*function70)(); 
	/*0x11C*/	void (*function71)(); 
	/*0x120*/	void (*function72)(); 
	/*0x124*/	void (*function73)(); 
	/*0x128*/	void (*function74)(); 
	/*0x12C*/	void (*function75)(); 
	/*0x130*/	void (*function76)(); 
	/*0x134*/	void (*function77)(); 
	/*0x138*/	void (*function78)(); 
	/*0x13C*/	void (*function79)(); 
	/*0x140*/	void (*function80)(); 
	/*0x144*/	void (*function81)(); 
	/*0x148*/	void (*function82)(); 
	/*0x14C*/	void (*function83)(); 
	/*0x150*/	void (*function84)(); 
	/*0x154*/	void (*function85)(); 
	/*0x158*/	void (*function86)(); 
	/*0x15C*/	void (*function87)(); 
	/*0x160*/	void (*function88)(); 
	/*0x164*/	void (*function89)(); 
	/*0x168*/	void (*function90)(); 
	/*0x16C*/	void (*function91)(); 
	/*0x170*/	void (*function92)(); 
	/*0x174*/	void (*function93)(); 
	/*0x178*/	void (*function94)(); 
	/*0x17C*/	void (*function95)(); 
	/*0x180*/	void (*function96)(); 
	/*0x184*/	void (*function97)(); 
	/*0x188*/	void (*function98)(); 
	/*0x18C*/	void (*function99)(); 
	/*0x190*/	void (*function100)(); 
	/*0x194*/	void (*function101)(); 
	/*0x198*/	void (*function102)(); 
	/*0x19C*/	void (*function103)(); 
	/*0x1A0*/	void (*function104)(); 
	/*0x1A4*/	void (*function105)(); 
	/*0x1A8*/	void (*function106)(); 
	/*0x1AC*/	void (*function107)(); 
	/*0x1B0*/	void (*function108)(); 
	/*0x1B4*/	void (*function109)(); 
	/*0x1B8*/	void (*function110)(); 
	/*0x1BC*/	void (*function111)(); 
	/*0x1C0*/	void (*function112)(); 
	/*0x1C4*/	void (*function113)(); 
	/*0x1C8*/	void (*function114)(); 
	/*0x1CC*/	void (*function115)(); 
	/*0x1D0*/	void (*function116)(); 
	/*0x1D4*/	void (*function117)(); 
	/*0x1D8*/	void (*function118)(); 
	/*0x1DC*/	void (*function119)(); 
	/*0x1E0*/	void (*function120)(); 
	/*0x1E4*/	void (*function121)(); 
	/*0x1E8*/	void (*function122)(); 
	/*0x1EC*/	void (*function123)(); 
	/*0x1F0*/	void (*function124)(); 
	/*0x1F4*/	void (*function125)(); 
	/*0x1F8*/	void (*function126)(); 
	/*0x1FC*/	void (*function127)(); 
	/*0x200*/	void (*function128)(); 
	/*0x204*/	HCONSOLEVAR (*GetConsoleVariable)(const char* pszVarName); 
	/*0x208*/	int (*RunConsoleCommand)(char* pszString);  //0x208
	/*0x20C*/	HLOCALOBJ (*GetClientObject)(); 
	/*0x210*/	void (*function132)(); 
	/*0x214*/	void (*function133)(); 
	/*0x218*/	void (*function134)(); 
	/*0x21C*/	void (*function135)(); 
	/*0x220*/	void (*function136)(); 
	/*0x224*/	void (*function137)(); 
	/*0x228*/	void (*function138)(); 
	/*0x22C*/	void (*function139)(); 
	/*0x230*/	void (*function140)(); 
	/*0x234*/	void (*function141)(); 
	/*0x238*/	void (*function142)(); 
	/*0x23C*/	void (*function143)(); 
	/*0x240*/	void (*function144)(); 
	/*0x244*/	void (*function145)(); 
	/*0x248*/	void (*function146)(); 
	/*0x24C*/	void (*function147)(); 
	/*0x250*/	void (*function148)(); 
	/*0x254*/	void (*function149)(); 
	/*0x258*/	void (*function150)(); 
	/*0x25C*/	void (*function151)(); 
	/*0x260*/	void (*function152)(); 
	/*0x264*/	void (*function153)(); 
	/*0x268*/	void (*function154)(); 
	/*0x26C*/	void (*function155)(); 
	/*0x270*/	void (*function156)(); 
	/*0x274*/	void (*function157)(); 
	/*0x278*/	void (*function158)(); 
	/*0x27C*/	void (*function159)(); 
	/*0x280*/	void (*function160)(); 
	/*0x284*/	void (*function161)(); 
	/*0x288*/	void (*function162)(); 
	/*0x28C*/	void (*function163)(); 
	/*0x290*/	void (*function164)(); 
	/*0x294*/	void (*function165)(); 
	/*0x298*/	void (*function166)(); 
	/*0x29C*/	void (*function167)(); 
	/*0x2A0*/	void (*function168)(); 
	/*0x2A4*/	void (*function169)(); 
	/*0x2A8*/	void (*function170)(); 
	/*0x2AC*/	void (*function171)(); 
	/*0x2B0*/	void (*function172)(); 
	/*0x2B4*/	void (*function173)(); 
	/*0x2B8*/	void (*function174)(); 
	/*0x2BC*/	void (*function175)(); 
	/*0x2C0*/	LTRESULT (*GetServiceList)(NetService* &pListHead); 
	/*0x2C4*/	void (*SelectService)(); 
	/*0x2C8*/	void (*function178)(); 
	/*0x2CC*/	void (*function179)(); 
	/*0x2D0*/	void (*function180)(); 
	/*0x2D4*/	LTRESULT (*IsLobbyLaunched)(const char* sDriver); 
	/*0x2D8*/	LTRESULT (*GetLobbyLaunchInfo)(const char* sDriver, void** ppLobbyLaunchData); 
	/*0x2DC*/	void (*function183)(); 
	/*0x2E0*/	LTRESULT (*GetVersionInfoExt)(LTVERSIONINFOEXT* pVersionInfo); 
	/*0x2E4*/	void (*function185)(); 
	/*0x2E8*/	void (*function186)(); 
	/*0x2EC*/	void (*function187)(); 
	/*0x2F0*/	void (*function188)(); 
	/*0x2F4*/	void (*function199)(); 
	/*0x2F8*/	void (*function200)(); 
};

struct dummy
{
	/*0x00*/	DWORD unk1;
	/*0x04*/	ClientInfo* user;
};

//struct CViewangles
//{
//	/*0x00*/	char unknown0[76];
//	/*0x48*/	float pitch;
//	/*0x4C*/	float yaw; 
//	/*0x50*/	float roll;
//	/*0x54*/	char unknown84[216];
//	/*0x12C*/	CCamera1* cam1;
//	/*0x130*/	CCamera2* cam2; 
//};

struct CViewangles
{
	char unknown0[76]; //0x0000
	float pitch; //0x004C  
	float yaw; //0x0050  
	float roll; //0x0054  
	char unknown88[220]; //0x0058
	CCamera1* cam1; //0x0134  
	CCamera2* cam2; //0x0138  
};//Size=0x013C(316)

//\xA1\x00\x00\x00\x00\x8B\x40\x30\x83\xF8\x06
//37829338

//3746E062    A1 F4958237     MOV EAX,DWORD PTR DS:[378295F4] 54 58 5C

struct CCamera1
{
	/*0x00*/	char unknown0[4];
	/*0x04*/	LTVector pos1;
	/*0x10*/	LTVector pos2;
};

struct CCamera2
{
	/*0x00*/	char unknown0[24];
	/*0x18*/	LTVector pos;
};

struct CLocal
{
	/*0x00*/	char unknown0[200];
	/*0xC8*/	LTVector pos;
	/*0xD4*/	char unknown1[20];
	/*0xE8*/	float gravity;
};

//struct CEntity
//{
//	/*0x00*/	char unknown0[16];
//	/*0x10*/	HOBJECT object;
//	/*0x14*/	char unknown20[88];
//	/*0x6C*/	BYTE index;
//};






//struct CEntity
//{
//	char unknown0[16]; //0x0000
//	HOBJECT object; //0x0010  
//	char unknown20[88]; //0x0014
//	BYTE index; //0x006C  
//	char unknown109[1343]; //0x006D
//	BYTE ID03ABE4E8; //0x05AC  
//	BYTE ID03AB9268; //0x05AD 
//	BYTE lol[0x10];
//	WORD health; //0x05BE  
//	WORD armor; //0x05C0  
//
//	//5BE und 5C0
//};//Size=0x05B2(1458)

class CPlayerManager
{
public:
		char unknown0[24]; //0x0000
	DWORD weaponmanager; //0x0018  
		char unknown28[44]; //0x001C
	float pitch; //0x0048  
	float yaw; //0x004C  
	float roll; //0x0050  
		char unknown84[216]; //0x0054
	HOBJECT camera; //0x012C  
};//Size=0x0130(304)

struct CName
{
			char unknown0[4];
	char name[28];					//0004
};

struct CEntity
{
		char unknown0[16]; //0x0000
	HOBJECT object; //0x0010  
		char unknown20[40]; //0x0014
	CName* name; //0x003C  
	BYTE isplayer; //0x0040  
		char unknown65[15]; //0x0041
	BYTE ID03910438; //0x0050  
	BYTE ownerid; //0x0051  
		char unknown82[26]; //0x0052
	BYTE index; //0x006C  
		char unknown109[11]; //0x006D
	float pitch; //0x0078  
	float yaw; //0x007C  
		char unknown128[1240]; //0x0080
	HOBJECT hitbox; //0x0558  
		char unknown1372[98]; //0x055C
	WORD health; //0x05BE  
	WORD armor; //0x05C0  
};//Size=0x05C2(1474)


//struct CEntity
//{
//	char unknown0[4]; //0x0000
//	HOBJECT object; //0x0004  
//	char unknown8[100]; //0x0008
//	BYTE index; //0x006C  
//	char unknown109[1359]; //0x006D
//	BYTE ID039F22C8; //0x05BC  
//	char unknown1469[1]; //0x05BD
//	WORD health; //0x05BE  
//	WORD armor; //0x05C0  
//};


//8B 41 04 85 C0 74 18 8B 54 24 04 EB 03
//38 deaths in a row
struct ClientInfo
{
	/*0x00*/	  char unknown0[4];
	/*0x04*/	DWORD index; 
	/*0x08*/	  char unknown8[8];
	/*0x10*/	char name[20];
	/*0x24*/	  char unknown36[8];
	/*0x2C*/	DWORD kills;
	/*0x30*/	DWORD deaths;
	/*0x34*/	  char unknown52[60];
	/*0x70*/	BYTE team; 
	/*0x71*/	  char unknown113[7];
	/*0x74*/	BYTE isdead;  
	/*0x75*/	  char unknown117[319];
	/*0x1B4*/	  char unknown118[4];
	/*0x1B8*/	BYTE clanname[12];
	/*0x1C4*/	  char unknown440[84]; //84
	/*0x218*/	ClientInfo* prev; 
	/*0x21C*/	ClientInfo* next; //NA -> 0x224
};

struct ClientInfoNA
{
	/*0x00*/	  char unknown0[4];
	/*0x04*/	DWORD index; 
	/*0x08*/	  char unknown8[8];
	/*0x10*/	char name[20];
	/*0x24*/	  char unknown36[8];
	/*0x2C*/	DWORD kills;
	/*0x30*/	DWORD deaths;
	/*0x34*/	  char unknown52[60];
	/*0x70*/	BYTE team; 
	/*0x71*/	  char unknown113[3];
	/*0x74*/	BYTE isdead;  
	/*0x75*/	  char unknown117[319];
	/*0x1B4*/	  char unknown118[4];
	/*0x1B8*/	BYTE clanname[12];
	/*0x1C4*/	  char unknown440[92]; //84
	/*0x218*/	ClientInfo* prev; 
	/*0x21C*/	ClientInfo* next; //NA -> 0x224
};

struct CList
{
	/*0x00*/	CEntity** pEntity;
	/*0x04*/	DWORD unk1;
	/*0x08*/	DWORD maxindex;
	/*0x0C*/	DWORD unk2;
};

struct CEntityInfo
{
	/*0x00*/	DWORD unk1;
	/*0x04*/	CList pList[0x3D];
};

enum
{
	GRENADE  = 0x13,
	WEAPON   = 0x17,
	PLAYER   = 0x18,
	DEADBODY = 0x26,
	SPOTS    = 0x37,
	DOORS    = 0x39
};



extern DWORD dwUnlimitedRespawn;
extern DWORD dwUnlimitedRespawnOffset;
extern LTVector vTelePlayer;
extern bool bAlive;

struct blaaa
{
	DWORD dwAddress;
	BYTE copy[0x3E81];
}; 

extern blaaa copy1, copy2;


extern void SetPatches(int a, int b);


#define M_PI		3.14159265358979323846
#define M_PI_4      0.78539816339744830962
#define M_PI_2      1.57079632679489661923
#define GENERATE(a, r, g, b) g_pTools->GenerateTexture(pDevice, &tex##a, D3DCOLOR_ARGB(255,r,g,b));
#define SETTEX(a) if(tex##a && !IsBadReadPtr((void*)tex##a, 4)) pDevice->SetTexture(0, tex##a);
#define RELEASE(a) if(tex##a && !IsBadReadPtr((void*)tex##a, 4)){(tex##a)->Release();} tex##a = NULL;
#define CHECK(a) (!tex##a || IsBadReadPtr((void*)tex##a, 4))


typedef D3DXVECTOR3* (D3DAPI *D3DXVec3Project_t)(D3DXVECTOR3* pOut, D3DXVECTOR3* pV, D3DVIEWPORT9* pViewport, D3DXMATRIX* pProjection, D3DXMATRIX* pView, D3DXMATRIX* pWorld);
extern D3DXVec3Project_t pD3DXVec3Project;

typedef HRESULT (D3DAPI *D3DXCreateFont_t)(LPDIRECT3DDEVICE9 pDevice, INT Height, UINT Width, UINT Weight, UINT MipLevels, BOOL Italic, DWORD CharSet, DWORD OutputPrecision, DWORD Quality, DWORD PitchAndFamily, LPCTSTR pFacename, LPD3DXFONT* ppFont);
extern D3DXCreateFont_t pD3DXCreateFont;

typedef HRESULT (D3DAPI *D3DXCreateLine_t)(LPDIRECT3DDEVICE9 pDevice, LPD3DXLINE* ppLine);
extern D3DXCreateLine_t pD3DXCreateLine;

typedef HRESULT (D3DAPI *D3DXCreateSprite_t)(LPDIRECT3DDEVICE9 pDevice, LPD3DXSPRITE *ppSprite);
extern D3DXCreateSprite_t pD3DXCreateSprite;

typedef HRESULT (D3DAPI* D3DXGetImageInfoFromFileInMemory_t)(LPCVOID pSrcData, UINT SrcDataSize, D3DXIMAGE_INFO *pSrcInfo);
extern D3DXGetImageInfoFromFileInMemory_t pD3DXGetImageInfoFromFileInMemory;

typedef HRESULT (D3DAPI* D3DXCreateTextureFromFileInMemoryEx_t)(LPDIRECT3DDEVICE9 pDevice, LPCVOID pSrcData, UINT SrcDataSize, UINT Width, UINT Height, UINT MipLevels, DWORD Usage, D3DFORMAT Format, D3DPOOL Pool, DWORD Filter, DWORD MipFilter, D3DCOLOR ColorKey, D3DXIMAGE_INFO* pSrcInfo, PALETTEENTRY* pPalette, LPDIRECT3DTEXTURE9* ppTexture);
extern D3DXCreateTextureFromFileInMemoryEx_t pD3DXCreateTextureFromFileInMemoryEx;


class CConsoleCommand {

	char commands[100][100];
	float on_value[100];
	float off_value[100];
	int othertoggle[100];
	int* toggle;
	int oldtoggle;
	int slot;

public:
	CConsoleCommand(){
		memset(&commands, 0, sizeof(commands));
		memset(&on_value, 0, sizeof(on_value));
		memset(&off_value, 0, sizeof(off_value));
		memset(&othertoggle, 0, sizeof(othertoggle));
		toggle = 0;
		oldtoggle = 0;
	}

	CConsoleCommand(int *toggle) {
		CConsoleCommand();
		this->toggle = toggle;
	}

	void Add(char* cmd, float on_value, float off_value, int toggle = 0) {
		strcpy(commands[slot], cmd);
		this->on_value[slot] = on_value;
		this->off_value[slot] = off_value;

		if(toggle) {
			othertoggle[slot] = toggle;
		}

		slot++;
	}

	void Execute() {

		if(!g_pEngine->m_pLTBase) return;

		if(*toggle && *toggle != oldtoggle) {

			oldtoggle = *toggle;

			char buf[64] = "";

			bool found = false;

			for(int i = 0; i < slot; i++) {
				if(othertoggle[i] == *toggle) {

					sprintf(buf, _sf, commands[i], on_value[i]);
					g_pEngine->m_pLTBase->RunConsoleCommand(buf);
					found = true;
				}
			}

			if(found) {

				return;
			}

			for(int i = 0; i < slot; i++) {
				if(!othertoggle[i]) {

					sprintf(buf, _sf, commands[i], on_value[i]);
					g_pEngine->m_pLTBase->RunConsoleCommand(buf);
				}
			}
		}

		if(!*toggle && *toggle != oldtoggle) {

			oldtoggle = *toggle;
			char buf[64] = "";

			for(int i = 0; i < slot; i++) {
				sprintf(buf, _sf, commands[i], off_value[i]);
				g_pEngine->m_pLTBase->RunConsoleCommand(buf);
			}
		}
	}
};

class Main;
class Info;
class Stats;
class WepInfo;
class Something;

class Main
{
public:
	Info* goToInfoClass; //0000
};

class Info
{
public:
	char unknown0[8];
	__int32 Unknown1; //0008
	__int32 iCurrentHealth; //000C
	char unknown2[4];
	__int32 iMaxHealth; //0014
	char unknown3[4];
	WepInfo* goToWepInfo;//001C
	Something* goToSomething; //0020
	char unknown4[348];
	float fStamina; //0180
	char unknown5[8];
	__int32 iWeaponSlotInUse; //018C
	float fCameraSomething; //0190
	float fCameraSomething2; //0194
	float fCameraSomething3; //0198
	char unknown6[32];
	Stats* goToStats; //01BC
};

class WepInfo
{
public:
	__int32 iBackPackABulletsLeft; //0000
	__int32 i2ndaryBulletsLeft; //0004
	__int32 Unknown0; //0008
	__int32 iGrenadesLeft; //000C
	__int32 iBackPackBBulletsLeft; //0010
};

class Stats
{
public:
	char unknown0[16];
	char szLocalNameChangesInKill[16]; //0010
	char unknown1[12];
	__int32 iKills; //002C
	__int32 iDeaths; //0030
	char unknown2[44];
	__int32 iHeadShots; //0060
};

class Something
{
public:
	BYTE bIsAlive; //0000
};


struct WeaponInfos
{
	char unknown0[5564]; //0x0000
	DWORD weapontable; //0x15BC  
	__int32 number_of_weapons; //0x15C0  
	char unknown5572[652]; //0x15C4
	DWORD unk1; //0x1850  
	DWORD unk2; //0x1854  
	char unknown6232[40]; //0x1858
	float recoil1; //0x1880  
	float recoil2; //0x1884  
	char unknown6280[16]; //0x1888
	float recoil3; //0x1898  
	float recoil4; //0x189C  
};//Size=0x18A0(6304)


#endif