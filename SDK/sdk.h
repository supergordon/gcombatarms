#ifndef _SDK_H_
#define _SDK_H_



#define PROJECT_FEAR
#define _FINAL
#define INCLUDE_AS_ENUM


#include "sdk/game/shared/debugnew.h"
#include "sdk/engine/sdk/inc/iltmessage.h"
#include "sdk/game/clientshelldll/gameclientshell.h"

using namespace std;

typedef unsigned int        uint;
typedef char				int8;
typedef unsigned char		uint8;
typedef short int			int16;
typedef unsigned short int	uint16;
typedef int					int32;
typedef unsigned int		uint32;


#endif

//README:
//das in den zusätzlichen include verzeichnisse in den projekt optionen reinschreiben:
/*SDK\libs;SDK\libs\stdlith;SDK\libs\platform;SDK\libs\platform\sys;SDK\libs\platform\win32;SDK\engine\sdk\inc;SDK\engine\sdk;SDK\engine\;SDK\Game\Libs\LTGUIMgr;SDK\Game\Shared;SDK\Game\ObjectDLL;SDK\Game\ClientShellDLL;SDK\Game\ClientFxDLL;SDK\Game*/
//struktur muss so sein: 
//projektfolder\SDK
//projektfolder\SDK\engine
//projektfolder\SDK\libs
//etc

//diese datei, sdk.h in zu den header files in visual studio adden und in allen cpps includen