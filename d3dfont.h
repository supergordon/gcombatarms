#ifndef _D3DFONT_H
#define _D3DFONT_H

#define D3DFONT_RIGHT DT_RIGHT

// rendering and initialization macros
#define D3DFVF_BITMAPFONT (D3DFVF_XYZRHW|D3DFVF_DIFFUSE|D3DFVF_TEX1)
#define D3DFVF_FONTFX (D3DFVF_XYZRHW|D3DFVF_DIFFUSE)
#define SAFE_RELEASE(p) if(p) { p->Release(); p = NULL; }

// font rendering style flags
#define FT_NONE			0x0
#define FT_CENTER		0x1
#define FT_BORDER		0x2
#define FT_COLOUR		0x4
#define FT_SHADOW		0x16
#define FT_ALL			0xFFFFFFFF

// vertex definations
struct	d3dfont_s	{ float x,y,z,rhw; DWORD colour; float tu,tv; };
struct	d3dfontfx_s { float x,y,z,rhw; DWORD colour; };

// externed classes
class CD3DFont;
extern CD3DFont *g_pD3DFont;

// class defination
class CD3DFont
{
public:
	CD3DFont()
	{
		m_pD3Ddev = NULL;
		m_pD3Dtex = NULL;
		m_pD3Dbuf = NULL;
		m_pD3Dfxbuf = NULL;
		m_bCanPrint = false;
		m_iMaxChars = 1024;
		m_iUsedChars = 0;
		m_iMaxFxVert = 240;
		m_iUsedFxVert = 0;
	}

	~CD3DFont()
	{
		Invalidate();
	}
	
	//methods (initializations)
	HRESULT GenFont(IDirect3DDevice9*, char*, UINT, DWORD);	// generates font bitmap and coordinates
	HRESULT Invalidate();						// free resources and prevents drawing
	
	//methods (rendering)
	HRESULT AddFont(float x, float y, DWORD colour, DWORD flags, char *szText);	// adds characters and effects to buffer
	HRESULT RenderAll();						// render characters and effects from buffer

	//methods (other)
	void ClearBuffer();							// sets used buffer to 0

	//accessors
	float DrawLength(char*) const;				// gets renderable size of string (in px) 
	float TypeHeight() const { return m_fCharHeight; } // gets renderable height of a character (in px)

private:
	//private methods
	HRESULT GenBuffers();						// generates vertex buffers
	HRESULT GenStateBlocks();					// generates stateblocks

	IDirect3DDevice9		*m_pD3Ddev;			// rendering device
	IDirect3DTexture9		*m_pD3Dtex;			// font bitmap
	
	IDirect3DVertexBuffer9	*m_pD3Dbuf;			// regular character buffer
	IDirect3DVertexBuffer9	*m_pD3Dfxbuf;		// extra effects buffer

	IDirect3DStateBlock9	*m_pD3DstateDraw;	// render state for drawing fonts
	IDirect3DStateBlock9	*m_pD3DstateNorm;	// regular game render state saved
    
	bool m_bCanPrint;							// error checking

	unsigned int m_iTexWidth, m_iTexHeight;		// texture dimentions (remove?)
	float m_fTexCoords[96][4];					// texture character mapping
	float m_fCharWidth[96];						// character width
	float m_fCharHeight;						// character height (all chars are equal)

	int m_iMaxChars;							// maximum number of characters
	int m_iUsedChars;							// currently used # of characters
	int m_iMaxFxVert;							// maximum fx verticies
	int m_iUsedFxVert;							// currently used fx verticies
};

#endif