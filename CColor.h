#ifndef _COLOR_
#define _COLOR_

class CColor
{
	public:
		CColor(int r = 0, int g = 0, int b = 0, int a = 255);
		CColor &operator =(CColor& color);
		DWORD Get();
	private:
		int m_iR, m_iG, m_iB, m_iA;
		DWORD m_dwColor;
	protected:
		void Set();
};

extern CColor red;
extern CColor green;
extern CColor blue;
extern CColor white;
extern CColor black;
extern CColor yellow;
extern CColor grey;
extern CColor green_menu;
extern CColor white_menu;
extern CColor black_menu;

#endif